$(window).load(function () {

	var levelManager = new LevelManager();

	var skinManager = new SkinManager('assets/skin', function(){
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    });

    function xmlLoadHandler(xml){

		var domRoot = $('body');

		var videoData = $(xml).children('video');

		//var videoWidth = +$(videoData).attr('width');
		//var videoHeight = +$(videoData).attr('height');

		var videoWidth = $(document).width();
		var videoHeight = $(document).height();

		//if(!videoWidth || videoWidth == 0 || typeof videoWidth == 'undefined' || videoWidth == '') videoWidth = 800;
		//if(!videoHeight || videoHeight == 0 || typeof videoHeight == 'undefined' || videoHeight == '') videoHeight = 600;

		var videoSource = levelManager.getRoute(videoData.text());

		var sources = [
            { src: videoSource + ".mp4", type: 'video/mp4' },
            { src: videoSource + ".webm", type: 'video/webm' },
            { src: videoSource + ".ogv", type: 'video/ogg' }
        ];

        //$('#mainVideoPlayer').attr('width', videoWidth);
        //$('#mainVideoPlayer').attr('height', videoHeight);

		var player = new MediaElementPlayer('#mainVideoPlayer', {
		    success: function (mediaElement, domObject) {
	            mediaElement.setSrc(sources);
	            mediaElement.load();
	            mediaElement.play();
		    }
		});

		// ... sometime later

		//player.setSrc(videoSource + '.mp4');
		//player.play();

	}

	var vid = document.querySelector('video'), ratio;
	$(vid).css({display: 'block', margin: '0 auto'});
	$('.mejs-container').css({width: '100%', height: '95%'});

	function videoResize (r){
		console.log('**');
		console.log(ratio);
		console.log(window.innerWidth/window.innerHeight);


		if(window.innerWidth/window.innerHeight > ratio){
			$(vid).css({height: '100%', width: 'auto',  marginTop: 0});
		} else {
			$(vid).css({height: 'auto', width: '100%', marginTop: (window.innerHeight - $(vid).height())/2 + 'px'});
		}
	}

	var vidLoadInterval = setInterval(function(){

		if(vid.videoWidth){
			clearInterval(vidLoadInterval);

			ratio = vid.videoWidth/vid.videoHeight;

			videoResize();
			$(window).on('resize', videoResize);
		}

	}, 100);
});