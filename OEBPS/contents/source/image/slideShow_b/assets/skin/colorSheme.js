var colorSheme = {
	"scrollBar":{
		"trackColor": "#87D0E7",
		"arrowColor": "#87D0E7",
		"innerSliderColor": "#87D0E7",
		"outerSliderColor": "#87D0E7"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#00A8D4",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"answerContainer":{
		"color": "white",
		"stroke": "#7FD3E9",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"tableContainer":{
		"color": "white",
		"stroke": "#7FD3E9",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"answerBlock":{
		"color": "#00A8D4",
		"stroke": "",
		"strokeWidth": 1,
		"fontColor": "white",
		"cornerRadius": 4
	},
	"staticTableBlock":{
		"color": "#00A8D4",
		"stroke": "#00A8D4",
		"strokeWidth": 1,
		"fontColor": "white",
		"cornerRadius": 0
	},
	"tableBlock":{
		"color": "white",
		"stroke": "#d6d6d6",
		"strokeWidth": 1,
		"fontColor": "black",
		"cornerRadius": 0
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"resultScreen":{
		"header":{
			"color": "#82C351",
			"fontColor": "white"
		},
		"color": "white",
		"stroke": "#97c25a",
		"strokeWidth": 0.8,
		"fontColor": "black"
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"slideTitle":{
		"fontColor": "black",
		"limiterColor": "#97c25a",
		"limiterWidth": 1
	},
	"slideTip":{
		"fontColor": "black",
		"color": "#CDE7B9",
		"stroke": "#CDE7B9",
		"strokeWidth": 1
	},
	"setMarker":{
		"active":{
			"color": "#97C25A",
			"stroke": "#97C25A",
			"strokeWidth": 1,
			"fontColor": "white"
		},
		"regular":{
			"color": "white",
			"stroke": "#adadad",
			"strokeWidth": 1,
			"fontColor": "black"
		}
		
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#97c25a",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#97c25a"
		},
		"playButton": "playButtonGreen.png"
	},
	"slidePlayButton": "slidePlayButtonGreen.png"
}