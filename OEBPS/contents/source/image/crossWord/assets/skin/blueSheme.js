var colorSheme = {
	"scrollBar":{
		"trackColor": "#87D0E7",
		"arrowColor": "#87D0E7",
		"innerSliderColor": "#87D0E7",
		"outerSliderColor": "#87D0E7"
	},
	"scrollArrow":{
		"color": "#D7D7D7",
		"stroke": "",
		"strokeWidth": 0
	},
	"toolTipRectangle":{
		"backgroundColor": "#00A8D4",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 0
	},
	"bottomPanel":{
		"color": "#DCE6D1",
		"stroke":"#C0DEA8"
	},
	"crossWordSymbol":{
		"clear":{
			"backgroundColor": "white",
			"cornerRadius": 0,
			"stroke": "#adadad",
			"strokeWidth": 1,
			"fontColor": "black"
		},
		"active":{
			"backgroundColor": "#00A8D4",
			"stroke": "#dcebef",
			"strokeWidth": 1,
			"fontColor": "white"
		},
		"focused":{
			"backgroundColor": "#0059A9",
			"stroke": "#dcebef",
			"strokeWidth": 1,
			"fontColor": "white"
		},
		"constant":{
			"stroke": "#ffff5a",
			"strokeWidth": 2
		},
		"wrong":{
			"backgroundColor": "#FF7B7B",
			"stroke": "#FF0033"
		}
	},
	"crosswordPanel":{
		"backgroundColor": "white",
		"stroke": "",
		"cornerRadius": 0
	},
	"tipPanel":{
		"backgroundColor": "white",
		"stroke": "",
		"cornerRadius": 0,
		"lineColor": "#7FD3E9",
		"glowedRect":{
			"color": "#F8F8F9",
			"stroke": "#46a6d1",
			"strokeWidth": 0.8
		},
		"tipIcon":{
			"backgroundColor": "#00A8D4",
			"fontColor": "white"
		}
		
	},
	"keyboard":{
		"backgroundColor": "white",
		"stroke": "#46a6d1",
		"strokeWidth": 1.5,
		"cornerRadius": 0,
		"keyboardIcon": "keyIconBlue.png",
		"rectStroke": "#46a6d1",
		"rectStrokeWidth": 1,
		"closeIcon": "closeIconBlue.png"
	},
	"zoomIcon": "zoomIconBlue.png",
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#46a6d1",
		"strokeWidth": 0.9,
		"cornerRadius": 6
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#7FD3E9",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#7FD3E9"
		}
	}
}