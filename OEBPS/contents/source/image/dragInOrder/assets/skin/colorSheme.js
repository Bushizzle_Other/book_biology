var colorSheme = {
	"scrollBar":{
		"trackColor": "#97C25A",
		"arrowColor": "#97C25A",
		"innerSliderColor": "#97C25A",
		"outerSliderColor": "#97C25A"
	},
	"bottomPanel":{
		"color": "#97c25a",
		"stroke":"#97c25a"
	},
	"object":{
		"color": "white",
		"stroke": "#c8c4bd",
		"strokeWidth": 0.5,
		"cornerRadius": 0
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#8BC75F",
		"strokeWidth": 0,
		"cornerRadius": 0
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"tip":{
		"color": "black",
		"fontColor": "white",
		"opacity": 0.7
	},
	"player":{
		"backgroundRectColor": "black",
		"trackBackroundColor": "#4B4B4B",
		"trackColor": ["white", "white"],
		"opacity": 0.7
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#97c25a",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#97c25a"
		}
	}
}