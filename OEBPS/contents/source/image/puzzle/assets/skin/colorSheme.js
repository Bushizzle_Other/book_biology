var colorSheme = {
	"scrollBar":{
		"trackColor": "#97C25A",
		"arrowColor": "#97C25A",
		"innerSliderColor": "#97C25A",
		"outerSliderColor": "#97C25A"
	},
	"bottomPanel":{
		"color": "#DCE6D1",
		"stroke":"#C0DEA8"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#8BC75F",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"puzzleRect":{
		"fill": "white",		
		"stroke": "#97c25a",
		"strokeWidth": 2
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C"
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#97c25a",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#97c25a"
		}
	}
}