var colorSheme = {
	"body":{
		"backgroundColor": "white"
	},
	"scrollBar":{
		"trackColor": "#87D0E7",
		"arrowColor": "#87D0E7",
		"innerSliderColor": "#87D0E7",
		"outerSliderColor": "#87D0E7",
		"backgroundColor": "white"
	},
	"glowedWordColor": "orange",
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#00A8D4",
		"strokeWidth": 1.2,
		"cornerRadius": 0
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"glowedWordColor": "#d32424",
	"glowedWord": {
		"regular": "#3059a5",
		"wrong": "#d32424",
		"hover": "#46a6d1",
		"stroke": "#3059a5"
	},
	"experimentPanel":{
		"backgroundColor": "white",
		"stroke": "#00A8D4",
		"cornerRadius": 0,
		"opacity": 1
	},
	"experimentResult":{
		"backgroundColor": "white",
		"stroke": "#00A8D4"
	},
	"answerRect":{
		"color": "#00A8D4",
		"stroke": "#00A8D4",
		"cornerRadius": 2,
		"fontColor": "white"
	},
	"uiPanel":{
		"color": "#DCE6D1",
		"stroke": "#9DCD76"
	},
	"dropWord":{
		"backgroundColor": "#2E302A",
		"fontColor": "white",
		"stroke": "#82C351"
	},
	"defaultResult": "defaultResultBlue.png",
	"header":{
		"mainTitle":{
			"backgroundColor": "#7FD3E9",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#7FD3E9"
		}
	},
	"countDown":{
		"color": "#82C351"
	}
}