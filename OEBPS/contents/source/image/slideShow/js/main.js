$(function(){

    function clear(c, a, b){
        while(c.indexOf(a) > 0)c = c.replace(a, b);
        return c;
    }

    // Get script with xml variable

    var configUri = location.href.split('?config=')[1],
        folder = location.href.split('slideShow')[0] + location.href.split('index.html?config=')[1].split('/')[1] + '/';

    var script = document.createElement('script');
    script.src = configUri;
    document.documentElement.appendChild(script);

    var inteval = setInterval(function(){
        if(typeof xml != "undefined"){
            clearInterval(inteval);

            parseData();

            audios.forEach(function(item, i){
                audioPreload(item.file, i);
            });

        }
    }, 500);


    var audio = new Audio(),
        $playBtn = $('#play'),
        $img = $('#slide'),
        audios = [],
        images = [],
        currentAudio = 0,
        currentImage = 0,
        currentTimeOffset = false,
        isPlaying = false,
        nextImage,
        audioPreloadCounter = 0;

    function audioPreload(src, a){
        var aud = new Audio();
        aud.src = folder + src;
        aud.oncanplay = function(i){
            audios[a].duration = aud.duration*1000;
            console.log(audios[a].duration);
            audioPreloadCounter++;
            if(audioPreloadCounter == audios.length){

                console.log(audios);

                $playBtn.click(function(){

                    if(!isPlaying){
                        playThis();
                    } else {
                        stopThis();
                    }

                }).removeClass('loading');

            }
        };
    }

    function parseData(){

        var tempXml = xml;

        tempXml = tempXml.split('<sounds>')[1].split('</frames>')[0];
        tempXml = clear(tempXml, 'sound', 'b');
        tempXml = clear(tempXml, 'frame', 'i');

        var $manifest = $('<div/>').append(tempXml);
        $manifest.find('b').each(function(){
            audios.push({
                file: $(this).attr('File'),
                duration: 0
            });
        });
        $manifest.find('i').each(function(){
            var el = $(this);
            images.push({
                file: el.attr('File'),
                duration: el.attr('duration')
            })
        });

        $img.attr('src', folder + images[0].file);

        audio.onended = function(){
            currentAudio++;

            if(currentAudio == audios.length) {
                stopThis();
                resetThis();
                audio.setAttribute('src', folder + audios[currentAudio].file);
                isPlaying = false;
            }
            else if(isPlaying) {
                audio.setAttribute('src', folder + audios[currentAudio].file);
                console.log('audio: ' + (currentAudio + 1) + '/' + audios.length);
                audio.play();
            }
        };

        audio.setAttribute('src', folder + audios[currentAudio].file);
        console.log('audio: ' + (currentAudio + 1) + '/' + audios.length);
        $img.attr('src', folder + images[currentImage].file);
    }

    function playThis(){

        $playBtn.addClass('playing');
        isPlaying = true;

        var timeout = images[currentImage].duration;

        console.log('image: ' + (currentImage + 1) + '/' + images.length);

        if(currentTimeOffset){
            timeout = images[currentImage].duration - currentTimeOffset;


            console.log('*******play*********');
            console.log('duration: ' + images[currentImage].duration);
            console.log('current pos: ' + currentTimeOffset);
            console.log('remains: ' + timeout);

            currentTimeOffset = false;
        }

        nextImage = setTimeout(function(){
            currentImage++;

            if(currentImage == images.length) {
                console.log('no more images');
                clearTimeout(nextImage);
            }
            else if (isPlaying) {
                $img.attr('src', folder + images[currentImage].file);
                playThis();
            }

        }, timeout);

        audio.play();

    }

    function stopThis(){
        $playBtn.removeClass('playing');
        isPlaying = false;

        currentTimeOffset = getCurrentImageTime();
        audio.pause();
        clearTimeout(nextImage);
    }

    function resetThis(){
        currentImage = 0;
        currentAudio = 0;
        currentTimeOffset = false;
    }

    function getCurrentImageTime(){
        console.log('******** STOP *********');
        var currAudioTime = audio.currentTime*1000;
        console.log('curr audio time ' + currAudioTime);

        var prevAudioTime = function(){
            var t = 0;
            if(currentAudio > 0){
                var b = 0;
                while(b < currentAudio){t+=audios[b].duration;b++}
            }
            return t;
        };
        console.log('prev audios time:' + prevAudioTime());

        var globalAudioTime = currAudioTime + prevAudioTime();
        console.log('global audio time: ' + globalAudioTime);

        var prevImageTime = function(){
            var t = 0;
            if(currentImage > 0){
                var c = 0;
                while(c < currentImage){t+=images[c].duration;c++}
            }
            return t;
        };
        console.log('prev image time: ' + prevAudioTime());

        console.log('current image time: ' + (globalAudioTime - prevImageTime()));

        return (globalAudioTime - prevImageTime());
    }

});