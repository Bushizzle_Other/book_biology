var colorSheme = {
	"scrollBar":{
		"trackColor": "#97c25a",
		"arrowColor": "#97c25a",
		"innerSliderColor": "#97c25a",
		"outerSliderColor": "#97c25a",
		"backgroundColor": "white"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#97c25a",
		"strokeWidth": 2,
		"cornerRadius": 0
	},
	"radioButtonGraphic": "radioButtonGreen.png",
	"checkBoxGraphic": "checkBoxGreen.png",
	"taskBoxGraphic": "taskLabelGreen.png",
	"player":{
		"backgroundRectColor": "black",
		"trackBackroundColor": "#4B4B4B",
		"trackColor": ["white", "white"],
		"opacity": 0.7
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#E7F3DE",
			"strokeColor": "#E7F3DE"
		},
		"innerRect":{
			"backgroundColor": "white",
			"strokeColor": ""
		},
		"selected":{
			"backgroundColor": "#E7F3DE",
			"strokeColor": "#E7F3DE"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#97c25a",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#97c25a"
		}
	}
}