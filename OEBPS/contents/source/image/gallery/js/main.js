$(document).ready(function () {

	$(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

    hookManager.applyFix('KitkatTouchFix');

    Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    //var isLandscape = true;

    function reorient(e) {
		//isLandscape = (window.orientation % 180 != 0);
		//$("body > div").css("-webkit-transform", !landscape ? "rotate(-90deg)" : "");
		window.location.href = window.location.href;
	}

    window.onorientationchange = reorient;
    //window.setTimeout(reorient, 0);

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

	var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

	var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'touchstart';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'touchend';
	events[0].mousedown = 'mousedown';
	events[1].mousedown = 'tap';

	/*Глобальные переменные**/

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var contentMargin = {x: 5, y: 5}

	var aspectRatio = 0; //widescreen 16:9

	var defaultStageWidth = 1024;
	var defaultStageHeight = 576;

	//Отступ всего контента от сцены
	var marginLeft = 60;
    var marginTop = 95;

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 20;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

	var isHorizontal;

	var panelWidth;
	var panelHeight;

	var imageMaxWidth;
	var imageMaxHeight;

	var marginX;
	var marginY;

	var pictureCollection = [];

	var tipCollection = [];

	var currentPicture = null;

	var tipGroup;

	var preventActions = false;

	var levelManager = new LevelManager();
	var colorSheme = null;
	var skinManager = null;

	var imageHolder = null;

	var slideLock = false;

	var contentContainer = null;

	var mainFont = 'mainFont';

	var slideControlls = null;

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
	var tipLayer = new Kinetic.Layer();
	var controllsLayer = new Kinetic.Layer();

	stage.add(backgroundLayer);
	stage.add(contentLayer);
	stage.add(tipLayer);
	stage.add(controllsLayer);

	/*Группы**/
	var tableGroup = new Kinetic.Group();
	var answerGroup = new Kinetic.Group();

	//contentLayer.add(tableGroup);
	//contentLayer.add(answerGroup);

	var preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));

	$(window).on('resize', fitStage);

	function fitStage(){
		scaleScreen($(window).width(),  $(window).height());
	}

	function initAspectRatio(){
		//var flag =
		if($(window).width() / $(window).height() < 1.6){
			aspectRatio = 1;
			defaultStageWidth = 1024;
			defaultStageHeight = 768;

			stage.width(defaultStageWidth);
			stage.width(defaultStageHeight);
		}
	}

	function scaleScreen(width, height){
		var marginX = 0;
		var marginY = isIOS ? 0 : 0;

		width -= marginX;
		height -= marginY;

		var scalerX = width / defaultStageWidth;
		var scalerY = height / defaultStageHeight;
		var scaler = Math.min(scalerX, scalerY);

		stage.scaleX(scaler);
		stage.scaleY(scaler);

		stage.scaler = scaler;

		stage.width(width);
		stage.height(height);

		stage.draw();
	}

	function initWindowSize(){
		scaleScreen($(window).width(), $(window).height());
	}

	function isColliding(firstRectangle, secondRectangle){
		var result = false;

		if(!(firstRectangle.bottom < secondRectangle.top ||
			firstRectangle.top > secondRectangle.bottom ||
			firstRectangle.left > secondRectangle.right ||
			firstRectangle.right < secondRectangle.left))

				result = true;

		return result;
	 }

	function createAnswerBlock(x, y, width, height, label){
		var answerBlock = new Kinetic.Group({x:x, y:y, draggable:true, dragOnTop: false});

		answerBlock.defaultX = x;
		answerBlock.defaultY = y;

		answerBlock.getValue = function(){
			return label.text();
		}

		answerBlock.resetPosition = function(){
			answerBlock.x(answerBlock.defaultX);
			answerBlock.y(answerBlock.defaultY);

			contentLayer.draw();
		}

		x = y = 0;

		var rect = new Kinetic.Rect({
			x: x,
			y: y,
			width: width,
			height: height,
			stroke: 'black',
			strokeWidth: 0.5,
			fillLinearGradientStartPoint: {x:width/2, y:0},
	        fillLinearGradientEndPoint: {x:width/2,y:height},
	        fillLinearGradientColorStops: [0, '#E9FFB1', 1, '#7DAD55']
		});

		label.x(x);
		label.y(y);

		answerBlock.on(events[isMobile].mouseover, hoverPointer);
    	answerBlock.on(events[isMobile].mouseout, resetPointer);

		answerBlock.on('dragstart', function(evt) {
			answerBlock.moveToTop();
		});
		answerBlock.on('dragend', function(evt) {
			var node = evt.dragEndNode;
			var firstRectangle = { left:node.x(), right:node.x()+maxItemWidth, top:node.y(), bottom:node.y()+maxItemHeight } //danger height!!!!11
			var secondRectangle;
			var tableBoxArray = tableGroup.getChildren();

			for(var i = 0; i < tableBoxArray.length; i++){
				if(tableBoxArray[i].isLocked) continue;
				secondRectangle = {
					left : tableBoxArray[i].x(),
					right : tableBoxArray[i].x() + maxItemWidth,
					top : tableBoxArray[i].y(),
					bottom : tableBoxArray[i].y() + maxItemHeight
				}

				//if(isColliding(firstRectangle, secondRectangle)){

				if(isMouseColliding(secondRectangle)){

					tableBoxArray[i].setValue(node);

					if(answerMode=='copy'){
						answerBlock.resetPosition();
					} else{
						answerBlock.hide();
					}

					contentLayer.draw();

					return;
				}
			}
			answerBlock.resetPosition();
		});

	    answerBlock.add(rect);
	    answerBlock.add(label);
	    answerGroup.add(answerBlock);

	    contentLayer.draw();
	}

	function isMouseColliding(boundRectangle){
		var pointer = getRelativePointerPosition();

		return (pointer.x > boundRectangle.left && pointer.x < boundRectangle.right && pointer.y > boundRectangle.top && pointer.y < boundRectangle.bottom);
	}

	function getRelativePointerPosition() {
	    var pointer = stage.getPointerPosition();
	    var pos = stage.getPosition();
	    var offset = stage.getOffset();
	    var scale = stage.getScale();

	    return {
	        //x : ((pointer.x / scale.x) - (pos.x / scale.x) + offset.x),
	        //y : ((pointer.y / scale.y) - (pos.y / scale.y) + offset.y)
	        x : pointer.x / scale.x,
	        y : pointer.y / scale.y
	    };
	}

	function createTableBlock(x, y, width, height, type, value, answer){
		var tableBlock = new Kinetic.Group({x: x, y: y});
		var fontStyle = 'italic';

		if(value=='active'){
			tableBlock.answer = answer;
			tableBlock.isLocked = false;
			value = '';
		} else{
			fontStyle = 'bold'
			tableBlock.isLocked = true;
		}

		x = y = 0;

		var radius = 15;
		var drawFunction = center;

		switch(type){
			case "topLeft": drawFunction = topLeft;
				break;
			case "botLeft": drawFunction = botLeft;
				break;
			case "botRight": drawFunction = botRight;
				break;
			case "topRight": drawFunction = topRight;
				break;
		}

		var rect = new Kinetic.Shape({
		    drawFunc: drawFunction,
		    fill: 'white',
		    stroke: 'black',
		    strokeWidth: 1,
		});

		var blockLabel = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: value,
	        fontSize: xmlFontSize,
	        fontFamily: mainFont,
	        fill: 'black',
	        align: 'center',
	        fontStyle: fontStyle,
	        width: width
	    });

	    tableBlock.check = function(){
	    	return (answer.indexOf(tableBlock.value)!=-1)
	    }

		tableBlock.setValue = function(answerBlock){
			if(answerMode != 'copy'){

				if('answerBlock' in tableBlock){
					tableBlock.answerBlock.show();
					tableBlock.answerBlock.resetPosition();
				}

			tableBlock.answerBlock = answerBlock;

			}

			blockLabel.text(answerBlock.getValue());
			tableBlock.name('answered');
			tableBlock.value = answerBlock.getValue();

			contentLayer.draw();
		}

		tableBlock.setWrong = function(){
			blockLabel.fill('red');
			contentLayer.draw();
		}

		tableBlock.add(rect);
		tableBlock.add(blockLabel);
		tableGroup.add(tableBlock);
		contentLayer.draw();

		function topRight(context) {
			context.beginPath();
			context.moveTo(x, y);
			context.lineTo(x + width - radius, y);
			context.quadraticCurveTo(x + width, y, x + width, y + radius, radius);
			context.lineTo(x + width, y + height);
			context.lineTo(x, y + height);
			context.lineTo(x, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function topLeft(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x + radius, y);
			context.quadraticCurveTo(x, y, x, y + radius, radius);
			context.lineTo(x, y + height);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
		function botLeft(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);
			context.lineTo(x, y + height-radius);
			context.quadraticCurveTo(x, y + height, x+radius, y + height, radius);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function botRight(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);
			context.lineTo(x, y + height);
			context.lineTo(x+width-radius, y + height);
			context.quadraticCurveTo(x + width, y + height, x + width, y + height - radius, radius);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function center(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);
			context.lineTo(x, y + height);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }

	}

    skinManager = new SkinManager('assets/skin', function(){
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    });

    function xmlLoadHandler(xml){

		console.log('xml data');
		console.log(xml);

    	initAspectRatio();

    	initWindowSize();

    	marginX = 50;
    	marginY = 10;

    	var containerPadding = 15;

    	var containerPaddingX = ($(window).width() / stage.scaler) * 0.1;

    	contentContainer = new Kinetic.Rect({
    		x: containerPaddingX,
    		y: containerPadding,
    		width: ($(window).width() / stage.scaler)  - (containerPaddingX << 1),
    		height: $(window).height() / stage.scaler - (containerPadding << 1),
    		fill: colorSheme.contentContainer.color,
    		stroke: colorSheme.contentContainer.stroke,
    		strokeWidth: colorSheme.contentContainer.strokeWidth
    	});

    	var innerContainerPadding = 15;

    	backgroundLayer.add(contentContainer);

    	//alert(contentContainer.width());

    	panelWidth = contentContainer.width() * 0.58;
		panelHeight = contentContainer.height() * 1/6;

    	//panelHeight = 100;

    	imageMaxWidth = contentContainer.width() - (innerContainerPadding << 1);
    	imageMaxHeight = contentContainer.height() - panelHeight - innerContainerPadding;

    	var root = $(xml).children('root');

    	levelManager.initRoute(root);

    	var imageCollection = $(root).children('image');

    	preloadImages(contentContainer.x() + innerContainerPadding, contentContainer.y() + innerContainerPadding, imageCollection, imageMaxWidth, imageMaxHeight);

    	contentLayer.setClip({
    		x: 0,
    		y: 0,
    		height: imageMaxHeight + contentContainer.y() + innerContainerPadding,
    		width: imageMaxWidth + contentContainer.x() + innerContainerPadding
    	})

    	initSlideControlls();
    }

    function initSlideControlls(){
    	slideControlls = new Kinetic.Group({
    		x: 0,
    		y: 0
    	});

    	slideControlls.leftArrow = createControllsArrow('prev');

    	slideControlls.rightArrow = createControllsArrow('next');

    	slideControlls.isLocked = false;

    	slideControlls.padding = 5;

    	slideControlls.showControlls = function(x, y, width, height){
    		if(slideControlls.isLocked) return;
    		slideControlls.leftArrow.x(x + slideControlls.padding);
    		slideControlls.rightArrow.x(x + width - slideControlls.rightArrow.width() - slideControlls.padding);

    		slideControlls.y(y + ((height >> 1) - (slideControlls.leftArrow.height() >> 1)));
    		slideControlls.show();
    	}

    	slideControlls.add(slideControlls.leftArrow);
    	slideControlls.add(slideControlls.rightArrow);

    	slideControlls.hide();
    	controllsLayer.add(slideControlls);
    }

    function createControllsArrow(type){
    	var buttonGroup = new Kinetic.Group({
    		x: 0,
    		y: 0
    	});

    	var imageObj = new Image();

    	var graphicSize = {
    		x: 35,
    		y: 46
    	}

    	buttonGroup.width(graphicSize.x);
    	buttonGroup.height(graphicSize.y);

    	imageObj.onload = function(){
    		var arrowButton = new Kinetic.Sprite({
				x: 0,
				y: 0,
				image: imageObj,
				animation: type,
				animations: {
					prev: [
					  // x, y, width, height
					  0, 0, graphicSize.x, graphicSize.y
					],
					next: [
					  // x, y, width, height
					  graphicSize.x, 0, graphicSize.x, graphicSize.y
					]
				},
				frameRate: 7,
				frameIndex: 0
			});

			buttonGroup.on(events[isMobile].click, type == 'prev' ? jumpToPrev : jumpToNext);
			//buttonGroup.on(events[isMobile].mouseover, hoverPointer);
			//buttonGroup.on(events[isMobile].mouseout, resetPointer);

			buttonGroup.add(arrowButton);
			//backgroundLayer.draw();
    	}

    	imageObj.src = skinManager.getGraphic('gallerySlideArrow.png');

    	return buttonGroup;
    }

    function buildNavigationPanel(x, y, width, height){
    	var panelGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	});

    	var tempRect = new Kinetic.Rect({
    		x: 0,
    		y: 0,
    		width: width,
    		height: height,
    		fill: 'white'
    	});
    	//panelGroup.add(tempRect);

    	var panelPadding = 15;

    	var tipSize = Math.min(panelHeight - (panelPadding << 1), width / 6);

    	tipGroup = new Kinetic.Group({
    		x: x + tipSize,
    		y: y + panelPadding
    	});

    	tipLayer.batchDraw.process();
    	tipLayer.draw.process();

    	tipGroup.defaultX = tipGroup.x();
    	tipGroup.defaultY = tipGroup.y();

    	var backgroundRect = new Kinetic.Rect({
    		x: 0,
    		y: 0,
    		width: width,
    		height: height,
    		fill: 'black',
    		cornerRadius: 8
    	});

    	tipClip = {
    		x: tipGroup.x() - 2,
    		y: tipGroup.y() - 2,
    		width: width - (tipGroup.x() - x) * 2,
    		height: height - (tipGroup.y() - y)
    	}

    	tipLayer.setClip(tipClip);

    	var spacer = 20;

    	var dx = 0;

    	tipGroup.isLocked = false;

    	tipGroup.getVisibleCount = function(){
    		return Math.floor((tipClip.width + spacer * 2) / (tipSize + spacer));
    	}

    	for(var i = 0; i < pictureCollection.length; i++){

    		var img = pictureCollection[i].image.getImage();

    		var scaler = calculateAspectRatio(img.width, img.height, tipSize, tipSize);

    		var imgSize = Math.min(img.width, img.height);

			var tipImage = new Kinetic.Image({
				x: dx,
				y: 0,
				image: getImage(img),
				width: tipSize,
				height: tipSize,
				srtoke: colorSheme.navigationPanel.regularTip.stroke,
				strokeWidth: colorSheme.navigationPanel.regularTip.strokeWidth
			});

			tipImage.crop({
				x: 0,
				y: 0,
				width: imgSize,
				height: imgSize
			});

			//tipImage.stroke('black');

			//tipImage.cache();

			//tipImage.cropHeight(50);
			//tipImage.cropWidth(50);

    		tipImage._index = i;
    		tipImage.tipCount = pictureCollection.length;

    		if(!isMobile){
	    		tipImage.on(events[isMobile].mouseover, function(){
	    			if(slideControlls.isVisible()){
		    			slideControlls.hide();
		    			controllsLayer.batchDraw();
		    		}
	    			hoverPointer();
	    		});
	    		tipImage.on(events[isMobile].mouseout, resetPointer);
	    	}

    		tipImage.on(events[isMobile].click, function(){
    			if(slideLock) return;
    			if(this._index == currentPicture._index) return;
    			if(tipGroup.isLocked) return;
    			tipGroup.isLocked = true;
    			//slideLock = true;

    			slideControlls.fromNavigation = true;

    			tipCollection[currentPicture._index].unGlow();
		    	tipCollection[this._index].glow();

		    	//tipLayer.batchDraw();

    			jumpTo(this._index);
    		})

    		dx += tipSize + spacer;

    		tipGroup.add(tipImage);

    		tipImage.glow = function(){
    			this.stroke(colorSheme.navigationPanel.selectedTip.stroke);
    			//this.tipItem.cache();
    		};

    		tipImage.unGlow = function(){
    			this.stroke(colorSheme.navigationPanel.regularTip.stroke);
    			//this.tipItem.cache();
    		};

    		tipCollection[i] = tipImage;
    	}

    	var tipWidth = dx + tipGroup.x();

    	if(tipWidth < width) tipGroup.x((width >> 1) - (tipWidth >> 1));

    	backgroundRect.opacity(0.8);

    	//panelGroup.add(backgroundRect);
    	//panelGroup.add(tipGroup);

    	/*tipGroup.cache({
    		x: 0,
    		y: 0,
    		width: tipWidth,
    		height: height
    	})*/

    	tipLayer.add(tipGroup);
    	tipLayer.draw();

    	var arrowSize = tipSize * 0.8;
    	var arrowY = tipSize - arrowSize + (panelPadding >> 1);

    	var leftArrow = createArrowButton(0, arrowY, 'prev', arrowSize, arrowSize);
    	var rightArrow = createArrowButton(width - 45, arrowY, 'next', arrowSize, arrowSize);

    	panelGroup.add(leftArrow);
    	panelGroup.add(rightArrow);

    	tipGroup.slideTo = function(index){
    		if(pictureCollection.length < this.getVisibleCount()) return;
    		tipGroup.x(tipGroup.defaultX - (tipSize + spacer) * index);
    	}

    	tipGroup.getCurrentIndex = function(){
    		return Math.abs(Math.floor((tipGroup.x() - tipClip.x) / (tipSize + spacer)));
    	}

    	//nextGraphic.src = 'assets/ui/nextButton.png';

    	backgroundLayer.add(panelGroup);
    	backgroundLayer.draw();
    }

    function nextArrowHandler(){
    	if(pictureCollection.length < tipGroup.getVisibleCount()) return;
    	var currentIndex = tipGroup.getCurrentIndex();

		var newIndex = currentIndex + tipGroup.getVisibleCount() - 1;

		if(newIndex > pictureCollection.length) return;

		tipGroup.slideTo(newIndex);

		tipLayer.draw();
    }

    function prevArrowHandler(){
    	if(pictureCollection.length < tipGroup.getVisibleCount()) return;
    	var currentIndex = tipGroup.getCurrentIndex();

		if(currentIndex == 0) return;

		var newIndex = currentIndex - tipGroup.getVisibleCount();

		if(newIndex < 0) newIndex = 0;

		tipGroup.slideTo(newIndex);

		tipLayer.draw();
    }

    function createArrowButton(x, y, type, width, height){
    	var imageObj = new Image();
    	var buttonGroup = new Kinetic.Group({x: x, y: y});

    	var graphicSize = {
    		x: 35,
    		y: 47
    	}

    	var scaler = calculateAspectRatioFit(graphicSize.x, graphicSize.y, width, height);

    	buttonGroup.width(graphicSize.x * scaler);
    	buttonGroup.height(graphicSize.y * scaler);

    	imageObj.onload = function(){
    		var arrowButton = new Kinetic.Sprite({
				x: 0,
				y: 0,
				image: imageObj,
				animation: type,
				animations: {
					prev: [
					  // x, y, width, height
					  0, 0, graphicSize.x, graphicSize.y
					],
					next: [
					  // x, y, width, height
					  graphicSize.x, 0, graphicSize.x, graphicSize.y
					]
				},
				frameRate: 7,
				frameIndex: 0,
				scale:{
					x: scaler,
					y: scaler
				}
			});

			buttonGroup.on(events[isMobile].click, type == 'prev' ? prevArrowHandler : nextArrowHandler);

			if(!isMobile){
				buttonGroup.on(events[isMobile].mouseover, hoverPointer);
				buttonGroup.on(events[isMobile].mouseout, resetPointer);
			}

			buttonGroup.add(arrowButton);
			backgroundLayer.draw();
    	}

    	imageObj.src = skinManager.getGraphic(colorSheme.navigationPanel.navigationArrow)

    	return buttonGroup;
    }

    function preloadImages(marginX, marginY, imageCollection, maxWidth, maxHeight){
    	var imageCount = imageCollection.length;

    	imageHolder = new Kinetic.Group({
    		x: marginX,
    		y: marginY
    	});

    	contentLayer.add(imageHolder);

    	$(imageCollection).each(function(index){
    		var imgObj = new Image()

    		imgObj.title = $(this).text();
    		imgObj._index = index;

    		if(typeof imgObj.title == 'undefined' || !imgObj.title) imgObj.title = '';

    		imgObj.onload = function(){
    			var imageContainer = new Kinetic.Group({
    				x: 0,
    				y: 0
    			});

    			var image = new Kinetic.Image({
    				x: 0,
    				y: 0,
    				image: getImage(this)
    			});

    			var scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, maxWidth, maxHeight);

    			image.scale({
    				x: scaler,
    				y: scaler
    			});

    			if(image.width() * scaler < maxWidth) image.x((maxWidth >> 1) - (image.width() * scaler >> 1));
    			if(image.height() * scaler < maxHeight) image.y((maxHeight >> 1) - (image.height() * scaler >> 1));

    			imageContainer.image = image;

    			var tipLabel = new Kinetic.Text({
    				text: this.title,
    				fontFamily: mainFont,
    				fill: 'white',
    				fontSize: 20,
    				x: image.x(),
    				y: 0,
    				align: 'center',
    				width: image.width() * scaler
    			});

    			var tipPadding = 10;

    			var tipRect = new Kinetic.Rect({
    				x: image.x(),
    				y: (image.y() + (image.height() * scaler)) - (tipLabel.height() + (tipPadding << 1)),
    				width: image.width() * scaler,
    				height: tipLabel.height() + (tipPadding << 1),
    				fill: 'black'
    			});

    			tipLabel.y(tipRect.y() + tipPadding);

    			tipRect.opacity(0.5);

    			imageContainer.add(image);

    			if(this.title != ''){
	    			imageContainer.add(tipRect);
	    			imageContainer.add(tipLabel);
	    		}

    			//imageGroup.add(imageContainer);

    			imageContainer._index = this._index;

    			imageContainer.getBounds = function(){
    				return {
    					x: imageContainer.x() + imageHolder.x() + imageContainer.image.x(),
    					y: imageContainer.y() + imageHolder.y() + imageContainer.image.y(),
    					width: imageContainer.image.width() * imageContainer.image.scaleX(),
    					height: imageContainer.image.height() * imageContainer.image.scaleY()
    				}
    			}

    			imageContainer.on(events[isMobile].click, function(){
    				//var mousePosition = getRelativePointerPosition();
    				var mousePosition = {
    					x: stage.getPointerPosition().x / stage.scaler//,
    					//y: stage.getPointerPosition().y / stage.scaler
    				}

    				var relativePosition = {
    					x: mousePosition.x - imageContainer.getBounds().x
    					//y: mousePosition.y - imageContainer.y() - imageHolder.y() - imageContainer.image.y()
    				}

    				var sideLimiter = {
    					x: (imageContainer.image.width() * imageContainer.image.scaleX()) >> 1//,
    					//y: (imageContainer.image.height() * imageContainer.image.scaleY()) >> 1
    				}

    				if(relativePosition.x < sideLimiter.x){
    					jumpToPrev();
    				} else{
    					jumpToNext();
    				}

    				//console.log(relativePosition.x, relativePosition.y);
    				//
    			});

    			if(!isMobile){
	    			imageContainer.on(events[isMobile].mouseover, function(){
	    				if(slideControlls.isVisible()) return;
	    				slideControlls.showControlls(
	    					imageContainer.x() + imageHolder.x() + imageContainer.image.x(),
	    					imageContainer.y() + imageHolder.y() + imageContainer.image.y(),
	    					imageContainer.image.width() * imageContainer.image.scaleX(),
	    					imageContainer.image.height() * imageContainer.image.scaleY());
	    				controllsLayer.batchDraw();
	    				hoverPointer();
	    			});

	    			imageContainer.on(events[isMobile].mouseout, function(){
	    				var mousePosition = {
	    					x: stage.getPointerPosition().x / stage.scaler//,
	    					//y: stage.getPointerPosition().y / stage.scaler
	    				}

	    				var relativePosition = {
	    					x: mousePosition.x - imageContainer.x() - imageHolder.x() - imageContainer.image.x()//,
	    					//y: mousePosition.y - imageContainer.y() - imageHolder.y() - imageContainer.image.y()
	    				}

	    				//console.log(relativePosition);

	    				if(relativePosition.x > 0
	    				&&
	    				relativePosition.x < (imageContainer.image.width() * imageContainer.image.scaleX())) return;

	    				slideControlls.hide();
	    				controllsLayer.batchDraw();
	    				resetPointer();
	    			});
	    		}

    			//imageContainer.hide();

    			pictureCollection[this._index] = imageContainer;

    			imageCount--;

    			if(imageCount == 0) onAssetsLoad();
    		}

    		imgObj.src = levelManager.getRoute($(this).attr('src'));
    	})
    }

    function getRelativePointerPosition() {
	    var pointer = stage.getPointerPosition();
	    var pos = stage.getPosition();
	    var offset = stage.getOffset();
	    var scale = stage.getScale();

	    return {
	        x : ((pointer.x / scale.x) - (pos.x / scale.x) + offset.x),
	        y : ((pointer.y / scale.y) - (pos.y / scale.y) + offset.y)
	    };
	}

    function onAssetsLoad(){
    	preLoader.hidePreloader();

    	//var dW = 450;

    	//buildNavigationPanel(dW >> 1, imageMaxHeight + marginY + 5, $(window).width() / stage.scaler - dW, panelHeight - 20);
    	//buildNavigationPanel((($(window).width() / stage.scaler) >> 1) - 250, imageMaxHeight + marginY + 5, 500, panelHeight - 20);
    	buildNavigationPanel(
    		contentContainer.x() + (contentContainer.width() >> 1) - (panelWidth >> 1),
    		contentContainer.y() + contentContainer.height() - panelHeight,
    		panelWidth,
    		panelHeight);

    	currentPicture = pictureCollection[0];

    	imageHolder.add(currentPicture);
    	//currentPicture.show(); //ANIMATION GOES HERE, MAH NIGGA

    	tipCollection[currentPicture._index].glow();

    	contentLayer.draw.process();

    	tipLayer.draw();
    	contentLayer.draw();
    }

    function jumpToNext(){
    	if(slideLock) return;
    	slideLock = true;
    	if(!isMobile){
	    	slideControlls.isLocked = true;
	    	if(slideControlls.isVisible()){
		    	slideControlls.hide();
		    	controllsLayer.batchDraw();
		    }
		}
    	var newIndex = currentPicture._index + 1;

    	//console.log(currentPicture._index)

    	if(newIndex == pictureCollection.length) newIndex = 0;

    	tipGroup.slideTo(newIndex);

    	tipCollection[currentPicture._index].unGlow();
    	tipCollection[newIndex].glow();

    	//tipLayer.batchDraw();

    	//setTimeout(function(){
    		jumpTo(newIndex);
    	//}, 50);
    }

    function jumpToPrev(){
    	if(slideLock) return;
    	slideLock = true;

    	if(!isMobile){
	    	slideControlls.isLocked = true;
	    	if(slideControlls.isVisible()){
		    	slideControlls.hide();
		    	controllsLayer.batchDraw();
		    }
		}
    	var newIndex = currentPicture._index - 1;

    	if(newIndex < 0) newIndex = pictureCollection.length - 1;

    	tipGroup.slideTo(newIndex);

    	tipCollection[currentPicture._index].unGlow();
    	tipCollection[newIndex].glow();

    	//tipLayer.batchDraw();

    	//setTimeout(function(){
    		jumpTo(newIndex);
    	//}, 50);
    }

    function jumpTo(index){
    	if(currentPicture._index == index) return;
    	currentPicture.isListening(false);
    	//slideLock = true;

    	//preventActions = true;
    	//currentPicture.hide();
    	currentPicture.defaultY = currentPicture.y();

    	var counter = 0;

    	var outTween = new Kinetic.Tween({
    		node: currentPicture,
    		y: $(window).height() / stage.scaler + 200,
    		duration: 0.6
    	});

    	outTween.prevImage = currentPicture;

    	outTween.onFinish = function(){
    		//this.node.hide();
    		this.node.y(this.node.defaultY);
    		outTween.prevImage.isListening(true);
    		outTween.prevImage.remove();
    		counter++;
    		if(counter == 2) onTweenFinish();

    		outTween.destroy();
    	}

    	currentPicture = pictureCollection[index];

    	//currentPicture.show();
    	currentPicture.isListening(false);
    	imageHolder.add(currentPicture);

    	currentPicture.defaultScale = {
    		x: currentPicture.scaleX(),
    		y: currentPicture.scaleY()
    	}

    	currentPicture.scale({
    		x: 0,
    		y: 0
    	});

    	currentPicture.defaultX = currentPicture.x();

    	if(currentPicture._index % 2 == 0) currentPicture.x(currentPicture.x() + currentPicture.width() * currentPicture.defaultScale.x);

    	var inTween = new Kinetic.Tween({
    		node: currentPicture,
    		scaleX: currentPicture.defaultScale.x,
    		scaleY: currentPicture.defaultScale.y,
    		duration: 0.5,
    		x: currentPicture.defaultX
    	});

    	inTween.onFinish = function(){
    		counter++;
    		if(counter == 2) onTweenFinish();
    		inTween.destroy();
    	}

    	outTween.play();

    	setTimeout(function(){
    		inTween.play();
    	}, 5);

    	//contentLayer.draw();

    	function onTweenFinish(){
    		counter = 0;
    		setTimeout(function(){
    			slideControlls.isLocked = false;
    			slideLock = false;
    			currentPicture.isListening(true);
    			tipGroup.isLocked = false;
		    	tipLayer.batchDraw();

		    	if(!slideControlls.isVisible() && !slideControlls.fromNavigation && !isMobile){
		    		var bounds = currentPicture.getBounds();
		    		slideControlls.showControlls(bounds.x, bounds.y, bounds.width, bounds.height);
		    		controllsLayer.batchDraw();
		    	}

		    	if(slideControlls.fromNavigation) slideControlls.fromNavigation = false;
    		}, 50);
    	}
    }

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
     */
    function createHead(title, question){
        //$('#mainTitle').html(title);
        //$('#question').html(question);

        var mainTitle = new Kinetic.Text({
            x: aspectRatio == 1 ? 60 : 50 + contentMargin.x,
            y: contentMargin.y,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            fill: 'black',
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 20 + contentMargin.x,
            y: contentMargin.y,
            fill: 'green',
            width: aspectRatio == 1 ? 28 : mainTitle.fontSize(),
            height: aspectRatio == 1 ? 26 : mainTitle.fontSize() - 2
        })


        backgroundLayer.add(mainTitle);
        backgroundLayer.add(mainTitleRect);

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: aspectRatio == 1 ? 40 : 30 + contentMargin.x,
                y: mainTitle.height() + 12 + contentMargin.y,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: 'black'
            });

            var questionRect = new Kinetic.Rect({
                x: 20 + contentMargin.x,
                y: questionLabel.y(),
                fill: 'brown',
                width: aspectRatio == 1 ? 8 : 4,
                height: aspectRatio == 1 ? 18 : questionLabel.fontSize()
            })

            backgroundLayer.add(questionLabel);
            backgroundLayer.add(questionRect);

        }

        backgroundLayer.draw();
    }

    function buildColumns(columns, wrongWords){
    	var wordArray = [];
    	var titles = [];
    	var answers = [];

    	var currentRow;
    	var currentString;
    	var premadeLabel;
    	var rowStrArray = [];

    	colCount = columns.length;
    	rowCount = columns.eq(0).children('row').length;

    	var answerColCount = isHorizontal ? colCount : 2;

    	var horizontalElementsCount = isHorizontal ? colCount : colCount + answerColCount;

    	var horizontalAnswerIndentSpace = (answerColCount - 1) * horizontalAnswerSpacer;

    	var maxWidth = ($(window).width() / stage.scaler - horizontalElementsCount * textIndentX - answerGroupSpacer - marginLeft * 2 - (isHorizontal ? 0 : horizontalAnswerIndentSpace)) / horizontalElementsCount;
    	var maxHeight = 0;

    	var premadeLabel;

    	columns.each(function(colIndex){

    		currentRow = $(this).children('row');
    		answers[colIndex] = [];
    		titles[colIndex] = [];

    		currentRow.each(function(rowIndex){

    			currentString = $(this).text();

    			if(currentString[0]=='#'){

    				currentString = currentString.substr(1, currentString.length-1);

					rowStrArray = currentString.split('~');
					var strCount = rowStrArray.length>1?rowStrArray.length-1:1;

    				answers[colIndex][rowIndex] = rowStrArray;

					for(var i = 0; i<strCount; i++){
						if(!(rowStrArray[i] in wordArray)){
							premadeLabel = new Kinetic.Text({ //текстовая метка
						        x: 0,
						        y: 0,
						        text: rowStrArray[i],
						        fontSize: xmlFontSize,
						        fontFamily: mainFont,
						        fill: 'black',
						        align: 'center',
						        width: maxWidth
						    });

						    if(premadeLabel.height() > maxHeight) maxHeight = premadeLabel.height();


							wordArray[rowStrArray[i]] = premadeLabel;
						}
					}

					titles[colIndex][rowIndex] = 'active';

    			} else{
    				titles[colIndex][rowIndex] = currentString;
    			}
    		})
    	});

    	if(wrongWords!=''){
    		wrongWords = wrongWords.substr(1, wrongWords.length-1);
    		wrongArray = wrongWords.split('#');
    		for(i = 0; i<wrongArray.length; i++){
    			if(!(wrongArray[i] in wordArray)){
					premadeLabel = new Kinetic.Text({ //текстовая метка
				        x: 0,
				        y: 0,
				        text: wrongArray[i],
				        fontSize: xmlFontSize,
				        fontFamily: mainFont,
				        fill: 'black',
				        align: 'center',
				        width: maxWidth
				    });

				    if(premadeLabel.height()>maxHeight) maxHeight = premadeLabel.height();


					wordArray[wrongArray[i]] = premadeLabel;
				}
    		}
    	}

    	maxItemWidth = maxWidth + textIndentX;
    	maxItemHeight = maxHeight + textIndentY;

    	var centerPositionY = (($(window).height() / stage.scaler) >> 1) - ((rowCount * maxItemHeight) >> 1);

    	createTable(marginLeft + (horizontalAnswerIndentSpace >> 1), !isHorizontal ? centerPositionY : marginTop, titles, answers);

    	var answerBoxesPosition = {};

    	answerBoxesPosition.x = isHorizontal ? marginLeft : (marginLeft + colCount * maxItemWidth + answerGroupSpacer);
    	//answerBoxesPosition.y = isHorizontal ? marginTop + rowCount * maxItemHeight + answerGroupSpacer : marginTop;
    	answerBoxesPosition.y = isHorizontal ? marginTop + rowCount * maxItemHeight + answerGroupSpacer : centerPositionY;

    	buildAnswerBoxes(answerBoxesPosition.x, answerBoxesPosition.y, wordArray, answerColCount);
    }

    function buildAnswerBoxes(x, y, wordArray, boxColCount){
    	var dy = y;
    	var dx = x;
    	var counter = 0;
    	for(label in wordArray){

    		createAnswerBlock(dx, dy, maxItemWidth, maxItemHeight, wordArray[label]);

    		counter++;

    		if(counter != boxColCount){
    			dx += maxItemWidth+horizontalAnswerSpacer;
    		} else{
    			counter = 0;
    			dx = x;
    			dy += maxItemHeight+verticalAnswerSpacer;
    		}
    	}
    }

    function createTable(x, y, titles, answers){

    	for(var i = 0; i<colCount; i++){
    		for(var j = 0; j<rowCount; j++){

    			createTableBlock(x+maxItemWidth*i, y+maxItemHeight*j, maxItemWidth, maxItemHeight, calculateBoxType(i, j, colCount-1, rowCount-1), titles[i][j], answers[i][j])

    		}
    	}
    }

    function calculateBoxType(i, j, colCount, rowCount){
    	if(i==0&&j==0){
    		return 'topLeft';
    	}
    	if(j==0&&i==colCount){
    		return 'topRight'
    	}
    	if(j==rowCount&&i==0){
    		return 'botLeft';
    	}
    	if(j==rowCount&&i==colCount){
    		return 'botRight';
    	}

    	return 'center';
    }

    function answerButtonHandler(evt){
    	var answered = tableGroup.find('.answered');
    	var isCorrect = (colCount * rowCount) - colCount == answered.length ? true : false;

    	for(var i = 0; i<answered.length; i++){
    		if(!answered[i].check()){
    			answered[i].setWrong();
    			isCorrect = false;
    		}
    	}

    	showResultLabel($(window).width() / stage.scaler - 385, $(window).height() / stage.scaler - 70, isCorrect);

    	var button = evt.targetNode.getParent();

    	backgroundLayer.find('#reset')[0].hide();

    	button.setText('Ещё раз');
    	button.off(events[isMobile].click);
    	button.on(events[isMobile].click, clear);
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

	function calculateAspectRatio(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.max(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback){

    	var buttonGroup = new Kinetic.Group({ x:x, y:y });
    	var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
	        x: 0,
	        y: 0,
	        width: width,
	        height: height,
	        fillLinearGradientStartPoint: {x:width/2, y:0},
	        fillLinearGradientEndPoint: {x:width/2,y:height},
	        fillLinearGradientColorStops: [0, '#FFC23F', 1, '#FF9200']
	      });

    	var label = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: 4,
	        width: buttonRectangle.width(),
	        height: buttonRectangle.height(),
	        text: textLabel,
	        fontSize: 20,
	        fontFamily: mainFont,
	        fill: 'black',
	        align: 'center'
	      });

    	if(!isMobile){
	    	buttonGroup.on(events[isMobile].mouseover, hoverPointer);
	    	buttonGroup.on(events[isMobile].mouseout, resetPointer);
	    }

    	/*Определяет новую текстовую метку кнопки
    	@param {string} value новое значение метки**/
    	buttonGroup.setText = function(value){
    		label.text(value);
    		backgroundLayer.draw();
    	};

    	buttonGroup.add(buttonRectangle);
    	buttonGroup.add(label);
    	backgroundLayer.add(buttonGroup);
    	backgroundLayer.draw();

    	buttonGroup.on(events[isMobile].click, callback);
    	/*label.on("click", function(evt){ //костыль вместо полноценного потока событий
    		evt.targetNode = buttonRectangle;
    		buttonRectangle.fire(events[isMobile].click, evt);
    	});*/
		return buttonGroup;
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            }

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
                backgroundLayer.draw();
            }

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        }

        imgObj.src = 'assets/ui/preLoader.png'

        return preloaderObject;
    }

    function showResultLabel(x, y, isWin){
    	var label = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	backgroundLayer.draw();
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

});