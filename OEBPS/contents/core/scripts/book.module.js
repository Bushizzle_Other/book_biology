﻿/*
 * ----------------------------- JSTORAGE -------------------------------------
 * Simple local storage wrapper to save data on the browser side, supporting
 * all major browsers - IE6+, Firefox2+, Safari4+, Chrome4+ and Opera 10.5+
 *
 * Author: Andris Reinman, andris.reinman@gmail.com
 * Project homepage: www.jstorage.info
 *
 * Licensed under Unlicense:
 *
 * This is free and unencumbered software released into the public domain.
 *
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 *
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * For more information, please refer to <http://unlicense.org/>
 */

/* global ActiveXObject: false */
/* jshint browser: true */
(function(){function C(){var a="{}";if("userDataBehavior"==f){g.load("jStorage");try{a=g.getAttribute("jStorage")}catch(b){}try{r=g.getAttribute("jStorage_update")}catch(c){}h.jStorage=a}D();x();E()}function u(){var a;clearTimeout(F);F=setTimeout(function(){if("localStorage"==f||"globalStorage"==f)a=h.jStorage_update;else if("userDataBehavior"==f){g.load("jStorage");try{a=g.getAttribute("jStorage_update")}catch(b){}}if(a&&a!=r){r=a;var l=p.parse(p.stringify(c.__jstorage_meta.CRC32)),k;C();k=p.parse(p.stringify(c.__jstorage_meta.CRC32));
	var d,n=[],e=[];for(d in l)l.hasOwnProperty(d)&&(k[d]?l[d]!=k[d]&&"2."==String(l[d]).substr(0,2)&&n.push(d):e.push(d));for(d in k)k.hasOwnProperty(d)&&(l[d]||n.push(d));s(n,"updated");s(e,"deleted")}},25)}function s(a,b){a=[].concat(a||[]);var c,k,d,n;if("flushed"==b){a=[];for(c in m)m.hasOwnProperty(c)&&a.push(c);b="deleted"}c=0;for(d=a.length;c<d;c++){if(m[a[c]])for(k=0,n=m[a[c]].length;k<n;k++)m[a[c]][k](a[c],b);if(m["*"])for(k=0,n=m["*"].length;k<n;k++)m["*"][k](a[c],b)}}function v(){var a=(+new Date).toString();
	if("localStorage"==f||"globalStorage"==f)try{h.jStorage_update=a}catch(b){f=!1}else"userDataBehavior"==f&&(g.setAttribute("jStorage_update",a),g.save("jStorage"));u()}function D(){if(h.jStorage)try{c=p.parse(String(h.jStorage))}catch(a){h.jStorage="{}"}else h.jStorage="{}";z=h.jStorage?String(h.jStorage).length:0;c.__jstorage_meta||(c.__jstorage_meta={});c.__jstorage_meta.CRC32||(c.__jstorage_meta.CRC32={})}function w(){if(c.__jstorage_meta.PubSub){for(var a=+new Date-2E3,b=0,l=c.__jstorage_meta.PubSub.length;b<
l;b++)if(c.__jstorage_meta.PubSub[b][0]<=a){c.__jstorage_meta.PubSub.splice(b,c.__jstorage_meta.PubSub.length-b);break}c.__jstorage_meta.PubSub.length||delete c.__jstorage_meta.PubSub}try{h.jStorage=p.stringify(c),g&&(g.setAttribute("jStorage",h.jStorage),g.save("jStorage")),z=h.jStorage?String(h.jStorage).length:0}catch(k){}}function q(a){if("string"!=typeof a&&"number"!=typeof a)throw new TypeError("Key name must be string or numeric");if("__jstorage_meta"==a)throw new TypeError("Reserved key name");
	return!0}function x(){var a,b,l,k,d=Infinity,n=!1,e=[];clearTimeout(G);if(c.__jstorage_meta&&"object"==typeof c.__jstorage_meta.TTL){a=+new Date;l=c.__jstorage_meta.TTL;k=c.__jstorage_meta.CRC32;for(b in l)l.hasOwnProperty(b)&&(l[b]<=a?(delete l[b],delete k[b],delete c[b],n=!0,e.push(b)):l[b]<d&&(d=l[b]));Infinity!=d&&(G=setTimeout(x,Math.min(d-a,2147483647)));n&&(w(),v(),s(e,"deleted"))}}function E(){var a;if(c.__jstorage_meta.PubSub){var b,l=A,k=[];for(a=c.__jstorage_meta.PubSub.length-1;0<=a;a--)b=
	c.__jstorage_meta.PubSub[a],b[0]>A&&(l=b[0],k.unshift(b));for(a=k.length-1;0<=a;a--){b=k[a][1];var d=k[a][2];if(t[b])for(var n=0,e=t[b].length;n<e;n++)try{t[b][n](b,p.parse(p.stringify(d)))}catch(g){}}A=l}}var y=window.jQuery||window.$||(window.$={}),p={parse:window.JSON&&(window.JSON.parse||window.JSON.decode)||String.prototype.evalJSON&&function(a){return String(a).evalJSON()}||y.parseJSON||y.evalJSON,stringify:Object.toJSON||window.JSON&&(window.JSON.stringify||window.JSON.encode)||y.toJSON};if("function"!==
	typeof p.parse||"function"!==typeof p.stringify)throw Error("No JSON support found, include //cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js to page");var c={__jstorage_meta:{CRC32:{}}},h={jStorage:"{}"},g=null,z=0,f=!1,m={},F=!1,r=0,t={},A=+new Date,G,B={isXML:function(a){return(a=(a?a.ownerDocument||a:0).documentElement)?"HTML"!==a.nodeName:!1},encode:function(a){if(!this.isXML(a))return!1;try{return(new XMLSerializer).serializeToString(a)}catch(b){try{return a.xml}catch(c){}}return!1},
	decode:function(a){var b="DOMParser"in window&&(new DOMParser).parseFromString||window.ActiveXObject&&function(a){var b=new ActiveXObject("Microsoft.XMLDOM");b.async="false";b.loadXML(a);return b};if(!b)return!1;a=b.call("DOMParser"in window&&new DOMParser||window,a,"text/xml");return this.isXML(a)?a:!1}};y.jStorage={version:"0.4.12",set:function(a,b,l){q(a);l=l||{};if("undefined"==typeof b)return this.deleteKey(a),b;if(B.isXML(b))b={_is_xml:!0,xml:B.encode(b)};else{if("function"==typeof b)return;
	b&&"object"==typeof b&&(b=p.parse(p.stringify(b)))}c[a]=b;for(var k=c.__jstorage_meta.CRC32,d=p.stringify(b),g=d.length,e=2538058380^g,h=0,f;4<=g;)f=d.charCodeAt(h)&255|(d.charCodeAt(++h)&255)<<8|(d.charCodeAt(++h)&255)<<16|(d.charCodeAt(++h)&255)<<24,f=1540483477*(f&65535)+((1540483477*(f>>>16)&65535)<<16),f^=f>>>24,f=1540483477*(f&65535)+((1540483477*(f>>>16)&65535)<<16),e=1540483477*(e&65535)+((1540483477*(e>>>16)&65535)<<16)^f,g-=4,++h;switch(g){case 3:e^=(d.charCodeAt(h+2)&255)<<16;case 2:e^=
	(d.charCodeAt(h+1)&255)<<8;case 1:e^=d.charCodeAt(h)&255,e=1540483477*(e&65535)+((1540483477*(e>>>16)&65535)<<16)}e^=e>>>13;e=1540483477*(e&65535)+((1540483477*(e>>>16)&65535)<<16);k[a]="2."+((e^e>>>15)>>>0);this.setTTL(a,l.TTL||0);s(a,"updated");return b},get:function(a,b){q(a);return a in c?c[a]&&"object"==typeof c[a]&&c[a]._is_xml?B.decode(c[a].xml):c[a]:"undefined"==typeof b?null:b},deleteKey:function(a){q(a);return a in c?(delete c[a],"object"==typeof c.__jstorage_meta.TTL&&a in c.__jstorage_meta.TTL&&
delete c.__jstorage_meta.TTL[a],delete c.__jstorage_meta.CRC32[a],w(),v(),s(a,"deleted"),!0):!1},setTTL:function(a,b){var l=+new Date;q(a);b=Number(b)||0;return a in c?(c.__jstorage_meta.TTL||(c.__jstorage_meta.TTL={}),0<b?c.__jstorage_meta.TTL[a]=l+b:delete c.__jstorage_meta.TTL[a],w(),x(),v(),!0):!1},getTTL:function(a){var b=+new Date;q(a);return a in c&&c.__jstorage_meta.TTL&&c.__jstorage_meta.TTL[a]?(a=c.__jstorage_meta.TTL[a]-b)||0:0},flush:function(){c={__jstorage_meta:{CRC32:{}}};w();v();s(null,
	"flushed");return!0},storageObj:function(){function a(){}a.prototype=c;return new a},index:function(){var a=[],b;for(b in c)c.hasOwnProperty(b)&&"__jstorage_meta"!=b&&a.push(b);return a},storageSize:function(){return z},currentBackend:function(){return f},storageAvailable:function(){return!!f},listenKeyChange:function(a,b){q(a);m[a]||(m[a]=[]);m[a].push(b)},stopListening:function(a,b){q(a);if(m[a])if(b)for(var c=m[a].length-1;0<=c;c--)m[a][c]==b&&m[a].splice(c,1);else delete m[a]},subscribe:function(a,
																																																																																																																															 b){a=(a||"").toString();if(!a)throw new TypeError("Channel not defined");t[a]||(t[a]=[]);t[a].push(b)},publish:function(a,b){a=(a||"").toString();if(!a)throw new TypeError("Channel not defined");c.__jstorage_meta||(c.__jstorage_meta={});c.__jstorage_meta.PubSub||(c.__jstorage_meta.PubSub=[]);c.__jstorage_meta.PubSub.unshift([+new Date,a,b]);w();v()},reInit:function(){C()},noConflict:function(a){delete window.$.jStorage;a&&(window.jStorage=this);return this}};(function(){var a=!1;if("localStorage"in
	window)try{window.localStorage.setItem("_tmptest","tmpval"),a=!0,window.localStorage.removeItem("_tmptest")}catch(b){}if(a)try{window.localStorage&&(h=window.localStorage,f="localStorage",r=h.jStorage_update)}catch(c){}else if("globalStorage"in window)try{window.globalStorage&&(h="localhost"==window.location.hostname?window.globalStorage["localhost.localdomain"]:window.globalStorage[window.location.hostname],f="globalStorage",r=h.jStorage_update)}catch(k){}else if(g=document.createElement("link"),
		g.addBehavior){g.style.behavior="url(#default#userData)";document.getElementsByTagName("head")[0].appendChild(g);try{g.load("jStorage")}catch(d){g.setAttribute("jStorage","{}"),g.save("jStorage"),g.load("jStorage")}a="{}";try{a=g.getAttribute("jStorage")}catch(m){}try{r=g.getAttribute("jStorage_update")}catch(e){}h.jStorage=a;f="userDataBehavior"}else{g=null;return}D();x();"localStorage"==f||"globalStorage"==f?"addEventListener"in window?window.addEventListener("storage",u,!1):document.attachEvent("onstorage",
	u):"userDataBehavior"==f&&setInterval(u,1E3);E();"addEventListener"in window&&window.addEventListener("pageshow",function(a){a.persisted&&u()},!1)})()})();

var glSelection = false;

$(function () {
	function log() {
		// console.log.apply(console, arguments);
	}


	// time bomb
	//	
	
	var isMobile = {
	    Android: function() {
	        return /Android/i.test(navigator.userAgent);
	    },
	    BlackBerry: function() {
	        return /BlackBerry/i.test(navigator.userAgent);
	    },
	    iOS: function() {
	        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
	    },
	    Windows: function() {
	        return /IEMobile/i.test(navigator.userAgent);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
	    }
	};

    function isSafari() {
	    return /^((?!chrome).)*safari/i.test(navigator.userAgent);
	}

	function OC3Book(containers) {

		// набор функций для работы с разными режимами
		var page_variant = {
			next_page: function() {
				this.page_variant.set_page.call(this, this.page + 1);
				this.page_variant.show_page.call(this);
			},
			prev_page: function() {
				this.page_variant.set_page.call(this, this.page - 1);
				this.page_variant.show_page.call(this);
			},
			show_page: function() {
				$('.active-page').removeClass("active-page");
				this.book.showPage(this.page * 2 + 2);
				$(this.book.pages[this.page * 2 + 2]).addClass("active-page"); 
				if(contents_panel.validateControlls) contents_panel.validateControlls('navigation');
				this.set_location_hash();
			},
			set_page: function(p) { 
				p = parseInt(p);
				var l = (this.book.pages.length / 2) | 0;
				if (p < 0) { this.page = -1; }
				else if (p >= l) { this.page = l; }
				else { this.page = p; } 
			},
			page_count: function() {  return  (this.book.pages.length / 2) | 0; },
			get_page: function(i) { return this.book.pages[i * 2 + 2]; }
		};

		this.variant = {
			single_page: Object.create(page_variant), 
			landscape:   Object.create(page_variant),
			double_page: Object.create({
				next_page: function() {
					this.page_variant.set_page.call(this, this.page + 2);
					this.page_variant.show_page.call(this);
				},
				prev_page: function() {
					this.page_variant.set_page.call(this, this.page - 2);
					this.page_variant.show_page.call(this);
				},
				show_page: function() { 
					$('.active-page').removeClass("active-page");
					this.book.gotoPage(parseInt(this.page) + 1);
					if(contents_panel.validateControlls) contents_panel.validateControlls('navigation');
					this.set_location_hash();
				},
				set_page: function(p) { 
					p = parseInt(p);
					var l = (this.book.pages.length) | 0;
					if (p < 0) { this.page = -1; }
					else if (p >= l) { this.page = l; }
					else { this.page = p; } 
				},
				page_count: function() { return  (this.book.pages.length) | 0; },
				get_page: function(i) { return this.book.pages[i + 1]; },
			}),
		};

		this.set_variant_callback = containers.on_set_variant;
		this.page_callback = containers.on_page;
		this.load_callback = containers.on_load;

		// книги для каждого вида

		this.books = {
			double_page: null,
			single_page: null,
			landscape:   null,
		}

		this.containers = containers;

		this.variantConfig = {
			"double_page":{
				flipSound: false,
				height : 1024
				,width  : 1456
				,centeredWhenClosed : true
				,hardcovers : true
				,turnPageDuration : 10
				,pageNumbers : false 
				,updateBrowserURL: false
				,touchEnabled : false
				,keyboardNavigation : false
	 			,zoomLevel : this.zoom_level
	        	,zoomMax : this.zoom_max
	        	,zoomMin : this.zoom_min
	        	,zoomStep : this.zoom_step
				,scaleToFit: "#container"
				,thumbnailsPosition : 'bottom'
				,thumbnailsParent: '#thumbnailsContainer',
				disableFlipAnimation: false,
				disableZoomDrag: isMobile.any()
			},
			"single_page":{
				flipSound: false,
				height : 1024
				,width  :  768 // ширина одной страницы (НЕ) будет 1024
				,centeredWhenClosed : true
				,hardcovers : true
				,turnPageDuration : 0
				,pageNumbers : false 
				,updateBrowserURL: false
				,touchEnabled : false
				,keyboardNavigation : false
	 			,zoomLevel : this.zoom_level
	        	,zoomMax : this.zoom_max
	        	,zoomMin : this.zoom_min
	        	,zoomStep : this.zoom_step
				,scaleToFit: "#container"
				,thumbnailsPosition : 'bottom'
				,thumbnailsParent: '#thumbnailsContainer',
				disableFlipAnimation: true,
				disableZoomDrag: isMobile.any()
			},
			"landscape":{
				flipSound: false,
				height : 768
				,width  : 1024 
				,centeredWhenClosed : true
				,hardcovers : true
				,turnPageDuration : 0
				,pageNumbers : false 
				,updateBrowserURL: false
				,touchEnabled : false
				,keyboardNavigation : false
	 			,zoomLevel : this.zoom_level
	        	,zoomMax : this.zoom_max
	        	,zoomMin : this.zoom_min
	        	,zoomStep : this.zoom_step
				,scaleToFit: "#container"
				,thumbnailsPosition : 'bottom'
				,thumbnailsParent: '#thumbnailsContainer',
				disableFlipAnimation:true,
				disableZoomDrag: isMobile.any()
			}
		}

		this.book = null;            // выбранная книга
		this.page_variant = null;    // выбраный вариант (single_page; double_page; landscape; auto)
		this.variant_name = "";      // название варианта 
		this.page = 0;               // выбранная страница
		this.thumbnails = $('<div id="thumbnailsContainer"></div>');      // контейнер для превью
		$('body').append(this.thumbnails);
		this.zoom_level = 1;
        this.zoom_max = 2;
        this.zoom_min = 1;
        this.zoom_step = 0.05;

        this.keyboard_navigation = {
            prev: 37,
            next: 39
        },

		this.book_name = location.pathname;  // название книги

		// загружаем закладки

		//var lsTemp = $.jStorage.get(location.pathname);
		//var ls = JSON.parse(lsTemp);
		//var ls = {};
		var ls = JSON.parse($.jStorage.get(location.pathname));
		console.log(ls);
		this.bookmarks = (ls && ls.bookmarks ? ls.bookmarks : {});
		

		this.location_hash_set_to = "";

		if (this.keyboard_navigation) {
			$(document).on("keydown", function(a) {
				if ($(a.target).filter("input, textarea, select").length == 0) { 
					a.which === this.keyboard_navigation.prev && this.prev();
					a.which === this.keyboard_navigation.next && this.next();
				}
				if(contents_panel.updateContent) contents_panel.updateContent();
        	}.bind(this));
		}

		$(window).unbind('hashchange'); // delete Bookhtml5 triger
		$(window).on("hashchange", function() {
            var h = window.location.hash;
            if (h === "" || h === "#" || h === '#'+this.location_hash_set_to) return;
            this.from_location_hash(); 
        }.bind(this));

        this.load_callback && this.load_callback(this);

	} // <-- end function OC3Book

	OC3Book.prototype.page_numbering = function(book, variant) {
		one_page_variant = (variant != 'double_page');
		var numbered_pages = [1,-2]; 
		var first_page_number = 0;
		var pages_length = book.pages.length;
		var b = numbered_pages[0];
		one_page_variant && (b%2) && (b++);
		var e = numbered_pages[1];
		e < 0 && (e = pages_length + e);

		$(".BookHtml5-page-number", book.elem).remove();
		for (var n = first_page_number; b <= e; (one_page_variant ? b+=2 : b++)) {
			var a = $(".BookHtml5-page-content", book.pages[b]);
			var a = $('<div class="BookHtml5-page-number"></div>').appendTo(a);
			$(".BookHtml5-page-content", book.pages[b]).append('<div id="note-container-' + variant + '-' + n + '" class="BookHtml5-note"><ul></ul></div>');
			a.html( n == 0 ? '' : n);
			n++;
		}
	}

	OC3Book.prototype.set_variant = function (v, a) {
		$('#page-preloader').show(); //this maybe overcalled because of mixed logic (TODO: seperate it later!)

		if (!(v in this.variant)) { v = isMobile.any() ? "auto" : "double_page"; } // variant by default

		if (v === 'auto') {
			this.variant_name = v; 
			this.update_variant();
			return;
		}

		if(this.books[v] == null){
			var self = this;

			self.initializeVariant(v, function(){
				self.set_variant(v, a);
			});
			return;
		}

		if (!this.animate) {
			log("animate");
			this.animate = true;
			this.page_variant = this.variant[v];

			var was_thumds = this.book ? $(".BookHtml5-thumbnails:visible").length > 0 : false;

			var __show_book = function (variant) {
				this.book = this.books[variant];
				!a && (this.variant_name = v); // update page_variant if not 'auto'
				this.zoom_at(this.zoom_level);
				this.zoom_level = this.book.zoomLevel;
				this.page_variant.show_page.call(this);
				$(this.book.elem).show().removeClass("hidden");
				this.set_variant_callback && this.set_variant_callback(this.variant_name);
				this.set_location_hash();
				this.animate = null;
				$('.content-book-text').css('padding-top', '10px');
				$('.back-img').css('display', 'none');

				// landscape shadow fix
				if (this.page_variant === this.variant['landscape']) {
					this.book.positionBookShadow();
				}
				this.rebuild_thumbnails();				
				if (was_thumds) {
					this.book.thumbnailsContainer && this.book.showThumbnails();
				}
				$('#page-preloader').hide();
			};

			if (this.book) {
				for (var i in this.books) {
					if(this.books[i]) this.books[i].thumbnailsContainer && this.books[i].hideThumbnails();
				}
				var self = this;
				$(self.book.elem).hide();
				setTimeout(function(){
					__show_book.bind(self, v)();
				}, 500);
				
			} else {
				__show_book.call(this, v);
			}
		}
	}

	OC3Book.prototype.initializeVariant = function(variant, callback){
		var self = this;

		var variantDataVariable = variant + '_data';

		importJS(
			'variants/' + variant + '.js',
			variantDataVariable, function(variantData){
				$('#book').append(variantData);
				variantData = '';

				$(self.containers[variant].container).BookHtml5(
					self.variantConfig[variant]
				).css({'display':'none', 'margin':'auto'});

				self.books[variant] = $.BookHtml5(self.containers[variant].container);

				self.page_numbering(self.books[variant], variant);
				if(!self.settings){
					oc3book.set_settings(window.BookSettings);
					window.BookSettings = null;
				}

				if(!self.book){
					contents_panel.make_contents();
					contents_panel.update_bookmarks();
					if(self.bindNoteEvents) self.bindNoteEvents();
				}

				oc3book.update_bookmarks(function (b) {
					$('#select-bookmarks').append('<option value="'+b+'">'+b+'</option>');
				});

				//всем урокам записываем состояние по-умолчанию "закрыто"
				$('.lesson').data('state', 'close');
				$('.lesson, .lesson-title, .lesson-logo').css('cursor', 'pointer');

				$('.pages_html').each(function(){
					$(this).css({display:'block'});
				})

				window.bindEventListeners();

				if(callback) callback();
			}
		);
	}


	OC3Book.prototype.update_variant = function() {
		log('update_variant');
		try {
			var width, height;	
			if (document.body && document.body.offsetWidth) {
				width = document.body.offsetWidth;
				height = document.body.offsetHeight;
			}
			if (document.compatMode=='CSS1Compat' &&
			    document.documentElement &&
			    document.documentElement.offsetWidth ) {
				width = document.documentElement.offsetWidth;
				height = document.documentElement.offsetHeight;
			}
			if (window.innerWidth && window.innerHeight) {
				width = window.innerWidth;
				height = window.innerHeight;
			}

			if (this.variant_name === 'auto') {
				if (width && height) {
					if (width > height) {
						this.set_variant("landscape", true);
					} else {
						this.set_variant("single_page", true);
					}
				} else if (orientation !== undefined) {
					
					if (orientation == 0 || orientation == 180) {
						this.set_variant("landscape", true);
					} else if (orientation == 90 || orientation == -90) {
						this.set_variant("single_page", true);
					}
				}
			} else{
				$('#page-preloader').hide();
			}
		}
		catch(err) {
			log(err);
			this.set_variant("double_page", true);
		}

	}

	OC3Book.prototype.next = function() {
		if (!this.book.animating && !this.animate) {
			this.animate = true;
			this.page_variant.next_page.call(this);
			this.page_callback && this.page_callback(this.page);
			log("go next page", this.page);
			this.animate = false;
		}
	}

	OC3Book.prototype.prev = function() {
		if (!this.book.animating && !this.animate) {
			this.animate = true;
			this.page_variant.prev_page.call(this);
			this.page_callback && this.page_callback(this.page);
			log("go prev page", this.page);
			this.animate = false;
		}
	}

	OC3Book.prototype.goto_page = function(p) {
		if (!this.book.animating) {
			this.page_variant.set_page.call(this, p);
			this.page_variant.show_page.call(this);
			this.page_callback && this.page_callback(this.page);
			log("go to page", this.page);
		}
	}

	OC3Book.prototype.get_page = function(i) {
		return this.page_variant.get_page.call(this, i);
	}

	OC3Book.prototype.page_count = function() {
		return this.page_variant.page_count.call(this);
	}

	OC3Book.prototype.save_bookmarks = function() {
		try 
		{
			var ls = JSON.parse($.jStorage.get(this.book_name));
			ls = ls ? ls : {};
			ls.bookmarks = this.bookmarks;
			$.jStorage.set(this.book_name, JSON.stringify(ls));
		}
		catch (error) 
	    {
	        return false;
	    }

		return true;
	}

	OC3Book.prototype.update_bookmarks = function(f) {
		for (var i in this.bookmarks) {
			this.bookmarks[i] !== null && f(i, this.bookmarks[i]);
		}
	}

	OC3Book.prototype.add_bookmark = function() {
		var b = $('#page_bookmark').val();
		switch (b) {
			case '':
			case null:
				break;
			default:
				this.bookmarks[b] = this.page;
				if (this.save_bookmarks())
				{
					log("add bookmark", b, this.page);
					contents_panel.update_bookmarks();
				} else {
					if(isSafari()) {
			    		alert("Не удалось создать закладку. Возможно вы находитесь в режиме приватного просмотра.");	
			    	}
			    	this.bookmarks[b] = null;
				}
			break;              
		}
	}

	OC3Book.prototype.delete_bookmark = function(b) {
		if (this.bookmarks[b] !== undefined) {
			if (this.save_bookmarks()) // check lockalstorage
			{
				delete this.bookmarks[b];
				this.save_bookmarks();
			} else {
				if(isSafari()) {
		    		alert("Не удалось удалить закладку. Возможно вы находитесь в режиме приватного просмотра.");	
		    	}
			}
		}
	}

	OC3Book.prototype.open_bookmark = function(b) {
		if (this.bookmarks && (b in this.bookmarks)) {
			this.goto_page(this.bookmarks[b]);
			log("open bookmark", b, this.page);
		} else {
			log("bookmark", b, "not found");
		}
	}

	OC3Book.prototype.search_text = function(text) {
		pages = [];
		for (var i=0, len = this.page_count(); i < len; i++) {	
			content = $(this.get_page(i)).text();
			content = content ? content.toLowerCase() : ''; 
			if ((content.indexOf(text.toLowerCase())+1)) {
				pages.push(i);
			}
		}	
		return pages;
	}

	OC3Book.prototype.zoom_in = function() {
		log("zoom in");
		for (var i in this.books) {
			if(this.books[i]) this.books[i].zoomIn();
		}         
		this.zoom_level = this.book.zoomLevel;
	}

	OC3Book.prototype.zoom_out = function() {
		log("zoom out");
		for (var i in this.books) {
			if(this.books[i]) this.books[i].zoomOut();
		}
		this.zoom_level = this.book.zoomLevel;
	}

	OC3Book.prototype.zoom_at = function(z) {
		for (var i in this.books) {
			if(this.books[i]) this.books[i].zoom(z);
		}
		this.book && this.book.zoomLevel && (this.zoom_level = this.book.zoomLevel);
	}

	OC3Book.prototype.rebuild_thumbnails = function(){
		this.book.destroyThumbnails()
		this.book.createThumbnails()
		// используем правильное преключение страниц 
		var oc3b = this;
		$(this.book.thumbnails).each(function(i,t) {
			$(t).unbind('click').click(function(){
				$('.active-thumbnail').removeClass('active-thumbnail');
				oc3b.goto_page(oc3b.book == oc3b.books.double_page ? i - 1 : Math.ceil(i/2 - 1));
				if (oc3b.book.pageIsOnTheRight(i)) {
					oc3b.book.thumbnails[i].addClass('active-thumbnail');
					oc3b.book.thumbnails[i-1].addClass('active-thumbnail');
				} else {
					oc3b.book.thumbnails[i].addClass('active-thumbnail');
					oc3b.book.thumbnails[i+1].addClass('active-thumbnail');
				}
			});
		});

		if (this.book === oc3b.books.landscape) {
			this.book.thumbnailsContainer.addClass('one-page-landscape');	
		}
		if (this.book === oc3b.books.single_page) {
			this.book.thumbnailsContainer.addClass('one-page-portrait');	
		}
		$("#thumbs_holder").css("marginTop", -$("#thumbs_holder").height()/2)
	}

	OC3Book.prototype.toggle_thumbnails = function () {
		var  p = this.book == this.books.double_page ? this.page : this.page * 2;
		this.book.toggleThumbnails();
		if (this.book.pageIsOnTheRight(p)) {
			this.book.thumbnails[p].addClass('active-thumbnail');
			this.book.thumbnails[p-1].addClass('active-thumbnail');
		} else {
			this.book.thumbnails[p].addClass('active-thumbnail');
			this.book.thumbnails[p+1].addClass('active-thumbnail');
		}
	}

	OC3Book.prototype.set_location_hash = function() {
		var h = this.location_hash_set_to = this.to_location_hash();
		setTimeout(function(){ window.location.hash = h }, 20);
	}

	OC3Book.prototype.to_location_hash = function() {
		return this.variant_name + "/" + this.page + "/" + Math.round(this.zoom_level*100)/100;	
	}

	OC3Book.prototype.from_location_hash = function() {
		var h = window.location.hash.replace("#","").split('/');
		var v = h[0] || "auto";
		var p = (h[1]|0) || -1;
		var z = (Math.round(h[2]*100)/100) || 1;

		this.zoom_level = z;
		if(p != this.page && v === this.variant_name) { (this.goto_page(p)); }
		else if ( v != this.variant_name ) { this.page = p; this.set_variant(v); }
	}

	OC3Book.prototype.resize = function () {
		log("resize");
		for (var i in this.books) {
			if(this.books[i]) this.books[i].scaleToFit();
		}
	}

	OC3Book.prototype.set_settings = function (s) {
		var s = this.settings = Object.create(s);

		s.paragraph = [];
		for (var i in s.chapters) {
			s.paragraph = s.paragraph.concat(s.chapters[i].paragraph);
		}
		
		s.book_to_electronic = function (p) {
			var c = this.pages.filter(function(o) {return o[Object.keys(o)[0]] == p;})[0];
			return c ? Object.keys(c)[0] : p;
		}
		s.electronic_to_book = function (p) {
			var c = this.pages.filter(function(o) {return Object.keys(o)[0] == p;})[0];
			return c ? c[Object.keys(c)[0]] : p;
		}

		s.page_chapter = function (p) {
			return this.chapters.filter(function (o) { return p >= o.page_start && p <= o.page_end; })[0];
		}
		s.page_paragraph = function (p) {
			return this.paragraph.filter(function (o) { return p >= o.page_start && p <= o.page_end; })[0];
		}

		s.next_chapter = function (c) {
			var i = this.chapters.indexOf(c);
			if (i === undefined || i < 0 || i == this.chapters.length - 1) return undefined;
			return this.chapters[i+1];
		}
		s.prev_chapter = function (c) {
			var i = this.chapters.indexOf(c);
			if (i === undefined || i <= 0) return undefined;
			return this.chapters[i-1];
		}

		s.next_paragraph = function (p) {
			var i = this.paragraph.indexOf(p);
			if (i === undefined || i < 0 || i == this.paragraph.length - 1) return undefined;
			return this.paragraph[i+1];
		}
		s.prev_paragraph = function (p) {
			var i = this.paragraph.indexOf(c);
			if (i === undefined || i <= 0) return undefined;
			return this.paragraph[i-1];
		}
	}

	OC3Book.prototype.goto_next_chapter = function() {
		var c = this.settings.next_chapter(this.settings.page_chapter(this.page));
		c && this.goto_page(c.page_start);
	}

	OC3Book.prototype.goto_prev_chapter = function() {
		var c = this.settings.prev_chapter(this.settings.page_chapter(this.page));
		c && this.goto_page(c.page_start);
	}

	OC3Book.prototype.goto_next_paragraph = function() {
		var c = this.settings.next_paragraphr(this.settings.page_paragraph(this.page));
		c && this.goto_page(c.page_start);
	}

	OC3Book.prototype.goto_prev_paragraph = function() {
		var c = this.settings.prev_paragraph(this.settings.page_paragraph(this.page));
		c && this.goto_page(c.page_start);
	}

	OC3Book.prototype.glowWord = function(domPath, start, end, glowPage) {

		var page = glowPage || this.page;

		var container = this.get_page(page);

		var word = container.find(domPath);

		var text = $(word).text();

		var wordValue = $(word).text().substring(start, end);

		wordValue = '<span class="note" id="glowedWord">' + wordValue + '</span>';

		replaceWord($(word)[0], start, end, wordValue);
	}

	function normalizeIt(node) {
		for (var i = 0, children = node.childNodes, nodeCount = children.length; i < nodeCount; i++) {
			var child = children[i];
			if (child.nodeType == 1) {
				normalizeIt(child);
				continue;
			}
			if (child.nodeType != 3) { continue; }
			var next = child.nextSibling;
			if (next == null || next.nodeType != 3) { continue; }
			var combined_text = child.nodeValue + next.nodeValue;
			new_node = node.ownerDocument.createTextNode(combined_text);
			node.insertBefore(new_node, child);
			node.removeChild(child);
			node.removeChild(next);
			i--;
			nodeCount--;
		}
	}

	OC3Book.prototype.unGlowWord = function unGlowWord(){
		var glowedWord = $('#glowedWord');

		if(glowedWord.length > 0){
			var glowedWordParent = glowedWord.parent()
			glowedWord.replaceWith(glowedWord.html().replace('<span class="note" id="glowedWord">', '').replace('</span>', ''));
			normalizeIt(glowedWordParent[0]);
		}
	}

	/*Helper function that would replace text indexed words in their html representation*/
    function replaceWord(wordContainer, wordStart, wordEnd, replacer) {
        var ret = 0, elem;

        var elems = wordContainer.childNodes;

        var wordValue = $(wordContainer).text().substring(wordStart, wordEnd);

        var exitRecursion = false;

        function recursiveTraverse(elems){

        	if(exitRecursion) return;
    
	        for ( var i = 0; elems[i]; i++ ) {
	        	if(exitRecursion) break;
	            elem = elems[i];
	    
	            // Get the text from text nodes and CDATA nodes
	            if ( elem.nodeType === 3 || elem.nodeType === 4 ) {
	                ret += elem.nodeValue.length;

	                if(ret > wordStart){
	                	exitRecursion = true;

	                	var preLenght = ret - elem.nodeValue.length;
	                	var newStartIndex = wordStart - preLenght;
	                	var newEndIndex = newStartIndex + (wordEnd - wordStart);

	                	var nodeWord = elem.nodeValue.substring(newStartIndex, newEndIndex);

	                	if(nodeWord == wordValue){
	                		book.glowedWord = $(elem).replaceWith(elem.nodeValue.substring(0, newStartIndex) + replacer + elem.nodeValue.substring(newEndIndex, elem.nodeValue.length));
	                	} else{
	                		book.glowedWord = $(elem).replaceWith(elem.nodeValue.replace(wordValue, replacer));
	                	}

	                	return;
	                }

	            // Traverse everything else, except comment nodes
	            } else if ( elem.nodeType !== 8 ) {
	                recursiveTraverse( elem.childNodes );
	            }
	        }
	    };

	    recursiveTraverse(elems);
    }

	window.OC3Book = OC3Book;
});