/*Скрипт используется для инициализации fancyBox'а внутри статей*/
$(document).ready(function() { //image zoom handler {TODO: write our own optimized solution}
	$('.fancybox').fancybox({
		'showCloseButton' : true,
		afterShow: function(){
		 wh = $(window).height()/this.height;
		 ww = $(window).width()/this.width;
		 if (wh<ww)
		 {
	        this.height = this.height*wh;
	        this.width = this.width*wh;
		 }
		 else
		 {
	        this.height = this.height*ww;
	        this.width = this.width*ww;
		 }
		},
        beforeShow: function() {
            $(".fancybox-image").one("click", function() {
                  $.fancybox.close();
            });
        }

	});
});