$(function () {

  var controll_elements = (function(){
    
    function Elements () {
      this.elements = [
        '#container', 
        '#bottom_panel', 
        '#contents_panel', 
        '#top_panel', 
        '#left-page-notes', 
        '#right-page-notes', 
        '#button-prev', 
        '#button-next', 
        '#show_panels_button',
        '#thumbnailsContainer',
        '#button-help'
      ];

      for (var i in this.elements) {
        this.elements[i] = { 
          element: this.elements[i],
          was_visible: false,
        }
      }
    };

    Elements.prototype.show = function() {
      for (var i in this.elements) {
        var el = this.elements[i];
        if (el.was_visible) {
          $(el.element).fadeIn();
        }
      }
    };

    Elements.prototype.hide = function() {
      for (var i in this.elements) {
        var el = this.elements[i];
        el.was_visible = $(el.element).is(':visible');
        $(el.element).fadeOut();
      }
    };

    return new Elements();
  })();

  function open_media_object (el, isHelp)
  {
    var frameContainer = $('#media-object');

    if($(frameContainer).children('iframe').length > 0) return;

    var mo, url;

    if(!isHelp){
      mo = $(el).find('[data-type="media-object"]');
      url = mo.attr('data-src');
    } else{
      mo = null;
      url = './help/index.html'
    }

    console.log(isHelp)


    var w = $(window).width();
    var h = $(window).height();
    
    var controllsPanelHeight = $('#close-media-object').height() + (parseInt($('#close-media-object').css('margin-top')) * 3);


    //map cache
    /*if(url.indexOf('map') != -1){//link contain map object
      var cachedMap = $(frameContainer).children('iframe');
      //console.log(cachedMap);
      if(cachedMap.length != 0){
        //console.log(cachedMap);
        $(cachedMap).show();

        $(frameContainer).fadeIn(function(){          
            setTimeout(function() {
              cachedMap[0].contentWindow.focus();
            }, 100);
          });

        cachedMap[0].contentWindow.postMessage(url, "*");

        $(cachedMap).addClass('activeFrame');

        $('#close-media-object').fadeIn();
        controll_elements.hide();
        $('nav').css('pointer-events', 'none');
        $('#close-media-object').css('pointer-events', 'auto');
        return false;
      }
    }*/

    //create new iframe and set it's prop

    var newFrame = $('<iframe />');
    newFrame.width(w);
    newFrame.height(h - controllsPanelHeight); //magic number, change it!
    
    frameContainer.width(newFrame.width());   
    frameContainer.height(newFrame.height());   

    $(newFrame).one('load', function () {
      $(frameContainer).fadeIn(function()
        {
          setTimeout(function() {
            newFrame[0].contentWindow.focus();
          }, 100);
        });
    });

    newFrame.attr('src', url);

    frameContainer.append(newFrame);

    $(newFrame).addClass('activeFrame');

    $('#close-media-object').fadeIn();
    controll_elements.hide();
    $('nav').css('pointer-events', 'none');
    $('#close-media-object').css('pointer-events', 'auto');
    return false;
  }

  window.open_media_object = open_media_object;

  window.bindEventListeners = function(){
  	$('[data-type="media-object"]').click(function(e){
    var el = ($(this).parent().is('li')) ? $(this).parent().parent().parent() : $(this).parent();
    open_media_object (el);
    e.stopPropagation();
    return false; 
  });

  $('.info-object').click(function (e) {
    open_media_object (this)
    e.stopPropagation();
  });

  $('#close-media-object')
    .click(function (e) {
      e.stopPropagation();

      var frame = $('#media-object').children('.activeFrame');

      //media-object recyler
      //frame[0].contentWindow.postMessage('destroy', "*");

      $(frame).removeClass('activeFrame');

      //map cache
      /*var frameUrl = $(frame).attr('src');
      if(frameUrl.indexOf('map') != -1){
        console.log('map being cached');

        $('#media-object').fadeOut();
        $(frame).hide();

        $(this).fadeOut();
        controll_elements.show();
        $('nav').css('pointer-events', 'auto');

        if($(frame).attr('isOrientationChanged')){
          oc3book.update_variant();
          if (typeof oc3book.allign_notes == 'function') 
          {
            oc3book.allign_notes();
          }
        }
        
        return;
      }*/

      $(frame).attr('src', 'blank.html');

      setTimeout(function(){
        $(frame).remove();
      }, 1000)

      $('#media-object').fadeOut();

      $(this).fadeOut();
      controll_elements.show();
      $('nav').css('pointer-events', 'auto');

      if($(frame).attr('isOrientationChanged')){
        
        oc3book.resize();

        oc3book.update_variant();
        if (typeof oc3book.allign_notes == 'function') 
        {
          oc3book.allign_notes();
        }
      }
    });

	$(document).on('click', '#show-audio-player', function () {
		$('#player').fadeIn();
		$(this)
		  .attr('id', 'hide-audio-player')
		  .text('Спрятать плеер');
	});

	$(document).on('click', '#hide-audio-player', function () {
		$('#player').fadeOut();
		$(this)
		  .attr('id', 'show-audio-player')
		  .text('Показать плеер');
	});


	// музыка
	$('div').delegate('[data-type=audio-object]', 'click', function () {
		var url = $(this).attr('data-src');
		if(url && 'null.mp3' !== url.slice(-8))
		{
			$('#audio-object').attr('src', url);
			$('#player').fadeIn();
		}
	});

	$('#player .close').click(function() {
		$('#audio-object')
		   .attr('src', '');
		$('#audio-object').stop();
		$('#player').fadeOut();
	});
  }
})