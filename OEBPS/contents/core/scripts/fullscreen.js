$(function() {
    $(document).on("click", "#toggle-fullscreen-on", function() {
        $("body").fullscreen()
    });
    $(document).on("click", "#toggle-fullscreen-off", function() {
        $.fullscreen.exit()
    });
    $(document).on("fscreenopen", function() {
        $("#toggle-fullscreen-on").attr("id", "toggle-fullscreen-off")
    });
    $(document).on("fscreenclose", function() {
        $("#toggle-fullscreen-off").attr("id", "toggle-fullscreen-on")
    })
});