(function(P) {
    function F(a) {
        P.each("touchstart touchmove touchend touchcancel".split(/ /), function(f, c) {
            a.addEventListener(c, function() {
                P(a).trigger(c)
            }, false)
        });
        return P(a)
    }

    function J(a) {
        function g() {
            P(c).data(L, true);
            a.type = N;
            jQuery.event.handle.apply(c, f)
        }
        if (!P(this).data(M)) {
            var c = this,
                f = arguments;
            P(this).data(L, false).data(M, setTimeout(g, P(this).data(K) || P.longclick.duration))
        }
    }

    function I() {
        P(this).data(M, clearTimeout(P(this).data(M)) || null)
    }

    function H(a) {
        if (P(this).data(L)) {
            return a.stopImmediatePropagation() || false
        }
    }
    var E = P.fn.click;
    P.fn.click = function(a, c) {
        if (!c) {
            return E.apply(this, arguments)
        }
        return P(this).data(K, a || null).bind(N, c)
    };
    P.fn.longclick = function() {
        var a = [].splice.call(arguments, 0),
            f = a.pop();
        a = a.pop();
        var c = P(this).data(K, a || null);
        return f ? c.click(a, f) : c.trigger(N)
    };
    P.longclick = {
        duration: 500
    };
    P.event.special.longclick = {
        setup: function() {
            /iphone|ipad|ipod/i.test(navigator.userAgent) ? F(this).bind(D, J).bind([C, B, A].join(" "), I).bind(G, H).css({
                WebkitUserSelect: "none"
            }) : P(this).bind(z, J).bind([o, e, d, b].join(" "), I).bind(G, H)
        },
        teardown: function() {
            P(this).unbind(O)
        }
    };
    var N = "longclick",
        O = "." + N,
        z = "mousedown" + O,
        G = "click" + O,
        o = "mousemove" + O,
        e = "mouseup" + O,
        d = "mouseout" + O,
        b = "contextmenu" + O,
        D = "touchstart" + O,
        C = "touchend" + O,
        B = "touchmove" + O,
        A = "touchcancel" + O,
        K = "duration" + O,
        M = "timer" + O,
        L = "fired" + O
})(jQuery);