$(function(){

    var preload = $('body').data('preload');

    if(preload){

        $('body').addClass('img__preloading');

        preload = preload.split(',');
        var $preWrap = $('#imgPreloader'), counter = 0;

        preload.forEach(function(item){
            $preWrap.append($("<img />").attr("src", item));
        });

        $preWrap.find('img').each(function(){
            $(this).load(function(){
                counter++;

                if(counter == preload.length){
                    $preWrap.remove();
                    setTimeout(function(){
                        $('body').removeClass('img__preloading');
                    }, 500);
                }
            });
        });

    }

});