$(function(){ window.Note = function (module) {

	function log() 
	{
		// console.log.apply(console, arguments);
	}

	var left_cont = $('#left-page-notes');
	var right_cont = $('#right-page-notes');

	// load notes
	module.notes = [];
    var ls;
	if (typeof localStorage !== "undefined")
	{
		ls = JSON.parse($.jStorage.get(module.book_name));
	}
	else
	{
		ls = undefined;
	}
	module.notes = (ls && ls.notes ? ls.notes : {}); 

	function isSafari() 
	{
	    return /^((?!chrome).)*safari/i.test(navigator.userAgent);
	}

	// popup
	var bPopup = null;

	$('#popup-note #popup-note-button-cancel').on('click', function() {
		bPopup && bPopup.close();
		return false;
	});

	function word_pos(str, pos) 
	{
	    if (!str) return;
	    pos = Number(pos) >>> 0;
	    if (str[pos].trim() == '') { pos = str.slice(0, pos).search(/\S+\W$/); }
	    var left = str.slice(0, pos + 1).search(/\S+$/),
	        right = str.slice(pos).search(/\s/);
	    if (right < 0) {
	        return {left: left, right: str.length};
	    }
	    return {left: left, right: right + pos};
	}

	function selected_text()
	{

		var selection = {
			text: undefined, 
    		begin: 0, 
    		end: 0
		};

        var t = '', sel;
	    if (window.getSelection) {
	        // Webkit, Gecko

	        var s = window.getSelection();

			selection.text = s.toString();
			selection.begin = s.anchorOffset;
			selection.end = selection.begin + selection.text.length;

	    } else if ((sel = document.selection) && sel.type != "Control") {
	        // IE 4+
	        var textRange = sel.createRange();
	        if (!textRange.text) {
	            textRange.expand("word");
	        }
	        // Remove trailing spaces
	        while (/\s$/.test(textRange.text)) {
	            textRange.moveEnd("character", -1);
	        }
	        selection.text = textRange.text;

	        //TODO: retrive that!
	        selection.begin = 0;
	        selection.end = 0;
	    }

        return selection;
	}

	function validateSelection(selection, trigger){
		//$(trigger).addClass('note');
		var text = $(trigger).text();
		var index = text.indexOf(selection.text);
		//console.log('validation index ' + index + '_' + selection.begin);
		//console.log(selection.text + ' == ' + text.substring(selection.begin, selection.end), selection.text.length + ' == ' + text.substring(selection.begin, selection.end).length)

		if(index != -1){
			if(index != selection.begin){
				selection.begin = index;
				selection.end = index + selection.text.length;
			}
		}

		
		if(!validateLastCharacter(selection.text)){
			selection.text = selection.text.substring(0, selection.text.length - 1)
			selection.end--;

			if(!validateLastCharacter(selection.text)){
				selection.text = selection.text.substring(0, selection.text.length - 1)
				selection.end--;
			}
		}

		return selection;
	}

	function validateLastCharacter(text){
		var lastCharacter = text.slice(-1);
		var reg = /[а-яА-Я]/;

		return reg.test(lastCharacter);
	}

	function node_path(el) 
	{
		var path;
		node = $(el);
        while (node.length) {
            var realNode = node[0], name = realNode.localName;
            var parent = node.parent();
            if (name == 'html' || node.hasClass('BookHtml5-page')) break;

            if (name) {
            	name = name.toLowerCase();
	            var sameTagSiblings = parent.children(name);
	            if (sameTagSiblings.length > 1) { 
	                allSiblings = parent.children();
	                var index = allSiblings.index(realNode) + 1;
	                if (index > 1) {
	                    name += ':nth-child(' + index + ')';
	                }
	            }
            	path = name + (path ? '>' + path : '');
	        } 
            node = parent;
        }
        return path;
	}

	function create_note(selection) 
	{
		bPopup = $('#popup-note').bPopup({
			onClose: function(){
				if(selection){
					module.unGlowWord();
				}
			}
		});
		var textarea = $('#popup-note [name="note-title-text"]');
		textarea.val("");
		//textarea.focus();

		$('#popup-note textarea').show();
		
		var pn_select = $('#popup-note #popup-note-page-number');
		

		if (module.variant_name == 'double_page' && !selection) {
			$('#popup-note #popup-note-title').text('Создание заметки на странице');
			pn_select.empty().show();
			var lp = module.book.leftPageIndex(module.page);
			var rp = module.book.rightPageIndex(module.page);
			lp = ((module.page % 2 != 0) ? lp - 1 : lp + 1);
			rp = ((module.page % 2 != 0) ? rp - 1 : rp + 1);

			var lp_rb = $('<input type="radio" name="note-page-number" value="' + lp + '" checked="checked">');
			var rp_rb = $('<input type="radio" name="note-page-number" value="' + rp + '">');
			pn_select.append($('<span></span>').append(lp_rb).append(lp))
					 .append($('<span>/</span>'))
					 .append($('<span></span>').append(rp_rb).append(rp));
		} else {
			$('#popup-note #popup-note-title').text('Создание заметки');
			pn_select.empty().hide();
		}


		$('#popup-note #popup-note-button-ok').on('click', function() {
			var n_page = '';
			if (module.book == module.books.double_page && !selection) {
				n_page = parseInt($('#popup-note [name="note-page-number"]:checked').val());
			} else {
				n_page = module.page
			}

			if (!isNaN(n_page) && n_page >= 0) 
			{
				var title = textarea.val();
				if (title && title.trim() != '') 
				{
					var note = {
						begin: 0,
						end: 0,
						length: 0,
						text: '',
						type: "text",
						dom_path: '',
						page: n_page,
						title: title,
					};

					if(selection){
						note.begin = selection.begin;
						note.end = selection.end;
						note.text = selection.text;
						note.page = selection.page;
						note.dom_path = selection.dom;
					}

					oc3book.add_note(note);

					if(selection){
						oc3book.unGlowWord();
					}
				}
			}

			bPopup && bPopup.close();
			$('#popup-note #popup-note-button-ok').unbind('click');
		}).val('Создать');
	}

	// module API extention
	module.toggle_notes_controll = function () 
	{
		log('toggle_notes_controll');
		left_cont.find('[data-action="edit-note"]').toggle();
		left_cont.find('[data-action="remove-note"]').toggle();
		right_cont.find('[data-action="edit-note"]').toggle();
		right_cont.find('[data-action="remove-note"]').toggle();
    }

    module.show_notes_controll = function () 
    {
		log('show_notes_controll');
		left_cont.find('[data-action="edit-note"]').show();
		left_cont.find('[data-action="remove-note"]').show();
		right_cont.find('[data-action="edit-note"]').show();
		right_cont.find('[data-action="remove-note"]').show();
    }

    module.hide_notes_controll = function () 
    {
		log('hide_notes_controll');
		left_cont.find('[data-action="edit-note"]').hide();
		left_cont.find('[data-action="remove-note"]').hide();
		right_cont.find('[data-action="edit-note"]').hide();
		right_cont.find('[data-action="remove-note"]').hide();
    }

    module.add_note = function (n) 
    {
    	if (n) 
    	{
	    	var p_notes = this.notes[n.page];
	    	if (!p_notes) { p_notes = []; this.notes[n.page] = p_notes; }
	    	p_notes.push(n);
	    	p_notes = sort_notes(p_notes);
	    	if (save_notes()) 
	    	{
	    		module.update_notes_view();
	    	} else 
			{
				if(isSafari()) {
		    		alert("Не удалось создать заметку. Возможно вы находитесь в режиме приватного просмотра.");	
		    	}
			}
	    }
    }

    module.update_notes_view = function () 
    {
    	if (!module.book) return;

    	var conts = [];

    	if (module.book == module.books.double_page) 
    	{	
    		var lp = module.book.leftPageIndex(module.page);
    		var rp = module.book.rightPageIndex(module.page);

   			conts = [{
	    		cont: left_cont,
	    		notes: this.notes[(module.page % 2 != 0) ? lp - 1 : lp + 1],
	    		page: (module.page % 2 != 0) ? lp - 1 : lp + 1,
	    		align: 'left'
	    	}, {
	    		cont: right_cont,
	    		notes: this.notes[(module.page % 2 != 0) ? rp - 1 : rp + 1],
	    		page: (module.page % 2 != 0) ? rp - 1 : rp + 1,
	    		align: 'right'
	    	}];
    	} else 
    	{
    		conts = [{
	    		cont: left_cont,
	    		notes: this.notes[module.page],
	    		page: module.page,
	    		align: 'right'
	    	}, {
	    		cont: right_cont,
	    		notes: [],
	    		page: null,
	    		align: 'right'
	    	}];
    	}
    	for (var i in conts) 
    	{
    		var container = null;
    		if(conts[i].page != null) container = $('#note-container-' + module.variant_name + '-' + conts[i].page);
    		console.log('#note-container-' + module.variant_name + '-' + conts[i].page);

    		if(!container) return;

    		if(conts[i].align == 'right'){
    			container.removeClass('note-container-left').addClass('note-container-right');
    		} else{
    			container.removeClass('note-container-right').addClass('note-container-left');
    		}

    		var ul = container.children('ul');
    		var p_notes = conts[i].notes ? conts[i].notes : [];
	    	ul.empty();
	    	if (p_notes.length) 
	    	{
	    		var tmpl = '<% for(var i=0; i<notes.length; i++) {%>\
						<% var note = notes[i]; %>\
						<% if(note && note.page !== undefined) {%>\
							<li data-note-index="<%= i %>" data-note-page="<%= note.page %>">\
							  <span data-action="show-note"><i class="flaticon-bookmark10"></i></span>\
							  <div class="note-title"><%= note.title %></div>\
							  <span data-action="view-note"><i class="flaticon-open163"></i></span>\
							  <span data-action="edit-note"><i class="flaticon-note32"></i></span>\
							  <span data-action="remove-note">&times;</span>\
							</li>\
						<% } %>\
					<% } %>';
				conts[i].notes = p_notes = sort_notes(p_notes);
	    		ul.append(new EJS({text: tmpl}).render({notes:p_notes}));
	    	} else {
				//ul.append('<li>заметок нет</li>');
	    	}
    	}
    	module.allign_notes();
    	//module.hide_notes_controll();
    }

    module.allign_notes = function()
    {
    	return;
    	if (module.book && module.book.elem) 
    	{
	    	var book_elem = module.book.elem;
	    	left_cont.height(book_elem.height() - 100);
	    	right_cont.height(book_elem.height() - 100);
	    	left_cont.css('top', $('#container').css('padding-top'));
	    	right_cont.css('top', $('#container').css('padding-top'));

	    	var dLeft = 0;
	    	var dRight = 0;

	    	switch(oc3book.variant_name){
	    		case 'double_page': 
	    			dLeft = 0;
	    			dRight = 21;
	    		break;

	    		case 'single_page':
	    			dLeft = 1;
	    			dRight = 0;
	    		break;

	    		case 'landscape':
	    			dLeft = 5;
	    			dRight = 0;
	    		break;
	    	}

	    	var left_pos = Math.max(parseInt(oc3book.book.elem.css('margin-left')) + dLeft , 0);
	    	left_cont.css('left', left_pos);

	    	var right_pos = Math.max(parseInt(oc3book.book.elem.css('margin-right')) + dRight, 0);
	    	right_cont.css('right', right_pos);
    	}
    }


    // helper functions
    function sort_notes(p_notes) 
    {
		return p_notes;
			/*.map(function(n) {
				var e = $(module.get_page(n.page)).find(n.dom_path);
				if (e.length) {
					return { 
						rect: e[0].getBoundingClientRect(),
						note: n
					};
				}
			})
			.filter(function(n) { return n != undefined; })
			.sort(function (a, b) { 
				var dy = a.rect.top - b.rect.top;	// сравниваем высоту
				if (dy !== 0) {
					return dy;
				} else { // если высота равна, сравниваем отступ слева
					var dx =  a.rect.left - b.rect.left; 
					if (dx !== 0) {
						return dx;
					} else if(a.note.type == 'text' && b.note.type == 'text') { // если отступ равен, пробуем упорядочить по позиции в тексте
						var db = a.note.begin - b.note.begin;
						return db !== 0 ? db : a.note.end - b.note.end;
					}
				}
				return 0; // иначе, равны
			})
			.map(function(r){
				return r.note;
			});*/
	}

	function save_notes() 
	{
		try {
			var ls;
			if (typeof localStorage !== "undefined") {
				ls = JSON.parse($.jStorage.get(module.book_name));
				$.jStorage.deleteKey(module.book_name);
				ls = ls ? ls : {};
				ls.notes = module.notes;
				$.jStorage.set(module.book_name, JSON.stringify(ls));
			}
			else
			{
				ls = undefined;
			}
		}
		catch (error) 
	    {
	        return false;
	    }
	    return true;
	}

    function clear_note_selection() 
    {
    	$("span.note").each(function() 
    	{
		    var text = $(this).text();
		    $(this).replaceWith(text);
		});

		$(".note").each(function() { 
			$(this).removeClass('note');
		});
    }

    function select_note(note) 
    {
    	if (note) {
    		clear_note_selection();
    		var page = $(module.get_page(note.page));
    		var el = page.find(note.dom_path);
    		if (note.type == 'text') {
    			var spn = '<span class="note">' + note.text + '</span>';
				$(el).html(el.text().slice(0, note.begin) + spn + el.text().slice(note.end));
    		} else if (note.type == 'element') {
    			$(el).addClass('note');
    		}
    	}
    }

	function remove_note(note) 
    {
		        if (note) 
        {

            bPopup = $('#popup-delete-confirmation').bPopup({
                onClose: function(){
                    $('#popup-delete-confirmation #declineBtn').unbind('click');
                    $('#popup-delete-confirmation #confirmBtn').unbind('click');
                }
            });
            $('#popup-delete-confirmation #popup-title').text('Вы уверены что хотите удалить заметку?');

           
            $('#popup-delete-confirmation #declineBtn').on('click', function() {
                bPopup && bPopup.close();
            });

            $('#popup-delete-confirmation #confirmBtn').on('click', function() {

                oc3book.unGlowWord();

            
                if (save_notes()) 
                {
                   
                    var p = note.page;
                    delete module.notes[p][module.notes[p].indexOf(note)];
                    module.notes[p] = module.notes[p].filter(function (n){ return n !== undefined; });
                    save_notes();
                    module.update_notes_view();
                } else 
                {
                    if(isSafari()) 
                    {
                        alert("Не удалось удалить заметку. Возможно вы находитесь в режиме приватного просмотра.");    
                    }
                }

                bPopup && bPopup.close();
            }).val('Удалить');
        }
    }


    function showNote(note){
    	if(note) 
    	{
    		bPopup = $('#popup-note-view').bPopup();
    		$('#popup-note-content').text(note.title);

    		$('#popup-note-view #popup-note-view-button-close').on('click', function() {
    			bPopup && bPopup.close();
				$('#popup-note-view #popup-note-view-button-close').unbind('click');
    		});
    	}
    }

    function edit_note(note) 
    {
    	if(note) 
    	{
    		bPopup = $('#popup-note').bPopup();
			var textarea = $('#popup-note [name="note-title-text"]');
			textarea.val(note.title);
			textarea.focus();

			$(textarea).show();

			$('#popup-note #popup-note-title').text('Введите новый текст заметки');
			$('#popup-note #popup-note-page-number').empty().hide();

			$('#popup-note #popup-note-button-ok').on('click', function() {
				var title = textarea.val();
				if (title && title.trim() != '') 
				{
					note.title = title;
					if (save_notes()) 
	    			{
						module.update_notes_view();
	    			}else 
					{
						if(isSafari()) 
						{
				    		alert("Не удалось изменить заметку. Возможно вы находитесь в режиме приватного просмотра.");	
				    	}
					}
				}
				bPopup && bPopup.close();
				$('#popup-note #popup-note-button-ok').unbind('click');
			}).val('Изменить');
    	}
    }

    function note_from(container) 
    {
    	var d = $(container).parent().data();
		if(d.notePage !== undefined && d.noteIndex !== undefined )
		{
			var p_notes = module.notes[d.notePage];
	    	p_notes = sort_notes(p_notes);
	    	var note = p_notes ? p_notes[d.noteIndex] : null;
	    	if (note && note.page && note.page === d.notePage) 
	    	{
	    		return note;
	    	}
		}
		return null;
    }

	var isMobile = 
	{
	    Android: function() {
	        return /Android/i.test(navigator.userAgent);
	    },
	    BlackBerry: function() {
	        return /BlackBerry/i.test(navigator.userAgent);
	    },
	    iOS: function() {
	        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
	    },
	    Windows: function() {
	        return /IEMobile/i.test(navigator.userAgent);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
	    }
	};

/***********************
 * Interface
 ***********************/
	// make context menu
	$.contextMenu({
        selector: '.notable', 
        trigger: ( isMobile.any() ? 'none' : 'right'),
        build: function(trigger, e) {
            return {
                callback: function(key, options) {
                	showNoteCreationDialog(trigger, e);
		        }
            };
        },
        items: {
            "note": {name: "Создать заметку"}
        }
    });

	function showNoteCreationDialog(trigger, e){
    	var page = module.page;

    	if(module.variant_name == 'double_page'){
    		if(!$(trigger).closest('div[class^="BookHtml5-page"]').hasClass('BookHtml5-left')){
    			page = module.book.rightPageIndex(module.page);
    			page = ((module.page % 2 != 0) ? page - 1 : page + 1);
    		} else{
    			page = module.book.leftPageIndex(module.page);
    			page = ((module.page % 2 != 0) ? page - 1 : page + 1);
    		}
    	}

    	var selection = validateSelection(selected_text(), e.target);                	
    	selection.dom = node_path(e.target);
    	selection.page = page;

    	module.glowWord(selection.dom, selection.begin, selection.end, page);
    	create_note(selection);
	}

	module.bindNoteEvents = function(){
		if (isMobile.any()) {

			$('.notable').bind("touchstart", function(e) {
				if(module.contextTimeout){
					clearTimeout(module.contextTimeout);
					module.contextTimeout = null;
				}

				module.contextTimeout = setTimeout(function(){
					if(module.touch){
						if(module.touch.isDrag || module.touch.distance > 50) return;
					}

					console.log(e.target);

					showNoteCreationDialog(e, e);

					module.touch = null;
				}, 1000);
			});

		}
	}

	if (isMobile.any()) {

	    $('#container').on("touchend", function(c) {
	    	if(oc3book.contextTimeout){
				clearTimeout(oc3book.contextTimeout);
				oc3book.contextTimeout = null;
			}

			oc3book.touch = null;
	    })
	}

	$('#button-note').on('click', function(){
		create_note();
	});

    $(document).on('click', '[data-action="show-note"]', function(){
    	// неправильно работает
		// select_note(note_from(this));
		
		if ($(this).parent().hasClass('active-note'))
		{
			oc3book.unGlowWord();

			$(this).parent().removeClass('active-note');
		} else 
		{
			$('li.active-note').removeClass('active-note');
			$(this).parent().addClass('active-note');

			var note = note_from(this);

			oc3book.unGlowWord();

			if(note.dom_path != ''){
				oc3book.glowWord(note.dom_path, note.begin, note.end, note.page);
			}
		}
    	
	});

    $(document).on('click', '[data-action="remove-note"]', function(){
		remove_note(note_from(this));
	});

	$(document).on('click', '[data-action="edit-note"]', function(){
		edit_note(note_from(this));
		$('li.active-note').removeClass('active-note');
		$(this).parent().addClass('active-note');
	});

	$(document).on('click', '[data-action="view-note"]', function(){
		showNote(note_from(this));
		$('li.active-note').removeClass('active-note');
		$(this).parent().addClass('active-note');
	});

	module.update_notes_view();

  	module.hide_notes_controll();
}

});
