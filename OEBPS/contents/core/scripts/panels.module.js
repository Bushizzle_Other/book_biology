/* реализация селекта */
$(function()
{

	// time bomb
  $('[data-control=select]').each(function(index, el)
  {
    var $selectContainer = $(el);  
    var $selectVariant = $selectContainer.find('[data-control=variant]');
    var $selectItemList = $selectContainer.find('[data-control=items]');
    var $selectItems = $selectContainer.find('[data-control=item]');

    // скрываем/показываем список
    $selectVariant.click(function(event)
    {
      event.preventDefault();
      $selectItemList.toggleClass('select-items-open');
    });

    // выбор пункта
    $selectItems.click(function(event)
    {
      var $item = $(this);
      var index = $item.attr('data-select-index');

      // отмечаем выбранный пункт
      $selectContainer.attr('data-selected-index', index);

      // скрываем список
      setTimeout(function()
      {
        $selectItemList.toggleClass('select-items-open');
      }, 300);
    });
  })
});

$(function () {
	var bottom_panel = {
		elem: $("#bottom_panel .cp-controls"),
		height: $("#bottom_panel .cp-controls").height(),
	};

	var top_panel = {
		elem: $("#top_panel .cp-controls"),
		height: $("#top_panel .cp-controls").height(),
	};

	var book_container = $('#container');
	var is_visible = false;

	function Contents(elem) {
		this.elem = $(elem);
		this.is_open = false;
		this.mode = undefined; // bookmarks, contents, search
		this.width = $(elem).width();
		this.bookmarks = $(elem+' #block-bookmarks');
		this.contents = $(elem+' #block-contents');
		this.search = $(elem+' #block-content-search');
	};

	Contents.prototype.hide_panel = function () {
		this.elem.stop(1).animate({
			"left": -this.width,
			"opacity" : 0,
		},{
			duration:250,
			stop : function () { this.elem.hide(); }
		});
		this.is_open = false;
	}

	Contents.prototype.show_panel = function (mode) {
		this.set_mode(mode);
		//if (typeof oc3book.hide_notes_controll == "function") { oc3book.hide_notes_controll(); }
		this.elem.stop(1).show().animate({
			"left": 0,
			"opacity" : 1,
		},{
			duration:250
		});
		this.is_open = true;
	}

	Contents.prototype.set_mode = function (mode) {
		if (['bookmarks','contents','search'].indexOf(mode) >= 0 ) {
			// set button active state
			//
			({
				bookmarks: function()
				{
					// $('#button-bookmarks').toggleClass('cp-button-active');
					// $('.cp-button-active').not('#button-bookmarks').removeClass('cp-button-active');

				},
				contents: function()
				{
					// $('#button-contents').toggleClass('cp-button-active');
					// $('.cp-button-active').not('#button-contents').removeClass('cp-button-active');
				},
				search: function()
				{
					$('.cp-button-active').removeClass('cp-button-active');
				},
				'undefined': function()
				{
					$('.cp-button-active').removeClass('cp-button-active');
				}
			}[mode || typeof mode]());

			this.bookmarks.hide();
			this.contents.hide();
			this.search.hide();

			this[mode] && this[mode].show(); 
			this.mode = mode;
		}
	}

	Contents.prototype.toggle = function(mode) {
		if (['bookmarks','contents','search'].indexOf(mode) >= 0 ) {
			if (mode === this.mode && this.is_open) { this.hide_panel(); }
			else { this.show_panel(mode); }
		}
	}

	Contents.prototype.update_bookmarks = function() {
		var self = this;
		var ul = $(this.bookmarks.find('ul')[0]);
		if (typeof ul == "undeifined" ) return;
		ul.empty();
		oc3book.update_bookmarks(function (name, page) {
			var li = $('<li></li>');
			var a = $('<a href="#" title="'+name+'" >'+name+'</a>');
			a.on("click", function() {
				oc3book.open_bookmark(name);
				return false;
			});
			li.append(a);

			var del = $('<span class="bookmark-item-remove">&times</span>');

			del.on("click", function() {

				bPopup = $('#popup-delete-confirmation').bPopup({
					onClose: function(){
						$('#popup-delete-confirmation #declineBtn').unbind('click');
						$('#popup-delete-confirmation #confirmBtn').unbind('click');
					}
				});

				$('#popup-delete-confirmation #popup-title').text('Вы уверены что хотите удалить закладку?');

				$('#popup-delete-confirmation #declineBtn').on('click', function() {
					bPopup && bPopup.close();
				});

				$('#popup-delete-confirmation #confirmBtn').on('click', function() {

					oc3book.delete_bookmark(name);
					self.update_bookmarks();

					bPopup && bPopup.close();
				}).val('Удалить');
				
				return false;
			});

			li.append(del);

			ul.append(li);
		});
	}

	Contents.prototype.validateControlls = validateControlls;
	Contents.prototype.updateContent = updateContent;

	//updates content panel state
	function updateContent(skipScroll){
		if(!(contents_panel.mode === 'contents' && contents_panel.is_open)) return;

		var currentPage = oc3book.page;

		$('.active__para').removeClass('active__para');

		if(currentPage >= (oc3book.page_count() - 2)) return;

		var contentContainer = $('#cp-aside-list-content');

		var partCollection = $('.part-page-num');
		var page = 0;
		var glowElement = null;

		var node = null;

		for(var i = 0; i < partCollection.length; i++){
			page = parseInt($(partCollection[i]).text());
			if(currentPage <= page || (i == (partCollection.length - 1))) {				
				if(currentPage == page){
					glowElement = $(partCollection[i]).parent();
				} else{
					if(i == 0) {
						//glowElement = $(partCollection[i]).parent();
					} else{
						var parentIndex = (currentPage > page) ? i : i - 1;
						console.log(i, parentIndex, partCollection.length);
						var paraCollection = $(partCollection[parentIndex]).parent().siblings('ul').children('li');

						if(paraCollection.length == 0) glowElement = $(partCollection[parentIndex]).parent();

						for(var j = 0; j < paraCollection.length; j++){
							node = $(paraCollection[j]).children('.para-page-num');
							page = parseInt(node.text());

							if(currentPage <= page){
								if(currentPage == page || j == 0){
									glowElement = paraCollection[j];
								} else{
									glowElement = paraCollection[j - 1];
								}
								
								break;
							}

							if(j == (paraCollection.length - 1)){
								glowElement = paraCollection[j];
							}
						}
					}
				}

				break;
			}
		}


		if(glowElement){
			$(glowElement).addClass('active__para');

			if(!skipScroll){
				$('#block-contents').animate({
		            scrollTop: ($('#block-contents').scrollTop() + $(glowElement).offset().top - 50) + 'px'
		        }, 'fast');
			}
		}

	}

	function validateControlls(controllType) {
    	if(controllType == 'all' || controllType == 'zoom'){
	    	$("#button-zoomout").removeClass('button-disabled');
			$("#button-zoomin").removeClass('button-disabled');

	    	if(oc3book.zoom_level == oc3book.zoom_max){
	    		$("#button-zoomin").addClass('button-disabled');
	    	} else if(oc3book.zoom_level == oc3book.zoom_min){
				$("#button-zoomout").addClass('button-disabled');
	    	}

	    	if(oc3book.zoom_level > oc3book.zoom_min){
	    		if(!oc3book.dragEnabled){
		    		oc3book.dragEnabled = true;
			    	$('#container').on("touchstart", zoomedTouchStart);
					$('#container').on("touchmove", zoomedTouchMove);
				}
			} else{
				if(oc3book.dragEnabled){
					$('#container').off("touchstart", zoomedTouchStart);
					$('#container').off("touchmove", zoomedTouchMove);
				}
			}
	    }

	    if(controllType == 'all' || controllType == 'navigation'){
	    	var isPrevVisible = true;
	    	var isNextVisible = true;

	    	if(oc3book.page <= -1){ //title
	    		isPrevVisible = false;
	    	} else if(oc3book.page >= (oc3book.page_count() - 2)){ //back
	    		isNextVisible = false;
	    	}

	    	isPrevVisible ? $('#button-prev').show() : $('#button-prev').hide();
	    	isNextVisible ? $('#button-next').show() : $('#button-next').hide();
	    }
    }

    function zoomedTouchStart(e) {
		e.memedPosition = {
			x: parseInt(oc3book.book.zoomContent.css('marginLeft')),
			y: parseInt(oc3book.book.zoomContent.css('marginTop'))
		}
		oc3book.touch = e;
	}

	function zoomedTouchMove(e) {
		if(oc3book.touch){ // dragging

			var x1 = oc3book.touch.originalEvent.touches[0].pageX;
			var y1 = oc3book.touch.originalEvent.touches[0].pageY;

			var x2 = e.originalEvent.touches[0].pageX;
			var y2 = e.originalEvent.touches[0].pageY;

			var distance = Math.sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));

			oc3book.touch.distance = distance;

			if(oc3book.zoom_level <= oc3book.zoom_min) return;

			if(distance > 50){ //start drag

				oc3book.touch.isDrag = true;

				var currentPosition = {
					x: parseInt(oc3book.book.zoomContent.css('marginLeft')),
					y: parseInt(oc3book.book.zoomContent.css('marginTop'))
				}

				var contentSize = {
					x: oc3book.book.zoomContent.width() * parseFloat(oc3book.book.zoomContent.css('zoom')),
					y: oc3book.book.zoomContent.height() * parseFloat(oc3book.book.zoomContent.css('zoom'))
				}

				var viewSize = {
					x: $(window).width(),
					y: $(window).height()
				}

				var boundBox = {
					left: 0,
					top: 0,
					right: -(contentSize.x - viewSize.x),
					bottom: -(contentSize.y - viewSize.y)
				}

				var horizontalLock = contentSize.x <= viewSize.x;
				var verticalLock = contentSize.y <= viewSize.y;

				x2 += (oc3book.touch.memedPosition.x - x1);
				y2 += (oc3book.touch.memedPosition.y - y1);

				if(x2 > boundBox.left) x2 = boundBox.left;
				if(y2 > boundBox.top) y2 = boundBox.top;

				if(x2 < boundBox.right) x2 = boundBox.right;
				if(y2 < boundBox.bottom) y2 = boundBox.bottom;

				if(!horizontalLock) oc3book.book.zoomContent.css('marginLeft', x2);
				if(!verticalLock) oc3book.book.zoomContent.css('marginTop', y2);

                e.stopPropagation();
			}

		}
	}

	Contents.prototype.update_search = function(pages) {
		var ul = $(this.search.find('ul')[0]);
		if (typeof ul == "undeifined" ) return;
		ul.empty();
		if (pages.length === 0) {
			ul.append('<li>Ничего не найдено</li>');
		} else {
			ul.append('<li>Найдено на страницах:</li>');
			function make_page(p) {
				return $('<li class="search-page-number"></li>').append(
					$('<a href="#" title="'+p+'" >'+p+'</a>').on('click', function() {
						oc3book.goto_page(p); return false;
					})
				);
			}

			for (var i = 0, len = pages.length; i < len; ++i) {
				// add page
				pages[i] && ul.append(make_page(pages[i]));			
			}	
		}
	}

	Contents.prototype.make_contents = function() {
    	var ul = $(this.contents.find('ul')[0]);
		if (typeof ul == "undeifined" ) return;
    	ul.empty();

    	if (oc3book && oc3book.settings && oc3book.settings.chapters) {
    		var tmpl = '\
<% for(var i=0; i<chapters.length; i++) {%>\
<li>\
  <% var ch = chapters[i]; %>\
  <% if(!ch.fictive) {%>\
  	<span>\
	    <div class="part-title"><%= ch.name %></div>\
	    <div class="part-page-num"><%= ch.page_start %></div>\
	</span>\
    <ul>\
  <% } %>\
  <% if(ch.paragraph) { %>\
    <% for(var p=0; p<ch.paragraph.length; p++) { %>\
      <% if(!ch.paragraph[p].fictive) { %> \
		<li>\
		  <div class="para-title">\
		    <%= ch.paragraph[p].name %>\
		  </div>\
		  <div class="para-page-num"><%= ch.paragraph[p].page_start %></div>\
		</li>\
	  <% } %>\
    <% } %>\
  <% } %>\
  <% if(!ch.fictive) { %>\
    </ul>\
  <% } %>\
</li>\
<% } %>';

    		ul.append(new EJS({text: tmpl}).render(oc3book.settings));
    	} else {
			ul.append('<li>Данные не загружены</li>');
    	}
    }

	window.contents_panel = new Contents('#contents_panel');

	function toggle_panels() {
		if (is_visible) {
			panels_hide(); 
		} else {
			panels_show();  
		}
	}

	function toggleHelp(){
		if(open_media_object) open_media_object(null, true);
	}

	function panels_show() {
		/*Eternal hide? Why??*/
		//if (typeof oc3book.hide_notes_controll == "function") { oc3book.hide_notes_controll(); }
		 $('#top_panel').css('display','block');
	    $('.cp-button-active').removeClass('cp-button-active');
		bottom_panel.elem.show();
	    top_panel.elem.show();

	    setTimeout(function()
	    {
			book_container.css({
				"padding-top" : top_panel.height,
				"padding-bottom" : bottom_panel.height
			});
			bottom_panel.elem.css({top: 0});
			top_panel.elem.css({top: 0});
			oc3book.thumbnails.css({bottom: bottom_panel.height + 'px'});
			oc3book.resize();
			oc3book.zoom_at(oc3book.zoom_level);
			if (typeof oc3book.allign_notes == "function") { oc3book.allign_notes(); }
	    }, 0);
	    
	    is_visible = true;
		
	}

	function panels_hide() {
		is_visible = false;
        $('#top_panel').css('display','none');
		contents_panel.hide_panel();
	    setTimeout(function()
	    {

			$("#variant").parent().find('ul').slideUp();
			book_container.css({
			"padding-top" : 0,
			"padding-bottom" : 0
			});
			bottom_panel.elem.css({top: top_panel.height});
			top_panel.elem.css({top: -bottom_panel.height});
			bottom_panel.elem.hide();
		    top_panel.elem.hide();
			oc3book.thumbnails.css({bottom: '0px'});
			oc3book.resize();
			oc3book.zoom_at(oc3book.zoom_level);
	    	if (typeof oc3book.allign_notes == "function") { oc3book.allign_notes(); }
	    }, 0);
	}

    function update_variant_icon() {
    	var variants = ['double_page', 'single_page', 'landscape', 'auto'];
    	$('[data-control=select]').attr('data-selected-index', variants.indexOf(oc3book.variant_name));
    }

    function initilize(entryVariant){

    $('#page-preloader').show();

	/***********************
	 * Initialization
	 ***********************/
	 	//books
		window.oc3book = new OC3Book({
			double_page: {
				container: '#features'
			},
			single_page: {
				container: '#portret'
			},
			landscape: {
				container: '#landscape'
			},
			on_set_variant : function (variant_name) {
				// функция вызывается при смене режима
				update_variant_icon();
				oc3book.update_notes_view && oc3book.update_notes_view();
			},
			on_page : function () {
				oc3book.update_notes_view && oc3book.update_notes_view();
			},
			on_load : function (oc3book) {
				Note(oc3book);
			}
		});

		date = new Date();
		current_id = date.getTime();
		if ($.cookie('current_id') == undefined) {
			$.cookie('current_id', current_id, { expires: 255, path: '/' })
		}

		oc3book.from_location_hash();

		validateControlls('zoom');

		panels_hide();
		contents_panel.hide_panel();

		$("#variant").parent().find('ul').hide();

		$('#player').hide();

		$('.lesson img').width(540);
		
	/***********************
	 * Interface
	 ***********************/

	 	var isMobile = 
		{
		    Android: function() {
		        return /Android/i.test(navigator.userAgent);
		    },
		    BlackBerry: function() {
		        return /BlackBerry/i.test(navigator.userAgent);
		    },
		    iOS: function() {
		        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
		    },
		    Windows: function() {
		        return /IEMobile/i.test(navigator.userAgent);
		    },
		    any: function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
		    }
		};

	 	if(!isMobile.any()) $('#button-autopage').remove();

		$('#show_panels_button').on("click", toggle_panels);
		$('#button-help').on("click", toggleHelp);

		// view variants
		//---------------------------

		$("#variant").on('click', function(){
			$("#variant").parent().find('ul').slideToggle();
			return false;
		});


		$(document).on('click', '#button-portret', function() {
			oc3book.set_variant("single_page"); 
			setStylesOfHeader(oc3book.variant_name);
			$("#variant").parent().find('ul').slideToggle();
			return false;
		});

		$(document).on('click', '#button-landscape', function() {
			oc3book.set_variant("landscape");
			setStylesOfHeader(oc3book.variant_name);
			$("#variant").parent().find('ul').slideToggle();
			return false;
		});

		$(document).on('click', '#button-double', function() {
			oc3book.set_variant("double_page");
			setStylesOfHeader(oc3book.variant_name);
			$("#variant").parent().find('ul').slideToggle();
			return false;
		});

		$(document).on('click', '#button-autopage', function() {
			oc3book.set_variant("auto");
			setStylesOfHeader(oc3book.variant_name);
			$("#variant").parent().find('ul').slideToggle();
			return false;
		});

		function setStylesOfHeader(variant){
				$('.content-book-text').css('padding-top', '10px');
				$('.back-img').css('display', 'none');
		}


		// navigation
		//---------------------------
		$(document).on('click', '#button-next', function(){
			oc3book.next();
			updateContent();
		});

		$(document).on('click', '#button-prev', function(){
			oc3book.prev();
			updateContent();
		});

		$(document).on('click', 'div[data-action="next-chapter"]', function(){
			oc3book.goto_next_chapter();
		});

		$(document).on('click', 'div[data-action="prev-chapter"]', function(){
			oc3book.goto_prev_chapter();
		});

		$(document).on('click', 'div[data-action="next-paragraph"]', function(){
			oc3book.goto_next_paragraph();
		});

		$(document).on('click', 'div[data-action="prev-paragraph"]', function(){
			oc3book.goto_prev_paragraph();
		});


		contents_panel.contents.on('click', 'ul .para-title, ul .part-title', function() {
		    var n = parseInt($(this).next().html());
		    oc3book.goto_page(n);
		    updateContent(true);

		});

		// search
		//---------------------------
		$("#button-search").on('click', function(){
			var searchTerm = $('input[data-control="search-val"]').val();
			// remove any old highlighted terms
			$('body').removeHighlight();
			// disable highlighting if empty
			if (searchTerm) {
				// highlight the new term
				$('body').highlight(searchTerm);
				contents_panel.update_search(oc3book.search_text(searchTerm)); 
				contents_panel.show_panel('search');
			}
		});

		$('#button-clear-search').on('click', function() {
			$('input[data-control="search-val"]').val('');
			$('body').removeHighlight();
			var ul = $(contents_panel.search.find('ul')[0]);
			ul && ul.empty();
			contents_panel.hide_panel();
		});

		// note
		//---------------------------
		$("#button-note").on('click', function () {
	   		/*$('#button-thumbs, button-bookmarks, #button-contents').removeClass('cp-button-active');
			$(this).closest('.cp-button').toggleClass('cp-button-active');
			$('.cp-button-active').not('#button-note').removeClass('cp-button-active');
			*/
			//contents_panel.hide_panel();
			//oc3book.toggle_notes_controll();
			return false;
		});


		// bookmarks
		//---------------------------
		$('#button-bookmarks').on('click', function() {
	    $('#button-thumbs, #button-note, #button-contents').removeClass('cp-button-active');
	    $(this).closest('.cp-button').toggleClass('cp-button-active');
			contents_panel.update_bookmarks();
			contents_panel.toggle('bookmarks');
			return false;
		});

		$('#button-add-bookmark').on("click", function() {
			bPopup = $('#popup-bookmark').bPopup();
			$('#page_bookmark').val('');

			$('#popup-button-add-bookmark').on('click', function(){
				bPopup.close();
				oc3book.add_bookmark();
				contents_panel.update_bookmarks();
				$('#popup-button-add-bookmark').unbind('click');
			});
			return false;
		});


		// contents
		//---------------------------
		$('#button-contents').on('click', function() {
	    $('#button-thumbs, #button-note, #button-bookmarks').removeClass('cp-button-active');
	    $(this).closest('.cp-button').toggleClass('cp-button-active');
			contents_panel.toggle('contents');
			updateContent();
		});


		// thumbnails
		//---------------------------
		$("#button-thumbs").on('click', function () {
	    $('#button-contents, #button-note, #button-bookmarks').removeClass('cp-button-active');
	    $(this).closest('.cp-button').toggleClass('cp-button-active');
			oc3book.toggle_thumbnails();
			oc3book.set_location_hash();
		});

		$("#thumbs_position button").on("click", function(){
			var position = $(this).text().toLowerCase()
			if ($(this).data("customized")) {
				position = "top"
				oc3book.book.opts.thumbnailsParent = "#thumbs_holder";
			} else {
				oc3book.book.opts.thumbnailsParent = "body";
			}
			oc3book.book.opts.thumbnailsPosition = position
			oc3book.rebuild_thumbnails();
		})

		$("#thumb_automatic").on('click', function(){
			oc3book.book.opts.thumbnailsSprite = null
			oc3book.book.opts.thumbnailWidth = null
			oc3book.rebuild_thumbnails();
		})
		$("#thumb_sprite").on('click', function(){
			oc3book.book.opts.thumbnailsSprite = "images/thumbs.jpg"
			oc3book.book.opts.thumbnailWidth = 136
			oc3book.rebuild_thumbnails();
		})
		$("#thumbs_size button").on('click', function(){
			var factor = 0.02*( $(this).index() ? -1 : 1 );
			oc3book.book.opts.thumbnailScale = oc3book.book.opts.thumbnailScale + factor;
			oc3book.rebuild_thumbnails();
		})


		// page numbers
		//---------------------------
		var bPopup = null;
		$('#button-pages').on("click", function() {
			bPopup = $('#popup-page-number').bPopup();
			$('#popup-page-number input[name="page_number"]').val("");
			$('#popup-page-number').css({'margin-bottom':'6px','line-height':'24px'});
			
			var page = oc3book.variant_name == "double_page" ? (oc3book.page / 2 | 0) * 2 : oc3book.page;
			var text = 'Электронная страница: ' + page;

			if (oc3book.variant_name == "double_page") 
			{
				text += ' / ' + (page + 1);
			}
			text += '<br >';

			text += 'Книжная страница: ' + oc3book.settings.pages.filter(function(o) {return Object.keys(o)[0] == page;}).map(function(o) { return o[Object.keys(o)[0]];}).join(', ');
			if (oc3book.variant_name == "double_page") 
			{
				text += ' / ' +  oc3book.settings.pages.filter(function(o) {return Object.keys(o)[0] == (page + 1);}).map(function(o) { return o[Object.keys(o)[0]];}).join(', ');
			}
			text += '<br >';
			$('#page-number-text').html(text);
			return false;
		}); 
		
		$('#popup-page-number [data-action="close-modal"]').on('click', function(e){
			bPopup && bPopup.close();
			return false;
		}); 

		$('#button-gotopage').on("click", function() {
			bPopup && bPopup.close(); 
			var p = parseInt($('#popup-page-number input[name="page_number"]').val());
			var variant = $('#popup-page-number [name="page_variant"]:checked').val();
			if (isNaN(p)) return;
			var max_pages = oc3book.settings.pages[oc3book.settings.pages.length-1];

			if (variant === 'book') {
				max_pages = max_pages[Object.keys(max_pages)[0]];

				if (max_pages < p) p = max_pages;
				if (oc3book.settings && oc3book.settings.pages)
				{
					oc3book.goto_page(oc3book.settings.book_to_electronic(p));
				}
			} else if (variant === 'electronic') {
				max_pages = Object.keys(max_pages)[0]+1;
				if (max_pages < p) p = max_pages;
				oc3book.goto_page(p);
			}

			updateContent();
			
			return false;
		});


		// zoom
		//---------------------------
		$("#button-zoomin").on('click', function() {
			oc3book.zoom_in();
			validateControlls('zoom');
	        oc3book.set_location_hash();
		});
		
		$("#button-zoomout").on('click', function() {
			oc3book.zoom_out();
			validateControlls('zoom');
	        oc3book.set_location_hash();
		});

		$(document).ready(function() { //image zoom handler {TODO: write our own optimized solution}
			$('.fancybox').fancybox({
				'showCloseButton' : true,
				afterShow: function(){
				 wh = $(window).height()/this.height;
				 ww = $(window).width()/this.width;
				 if (wh<ww)
				 {
				         this.height = this.height*wh;
				         this.width = this.width*wh;
				 }
				 else
				 {
				         this.height = this.height*ww;
				         this.width = this.width*ww;
				 }
				},
		        beforeShow: function() {
		            $(".fancybox-image").one("click", function() {
		                  $.fancybox.close();
		            });
		        }

			});
		});

		// variant switch
		//---------------------------
		window.onresize = function(){ //multiple call fix - looks if size changed since last 100ms
			if(window.skipNextResize){
				window.skipNextResize = false;
				return;
			}
			var activeElement = $(document.activeElement);
			var elementTagName = activeElement[0].tagName;
			console.log(elementTagName);
			if(elementTagName === "TEXTAREA"){
				window.skipNextResize = true;
				return;
			}
			if(elementTagName === 'INPUT'){
				if(activeElement.attr('type') === 'text'){
					window.skipNextResize = true;
					return;
				}
			}

			if(window.resizeInProcess){
				clearTimeout(window.resizeInProcess);
			} else{
				$('#page-preloader').show();
			}

			window.resizeInProcess = setTimeout(function(){
				window.resizeInProcess = null;
				onResize();
			}, 100);
		}

		function onResize(){
			var mediaObject = $('#media-object').children('.activeFrame');
			if(mediaObject.length > 0){
				$('#page-preloader').show();
				var mediaObjectSource = $(mediaObject).attr('src');

				if(mediaObjectSource && mediaObjectSource != ''){
					var controllsPanelHeight = $('#close-media-object').height() + (parseInt($('#close-media-object').css('margin-top')) * 3);

					$(mediaObject).width($(window).width());
					$(mediaObject).height($(window).height()  - controllsPanelHeight);
					$('#media-object').width($(mediaObject).width());
					$('#media-object').height($(mediaObject).height());
					mediaObject[0].contentWindow.postMessage('resize', "*");
				}

				$(mediaObject).attr('isOrientationChanged', !$(mediaObject).attr('isOrientationChanged'));

				setTimeout(function(){
					$('#page-preloader').hide();
				}, 200);

				return;
			}

			oc3book.resize();

			oc3book.update_variant();
			if (typeof oc3book.allign_notes == 'function') 
			{
				oc3book.allign_notes();
			}
			
		}


		// touch support
		//---------------------------
		function _untouch(a) {
	        return a.originalEvent.touches && a.originalEvent.touches[0] || a
	    }
	    $("#container").bind("touchstart", function(c) {
	        var b = c.originalEvent.touches;
	        if (1 >= b.length){
	        	oc3book.touch_started = {
		            x: b[0].pageX,
		            y: b[0].pageY,
		            timestamp: c.originalEvent.timeStamp,
		            inHandle: $(c.target).hasClass("BookHtml5-handle")
		        };
	        	oc3book.touch_started.inHandle && oc3book.book.pageEdgeDragStart(_untouch(c));
	        }
	    });
	    
	    $(document).on("touchmove", function(c) {
	        if (oc3book.touch_started) {
	            var b = c.originalEvent.touches;
	            oc3book.touch_ended = {
	                x: b[0].pageX,
	                y: b[0].pageY,
	                timestamp: c.originalEvent.timeStamp
	            };
	            if (oc3book.touch_started.inHandle) return oc3book.book.pageEdgeDrag(_untouch(c));
	            if(20 < Math.abs(oc3book.touch_ended.x - oc3book.touch_started.x)) c.preventDefault();
	            //c.preventDefault();
	        }
	    });
	    $(document).on("touchend touchcancel", function(c) {
	        if (oc3book.touch_started) {
	            !oc3book.touch_ended && $(c.target).hasClass("BookHtml5-handle") && (c = $(c.target).data("corner"), "r" === c && oc3book.next(), "l" === c && oc3book.prev());
	            var b = oc3book.touch_started,
	                f = oc3book.touch_ended || oc3book.touch_started;
	            oc3book.touch_started = null;
	            oc3book.touch_ended = null;
	            if (b.inHandle) return oc3book.book.pageEdgeDragStop({
	                pageX: f.x
	            }), !1;
	            c = f.x - b.x;
	            var e = f.y - b.y,
	                b = f.timestamp - b.timestamp;
	            if (!(20 > Math.abs(c) || 200 < b) && Math.abs(c) > Math.abs(e)) return 0 > c ? oc3book.next() : oc3book.prev(), !1
	        }
	    })
	}

	/**ENTRY POINT**/

	initilize('auto');

	/**ENTRY POINT**/
});