/*! jQuery Browser - v0.1.0 - 3/23/2012
 * https://github.com/jquery/jquery-browser
 * Copyright (c) 2012 John Resig; Licensed MIT */
;
/*!
 * jQuery hashchange event - v1.3 - 7/21/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function(b) {
    if (!b.browser) {
        var a;
        a = navigator.userAgent || "";
        b.uaMatch = function(c) {
            c = c.toLowerCase();
            c = /(chrome)[ \/]([\w.]+)/.exec(c) || /(webkit)[ \/]([\w.]+)/.exec(c) || /(opera)(?:.*version)?[ \/]([\w.]+)/.exec(c) || /(msie) ([\w.]+)/.exec(c) || 0 > c.indexOf("compatible") && /(mozilla)(?:.*? rv:([\w.]+))?/.exec(c) || [];
            return {
                browser: c[1] || "",
                version: c[2] || "0"
            }
        };
        a = b.uaMatch(a);
        b.browser = {};
        a.browser && (b.browser[a.browser] = !0, b.browser.version = a.version);
        b.browser.webkit && (b.browser.safari = !0)
    }
})(jQuery);
jQuery.easing.jswing = jQuery.easing.swing;
jQuery.extend(jQuery.easing, {
    easeOutCubic: function(f, b, c, a, e) {
        return a * ((b = b / e - 1) * b * b + 1) + c
    },
    easeOutExpo: function(f, b, c, a, e) {
        return b == e ? c + a : a * (-Math.pow(2, -10 * b / e) + 1) + c
    }
});
window.Modernizr = function(I, C, m) {
    function j(e, d) {
        for (var f in e) {
            if (i[e[f]] !== m) {
                return "pfx" == d ? e[f] : !0
            }
        }
        return !1
    }

    function t(h, g, p) {
        var k = h.charAt(0).toUpperCase() + h.substr(1),
            l = (h + " " + r.join(k + " ") + k).split(" ");
        if ("string" === typeof g || "undefined" === typeof g) {
            g = j(l, g)
        } else {
            h: {
                l = (h + " " + q.join(k + " ") + k).split(" "), h = l;
                for (var n in h) {
                    if (k = g[h[n]], k !== m) {
                        g = !1 === p ? h[n] : "function" === typeof k ? k.bind(p || g) : k;
                        break h
                    }
                }
                g = !1
            }
        }
        return g
    }
    var F = {},
        o = C.documentElement,
        v = C.createElement("modernizr"),
        i = v.style;
    I = " -webkit- -moz- -o- -ms- ".split(" ");
    var r = ["Webkit", "Moz", "O", "ms"],
        q = ["webkit", "moz", "o", "ms"],
        v = {},
        L = [],
        J = L.slice,
        K, G = function(N, M, B, w) {
            var z, A, n, s = C.createElement("div"),
                l = C.body,
                u = l ? l : C.createElement("body");
            if (parseInt(B, 10)) {
                for (; B--;) {
                    n = C.createElement("div"), n.id = w ? w[B] : "modernizr" + (B + 1), s.appendChild(n)
                }
            }
            return z = ["&#173;<style>", N, "</style>"].join(""), s.id = "modernizr", (l ? s : u).innerHTML += z, u.appendChild(s), l || (u.style.background = "", o.appendChild(u)), A = M(s, N), l ? s.parentNode.removeChild(s) : u.parentNode.removeChild(u), !!A
        },
        H = {}.hasOwnProperty,
        D;
    "undefined" === typeof H || "undefined" === typeof H.call ? D = function(d, c) {
        return c in d && "undefined" === typeof d.constructor.prototype[c]
    } : D = function(d, c) {
        return H.call(d, c)
    };
    Function.prototype.bind || (Function.prototype.bind = function(d) {
        var c = this;
        if ("function" != typeof c) {
            throw new TypeError
        }
        var g = J.call(arguments, 1),
            h = function() {
                if (this instanceof h) {
                    var b = function() {};
                    b.prototype = c.prototype;
                    var b = new b,
                        a = c.apply(b, g.concat(J.call(arguments)));
                    return Object(a) === a ? a : b
                }
                return c.apply(d, g.concat(J.call(arguments)))
            };
        return h
    });
    (function(f, d) {
        var h = f.join(""),
            g = d.length;
        G(h, function(k, e) {
            for (var n = k.childNodes, l = {}; g--;) {
                l[n[g].id] = n[g]
            }
            F.csstransforms3d = 9 === (l.csstransforms3d && l.csstransforms3d.offsetLeft) && 3 === l.csstransforms3d.offsetHeight
        }, g, d)
    })([, ["@media (", I.join("transform-3d),("), "modernizr){#csstransforms3d{left:9px;position:absolute;height:3px;}}"].join("")], [, "csstransforms3d"]);
    v.csstransforms3d = function() {
        var b = !!t("perspective");
        return b && "webkitPerspective" in o.style && (b = F.csstransforms3d), b
    };
    for (var E in v) {
        D(v, E) && (K = E.toLowerCase(), F[K] = v[E](), L.push((F[K] ? "" : "no-") + K))
    }
    i.cssText = "";
    return v = null, F._version = "2.5.3", F._prefixes = I, F._domPrefixes = q, F._cssomPrefixes = r, F.testProp = function(b) {
        return j([b])
    }, F.testAllProps = t, F.testStyles = G, F.prefixed = function(e, d, f) {
        return d ? t(e, d, f) : t(e, "pfx")
    }, F
}(this, this.document);
(function(h, e, k) {
    function j(g) {
        g = g || location.href;
        return "#" + g.replace(/^[^#]*#?(.*)$/, "$1")
    }
    "$:nomunge";
    var a = "hashchange",
        f = document,
        m, c = h.event.special,
        i = f.documentMode,
        b = "on" + a in e && (i === k || 7 < i);
    h.fn[a] = function(g) {
        return g ? this.bind(a, g) : this.trigger(a)
    };
    h.fn[a].delay = 50;
    c[a] = h.extend(c[a], {
        setup: function() {
            if (b) {
                return !1
            }
            h(m.start)
        },
        teardown: function() {
            if (b) {
                return !1
            }
            h(m.stop)
        }
    });
    m = function() {
        function r() {
            var n = j(),
                s = l(d);
            n !== d ? (p(d = n, s), h(e).trigger(a)) : s !== d && (location.href = location.href.replace(/#.*/, "") + s);
            q = setTimeout(r, h.fn[a].delay)
        }
        var g = {},
            q, d = j(),
            o = function(n) {
                return n
            },
            p = o,
            l = o;
        g.start = function() {
            q || r()
        };
        g.stop = function() {
            q && clearTimeout(q);
            q = k
        };
        h.browser.msie && !b && function() {
            var n, s;
            g.start = function() {
                n || (s = (s = h.fn[a].src) && s + j(), n = h('<iframe tabindex="-1" title="empty"/>').hide().one("load", function() {
                    s || p(j());
                    r()
                }).attr("src", s || "javascript:0").insertAfter("body")[0].contentWindow, f.onpropertychange = function() {
                    try {
                        "title" === event.propertyName && (n.document.title = f.title)
                    } catch (t) {}
                })
            };
            g.stop = o;
            l = function() {
                return j(n.location.href)
            };
            p = function(t, w) {
                var u = n.document,
                    v = h.fn[a].domain;
                t !== w && (u.title = f.title, u.open(), v && u.write('<script>document.domain="' + v + '"\x3c/script>'), u.close(), n.location.hash = t)
            }
        }();
        return g
    }()
})(jQuery, this);
(function(i) {
    function f(s, q) {
        s = i(s);
        var r = this;
        this.elem = s;
        this.id = s.attr("id");
        this.pages = [];
        this.opts = q;
        this.currentPage = null;
        this.pageWidth = q.width / 2;
        this.pageHeight = q.height;
        this.transparentPages = q.transparentPages = !1;
        this.startPage = q.startPage;
        this.onShowPage = q.onShowPage;
        this.slideShow = q.slideShow;
        this.slideShowDelay = q.slideShowDelay;
        this.pauseOnHover = q.pauseOnHover;
        this.turnPageDuration = q.turnPageDuration;
        this.foldGradient = q.foldGradient;
        this.foldGradientThreshold = q.foldGradientThreshold;
        this.shadows = q.shadows;
        this.shadowThreshold = q.shadowThreshold;
        this.clipBoundaries = q.clipBoundaries;
        this.zoomLevel = 1;
        this.zoomMax = q.zoomMax;
        this.zoomMin = q.zoomMin;
        this.zoomBoundingBox = q.zoomBoundingBox;
        this.zoomStep = q.zoomStep;
        this.onZoom = q.onZoom;
        this.mouseWheel = q.mouseWheel;
        this.flipSound = q.flipSound;
        this.centeredWhenClosed = q.centeredWhenClosed;
        this.disableFlipAnimation = q.disableFlipAnimation;
        this.disableZoomDrag = q.disableZoomDrag;
        this.sectionDefinition = q.sections;
        this.rtl = !!q.rtl;
        i.BookHtml5.support.filters = "filters" in s[0];
        s.addClass("BookHtml5");
        "static" === s.css("position") && s.css("position", "relative");
        var n = s.children(),
            p = this.pagesContainer = this.origin = i("<div class='BookHtml5-origin'></div>").css({
                position: "absolute"
            }).appendTo(s);
        r.bookShadow = i("<div class='BookHtml5-book-shadow'></div>").appendTo(s).css({
            top: 0,
            position: "absolute",
            display: "none",
            zIndex: 0
        });
        this.insertPages(n, !0);
        p.append("<div class='BookHtml5-shadow-clipper'><div class='BookHtml5-shadow-container'><div class='BookHtml5-shadow-internal'></div></div></div>");
        r.shadowContainer = i(".BookHtml5-shadow-container", p);
        r.internalShadow = i(".BookHtml5-shadow-internal", p);
        r.shadowClipper = i(".BookHtml5-shadow-clipper", p).css({
            display: "none"
        });
        r.foldShadow = i("<div class='BookHtml5-shadow-fold'></div>").appendTo(r.shadowContainer);
        r.foldGradientContainer = i("<div class='BookHtml5-fold-gradient-container'></div>").appendTo(r.shadowContainer);
        r.foldGradientElem = i("<div class='BookHtml5-fold-gradient'></div>").appendTo(r.foldGradientContainer);
        r.hardPageShadow = i("<div class='BookHtml5-hard-page-shadow'></div>").css({
            display: "none"
        }).appendTo(p);
        !i.support.opacity && i.BookHtml5.support.filters && i.BookHtml5.applyAlphaImageLoader(["BookHtml5-fold-gradient", "BookHtml5-fold-gradient-flipped", "BookHtml5-shadow-fold", "BookHtml5-shadow-fold-flipped"]);
        r.shadowContainer.css("position", "relative");
        this.leftHandle = i("<div class=''></div>").data("corner", "l").appendTo(p);
        this.rightHandle = i("<div class=''></div>").data("corner", "r").appendTo(p);
        Modernizr.csstransforms3d && (n = i("<div>").css("transform", "perspective(1px)"), this.perspectiveUnit = "none" != n.css("transform") ? "px" : "", n = null);
        i.browser.msie && i(".BookHtml5-handle", s).css({
            backgroundColor: "#fff",
            opacity: "0.01"
        });
        i(".BookHtml5-handle", p).bind("mousedown.BookHtml5", function(u) {
            return r.pageEdgeDragStart(u)
        });
        q.curl && i(".BookHtml5-handle", p).hover(function(u) {
            if (!r.curlTimer) {
                var z = this == r.leftHandle[0] ? r.leftPage() : r.rightPage();
                if (z) {
                    var v = z.offset(),
                        w = u.pageY - v.top;
                    r.curlTimer = setTimeout(function() {
                        r.curl(z, w > r.pageHeight / 2)
                    }, 120)
                }
            }
        }, function() {
            r.curlTimer = clearTimeout(r.curlTimer);
            r.uncurl()
        });
        var g;
        i(".BookHtml5-handle", p).on("mousedown.BookHtml5", function() {
            g = i.now()
        }).on("mouseup.BookHtml5", function() {
            if (!((new Date).getTime() - g > r.opts.handleClickDuration)) {
                r.stopAnimation();
                var u = i(this).data("corner");
                if ("r" === u) {
                    r[r.rtl ? "back" : "advance"]()
                }
                if ("l" === u) {
                    r[r.rtl ? "advance" : "back"]()
                }
            }
        });
        var l = !1;
        s.hover(function(u) {
            if (r.pauseOnHover) {
                l = r.slideShowTimer;
                u = i(r.opts.controls.slideShow);
                var v = u.hasClass("BookHtml5-disabled");
                r.stopSlideShow();
                u.toggleClass("BookHtml5-disabled", v)
            }
        }, function() {
            r.pauseOnHover && l && r.startSlideShow()
        });
        this.clipBoundaries && (this.origin.wrap("<div class='BookHtml5-clipper'><div class='BookHtml5-inner-clipper'></div></div>"), i(".BookHtml5-inner-clipper", s).css({
            position: "absolute",
            width: "100%",
            overflow: "hidden"
        }), this.clipper = i(".BookHtml5-clipper", s).css({
            position: "absolute",
            left: 0,
            top: 0,
            width: "100%",
            overflow: "hidden",
            zIndex: 1
        }));
        this.detectBestZoomMethod();
        s.wrapInner("<div class='BookHtml5-zoomwindow'><div class='BookHtml5-zoomcontent'></div></div>");
        this.zoomWindow = i(".BookHtml5-zoomwindow", s);
        this.zoomContent = i(".BookHtml5-zoomcontent", s);
        i.browser.ie8mode && this.zoomContent.wrapInner("<div class='IE8-zoom-helper'style='position:relative'></div>");
        this.zoomWindow.css({
            position: "absolute",
            top: 0
        });
        this.zoomContent.css({
            position: "absolute",
            left: 0,
            top: 0
        });
        this.setDimensions(this.opts.width, this.opts.height);
        q.scaleToFit && (this.scaleToFit(), this.responsive());
        this.fillToc();
        p = q.useTranslate3d;
        Modernizr.csstransforms3d && p && (i.BookHtml5.useTranslate3d = !0 == p || "mobile" == p && i.BookHtml5.utils.isMobile());
        if (q.deepLinking && this.getLocationHash()) {
            var d;
            try {
                d = r.selectorToPage("#" + this.getLocationHash())
            } catch (t) {}
            void 0 === d && (d = r.locationHashToPage());
            "number" === typeof d && (r.startPage = d, k(r.elem) || r.elem[0].scrollIntoView())
        }
        this.flipSound && (this.audio = this.createAudioPlayer());
        if (this.opts.keyboardNavigation) {
            i(document).on("keydown.BookHtml5", function(u) {
                i(u.target).filter("input, textarea, select").length || (u.which === r.opts.keyboardNavigation.back && r.back(), u.which === r.opts.keyboardNavigation.advance && r.advance())
            })
        }
        i(window).on("hashchange.BookHtml5", function() {
            var u = window.location.hash;
            if (u === r.locationHashSetTo) {
                r.locationHashSetTo = void 0
            } else {
                var v = (u = "" === u && !r.locationEndsInHash()) ? r.startPage : r.locationHashToPage();
                "number" === typeof v && (r.gotoPage(v, !u), u || k(r.elem) || r.elem[0].scrollIntoView())
            }
        });
        q.forceBasicPage && (this.foldPage = this.holdHardpage = this.foldPageBasic);
        i.BookHtml5.support.transform || (this.foldPage = this.foldPageBasic, i.BookHtml5.support.filters || (this.holdHardpage = this.foldPageBasic));
        this.mouseWheel && s.mousewheel(function(u, v) {
            if (r.mouseWheel) {
                return "zoom" === r.mouseWheel ? (0 < v && r.zoomIn(), 0 > v && r.zoomOut(), r.zoomedAt(u)) : (0 < v && r.advance(), 0 > v && r.back()), !1
            }
        });
        this.opts.touchEnabled && this.touchSupport();
        this.controllify(this.opts.controls);
        this.opts.thumbnails && this.createThumbnails();
        this.setupFullscreen();
        this.showPage(this.startPage, !1);
        this.zoom(this.opts.zoomLevel);
        this.callRAFCallback = function() {
            r.rafCallback()
        };
        window.raf(this.callRAFCallback);
        q.slideShow && this.startSlideShow()
    }

    function m(g, n) {
        var d = Math.cos(n),
            l = Math.sin(n);
        return {
            x: d * g.x - l * g.y,
            y: l * g.x + d * g.y
        }
    }

    function k(g) {
        var n = i(window).height(),
            d = g.offset(),
            l = i(window).scrollTop();
        return d.top > l && d.top + g.height() < l + n
    }

    function b(l, s, g) {
        var q, r, n, p;
        q = r = 0;
        if (!i.BookHtml5.support.boxSizing) {
            var d;
            q = "none" == l.css("borderTopStyle") ? 0 : o[d = l.css("borderTopWidth")] || parseFloat(d);
            r = "none" == l.css("borderRightStyle") ? 0 : o[d = l.css("borderRightWidth")] || parseFloat(d);
            n = "none" == l.css("borderBottomStyle") ? 0 : o[d = l.css("borderBottomWidth")] || parseFloat(d);
            p = "none" == l.css("borderLeftStyle") ? 0 : o[d = l.css("borderLeftWidth")] || parseFloat(d);
            r = parseFloat(l.css("paddingLeft")) + parseFloat(l.css("paddingRight")) + p + r;
            q = parseFloat(l.css("paddingTop")) + parseFloat(l.css("paddingBottom")) + q + n
        }
        l.css("width", s - r);
        l.css("height", g - q)
    }
    i.BookHtml5 = function(d) {
        return i(d).data("BookHtml5")
    };
    i.BookHtml5.support = {};
    i.fn.BookHtml5 = function(g) {
        if ("string" === typeof g) {
            var p = Array.prototype.slice.call(arguments, 1);
            if ("options" === g || "prop" === g) {
                var d = i.BookHtml5(this[0]),
                    l = p[0];
                return 1 < p.length ? d[l] = p[1] : d[l]
            }
            return this.each(function() {
                var q = i.BookHtml5(this);
                q[g].apply(q, p)
            })
        }
        var n = i.extend({}, i.BookHtml5.defaults, g);
        return this.each(function() {
            var q = new f(this, n);
            i(this).data("BookHtml5", q)
        })
    };
    f.prototype = {
        destroy: function() {
            this.callRAFCallback = i.noop;
            this.curlTimer = clearTimeout(this.curlTimer);
            i("*").add(document).add(window).off(".BookHtml5");
            this.destroyThumbnails();
            this.stopSlideShow();
            this.stopAnimation(!1);
            this.elem.empty().removeData().off()
        },
        setDimensions: function(l, s) {
            this.zoomed && this.zoom(1);
            this.currentScale = 1;
            var g = this.elem,
                q = this.pageWidth;
            g.css({
                height: s,
                width: l
            });
            var r = g.height();
            this.pageWidth = g.width() / 2;
            this.pageHeight = r;
            this._originalHeight || (this._originalHeight = this.pageHeight);
            this._originalWidth || (this._originalWidth = 2 * this.pageWidth);
            var n = this.origin.css({
                width: "100%",
                height: r
            });
            if (q && this.centeredWhenClosed) {
                var p = parseFloat(n.css("left"), 10);
                n.css("left", p / (q / this.pageWidth))
            }
            this.bookShadow.css({
                width: l,
                height: s
            });
            for (var p = 0, d = this.pages.length; p < d; p++) {
                this.pages[p].css({
                    width: this.pageWidth,
                    height: this.pageHeight,
                    left: this.pages[p].onLeft ? 0 : this.pageWidth
                }), q = i(".BookHtml5-page-content", this.pages[p]), b(q, this.pageWidth, this.pageHeight)
            }
            this.opts.gutterShadow && i(".BookHtml5-gutter-shadow", g).css("height", this.pageHeight);
            this.positionBookShadow();
            this.shadowClipper.css({
                width: g.width(),
                height: r
            });
            this.hardPageShadow.css({
                width: this.pageWidth,
                height: this.pageHeight
            });
            this.opts.handleWidth && i(".BookHtml5-handle", n).css("width", this.opts.handleWidth);
            this.rightHandle.css("left", l - this.rightHandle.width());
            this.clipBoundaries && (n = Math.ceil(Math.sqrt(this.pageWidth * this.pageWidth + this.pageHeight * this.pageHeight)), n = [this.pageHeight - n, g.width(), n, 0], this.origin.css("top", -n[0]), i(".BookHtml5-inner-clipper", g).css({
                width: "100%",
                height: n[2] - n[0],
                top: n[0]
            }), this.clipper.css({
                width: "100%",
                height: r
            }));
            this.zoomWindow.css({
                width: 2 * this.pageWidth,
                height: r
            });
            this.zoomContent.css({
                width: 2 * this.pageWidth,
                height: r
            });
            this.corners = {
                tl: [0, 0],
                bl: [0, this.pageHeight],
                tr: [this.pageWidth, 0],
                br: [this.pageWidth, this.pageHeight],
                l: [0, 0],
                r: [this.pageWidth, 0]
            };
            this.thumbnails && this.updateThumbnails()
        },
        scale: function(d) {
            if (i.BookHtml5.support.transform) {
                this.zoomed && this.zoom(1);
                this.currentScale = d;
                var g = this.zoomContent;
                this.opts.zoomUsingTransform ? g.css({
                    transform: "scale(" + d + ")",
                    transformOrigin: "0 0"
                }) : g.css("zoom", d);
                this.elem.css({
                    height: this._originalHeight * d,
                    width: this._originalWidth * d
                });
                if (this.opts.onResize) {
                    this.opts.onResize(this)
                }
            }
        },
        scaleToFit: function(g, n) {
            var d = g;
            if (!i.isNumeric(g)) {
                var l = i(g || this.opts.scaleToFit);
                if (!l.length) {
                    throw "jQuery selector passed to BookHtml5.resize did not matched in any DOM element."
                }
                d = l.width();
                n = l.height()
            }
            l = this._originalWidth / this._originalHeight;
            n * l <= d || (n = d / l);
            this.scale(n / this._originalHeight)
        },
        resize: function(d, g) {
            this.setDimensions(d, g);
            if (this.opts.onResize) {
                this.opts.onResize(this)
            }
        },
        responsive: function() {
            var d = this;
            /*i(window).on("resize.BookHtml5", function() {
                d.scaleToFit()
            })*/
        },
        insertPages: function(g, n) {
            for (var d = 0, l = g.length; d < l; d++) {
                this.insertPage(g[d], !0)
            }
            this.updateBook(n)
        },
        insertPage: function(g, l) {
            if (this.isDoublePage(g)) {
                this.insertDoublePage(g, l)
            } else {
                g = i(g).addClass("BookHtml5-page-content");
                var d = i("<div class='BookHtml5-page'></div>").css({
                    width: this.pageWidth,
                    height: this.pageHeight,
                    display: "none",
                    position: "absolute"
                }).appendTo(this.pagesContainer).append(g);
                i.BookHtml5.support.boxSizing && g.css(i.BookHtml5.support.boxSizing, "border-box");
                b(g, this.pageWidth, this.pageHeight);
                d.hardPageSetByUser = g.hasClass("BookHtml5-hardpage");
                this.opts.gutterShadow && i("<div class=''></div>").appendTo(g).css("height", this.pageHeight);
                this.pages.push(d);
                l || this.updateBook();
                return d
            }
        },
        insertDoublePage: function(g, p) {
            if (!this._insertingDoublePage) {
                this._insertingDoublePage = !0;
                var d = i(g),
                    l = d.clone().insertAfter(d),
                    n = d.add(l);
                d.css("left", 0);
                l.css("right", "100%");
                n.css({
                    width: "200%",
                    height: "100%",
                    position: "relative"
                });
                n.css(i.BookHtml5.support.boxSizing + "", "border-box").wrap("<div class='BookHtml5-double-page'></div>");
                this.insertPage(d.parent(), !0);
                this.insertPage(l.parent(), !0);
                i.BookHtml5.support.boxSizing || (b(d, 2 * this.pageWidth, this.pageHeight), b(l, 2 * this.pageWidth, this.pageHeight));
                p || this.updateBook();
                this._insertingDoublePage = !1
            }
        },
        isDoublePage: function(d) {
            d = i(d);
            return d.data("double") || d.is(this.opts.doublePages)
        },
        removePages: function(l, r) {
            arguments.length || (l = 0, r = -1);
            this.holdedPage && this.releasePage(this.holdedPage);
            var g = this.pages;
            r = (r || l) + 1 || g.length;
            var n = g.slice(l, r),
                p = g.slice(r);
            g.length = 0 > l ? g.length + l : l;
            g.push.apply(g, p);
            for (var p = 0, q = n.length; p < q; p++) {
                n[p].remove()
            }
            this.updateBook();
            return g.length
        },
        updateBook: function(g) {
            this.doPageNumbering();
            this.findPagesType();
            this.positionBookShadow();
            for (var p = this.rtl, d, l = 0, n = this.pages.length; l < n; l++) {
                d = this.pages[l].toggleClass("BookHtml5-left", p).toggleClass("BookHtml5-right", !p).data({
                    pageIndex: l,
                    holded: !1
                }), d.pageIndex = l, d.onLeft = p, d.onRight = !p, p = !p
            }
            this.findSections();
            g || this.showPage(this.currentPage)
        },
        doPageNumbering: function() {
            var g = this.opts;
            if (g.pageNumbers) {
                var p = g.numberedPages,
                    g = this.pages.length - 1,
                    d = this.pageIsOnTheLeft(g);
                "all" == p && (p = [0, -1]);
                p || (p = [2, d ? -3 : -2]);
                var l = p[0],
                    p = p[1];
                0 > l && (l = g + l + 1);
                0 > l && (l = 0);
                l > this.pages.length - 1 && (l = g);
                0 > p && (p = g + p + 1);
                0 > p && (p = 0);
                p > this.pages.length - 1 && (p = g);
                for (var n = this.opts.firstPageNumber, d = 0; d < l; d++) {
                    i(".BookHtml5-page-number", this.pages[d]).remove()
                }
                for (d = p + 1; d < g; d++) {
                    i(".BookHtml5-page-number", this.pages[d]).remove()
                }
                for (d = l; d <= p; d++) {
                    g = i(".BookHtml5-page-number", this.pages[d]), g.length || (g = i(".BookHtml5-page-content", this.pages[d]), g = i('<div class="BookHtml5-page-number"></div>').appendTo(g)), g.html(n++)
                }
            }
        },
        findPagesType: function() {
            var l = this.opts,
                r = {},
                g, n, p;
            p = l.hardPages || [];
            var q = !0 === p;
            if (!q) {
                for (l.hardcovers && (p.push(0, 1, -1), this.pageIsOnTheLeft(this.pages.length - 1) && p.push(-2)), l = 0, n = p.length; l < n; l++) {
                    g = p[l], 0 > g && (g = this.pages.length + g), 0 <= g && g < this.pages.length && (r[g] = !0)
                }
            }
            for (l = this.pages.length; l--;) {
                p = q || r[l] || this.pages[l].hardPageSetByUser, this.pages[l].toggleClass("BookHtml5-hardpage", p).isHardPage = p
            }
        },
        showPage: function(A, w) {
            0 > A && (A = 0);
            A > this.pages.length - 1 && (A = this.pages.length - 1);
            for (
            	var z = this.leftPageIndex(A),
            	s = this.rightPageIndex(A),
            	u = this.pageBelow(z),
            	n = this.pageBelow(s),
            	q = this.pageWidth,
            	l = this.rtl,
            	B = this.pages.length - 1,
            	d,
            	r,
            	C = 0;
            	C <= B;
            	C++) {
                d = this.pages[C].onLeft != this.rtl ? C : B - C, r = this.transparentPages ? l ? C <= z ? "block" : "none" : C >= s ? "block" : "none" : C === u || C === z || C === s || C === n ? "block" : "none", 
                this.pages[C].data("zIndex", d).css({
                    display: r,
                    left: this.pages[C].onLeft ? 0 : q,
                    top: 0,
                    zIndex: d
                }), l = !l
            }
            u = 0 == A;
            B = A == B || A == this.otherPage(B);
            this.leftHandle.toggleClass("BookHtml5-disabled", !this.backPage(z));
            this.rightHandle.toggleClass("BookHtml5-disabled", !this.backPage(s));
            this.toggleControl("back", u);
            this.toggleControl("next", B);
            this.toggleControl("first", u);
            this.toggleControl("last", B);
            (z = this.onShowPage) && i.isFunction(z) && !this.isOnPage(A) && (z(this, this.pages[A], A), (s = this.otherPage(A)) && z(this, this.pages[s], s));
            this.currentPage = A;
            this.centeredWhenClosed && (z = !!this.leftPage(A), s = this.rightPage(A), this.origin.css("left", z && s ? 0 : z ? q / 2 : -q / 2));
            this.positionBookShadow();
            !1 !== w && this.opts.updateBrowserURL && this.locationHashToPage() != A && (this.locationHashSetTo = window.location.hash = this.pageToLocationHash(A));
            this.showThumbnail()
        },
        holdPage: function(V, T, U, Q, R) {
            "number" === typeof V && (V = this.pages[V]);
            if (V) {
                var S = V.pageIndex,
                    O = this.pages.length - 1,
                    M = !this.otherPage(O);
                if (!Q) {
                    Q = this.pageIsOnTheLeft(S) ? "l" : "r"
                } else {
                    if (!this.corners[Q] || (this.pageIsOnTheLeft(S) ? /r/ : /l/).test(Q)) {
                        return
                    }
                }
                void 0 === R && (R = this.backPage(S));
                if (R) {
                    var D = R.pageIndex;
                    V.data("holded_info", [T, U, Q]);
                    if (this.centeredWhenClosed && (0 === S || 0 === D || M && (D === O || S === O))) {
                        var K = 0,
                            P, F = !this.rtl,
                            I = this.pageWidth,
                            H, L, N, E, G, C, B, J;
                        if (F ? 0 === S : S === O && M) {
                            H = -I / 2, L = -I / 4, N = 0, E = -I / 2, G = -I, C = L, B = G, J = T
                        }
                        if (F ? S === O && M : 0 === S) {
                            H = I, L = 3 * I / 2, N = I / 2, E = 0, G = I, C = 2 * I, B = T, J = C
                        }
                        if (F ? 0 === D : D === O && M) {
                            H = I / 2, L = S === (F ? O : 0) ? I : 3 * I / 2, N = S === (F ? O : 0) ? I / 2 : 0, E = -I / 2, G = H, C = 2 * I, B = T, J = C
                        }
                        if (F ? D === O && M : 0 === D) {
                            H = S === (F ? 0 : O) ? 0 : -I / 2, L = I / 2, N = I / 2, E = S === (F ? 0 : O) ? -I / 2 : 0, G = -I, C = L, B = G, J = T
                        }
                        T < H && (K = N, P = B);
                        T > L && (K = E, P = J);
                        T >= H && T <= L && (T = (T - H) / (L - H), K = N + T * (E - N), P = G + T * (C - G));
                        T = P;
                        this.origin.css("left", K);
                        this.positionBookShadow()
                    }
                    this.zoomed || "basic" == this.pageType(V) || "basic" == this.pageType(R) ? this.foldPageBasic(V, T, U, Q, R) : V.isHardPage || R.isHardPage ? this.holdHardpage(V, T, U, Q, R) : this.foldPage(V, T, U, Q, R);
                    if (!V.data("holded") && (V.addClass("BookHtml5-page-holded"), R.addClass("BookHtml5-page-holded"), V.data("holded", !0), this.holdedPage = V, this.holdedPageBack = R, this.shadows && this.shadowClipper.css("display", "block"), this.clipBoundaries && this.clipper.css("overflow", "visible"), this.positionBookShadow(), this.opts.onHoldPage)) {
                        this.opts.onHoldPage(this, S, V, R)
                    }
                }
            }
        },
        foldPage: function(g, p, d, l, n) {
            this._currentFlip || (this._currentFlip = this.foldPageStart(g, p, d, l, n));
            this._currentFlip.page == g && (this._currentFlip.x = p, this._currentFlip.y = d, this._currentFlip.page.data("holdedAt", {
                x: p,
                y: d
            }), this._currentFlip.corner = l, this.foldPageStyles(this._currentFlip))
        },
        foldPageStart: function(t, r, s, n, p) {
            var q = {};
            "number" === typeof t && (t = this.pages[t]);
            q.book = this;
            q.page = t;
            q.pageIndex = t.data("pageIndex");
            void 0 === p && (p = this.backPage(q.pageIndex));
            if (p && p.length) {
                q.back = p;
                q.pageContent = t.children().first();
                q.backContent = p.children().first();
                var l = this.pageWidth,
                    g = this.pageHeight;
                n || (n = "tl");
                if ("l" == n || "r" == n) {
                    var u = {
                        x: "l" == n ? 0 : l,
                        y: s
                    };
                    t.data("grabPoint", u);
                    q.grabPoint = u;
                    n = (s >= u.y ? "t" : "b") + n
                }
                q.page.data("holdedAt", {
                    x: r,
                    y: s
                });
                q.x = r;
                q.y = s;
                q.page.data("holdedCorner", n);
                q.corner = n;
                q.pageDiagonal = Math.sqrt(l * l + g * g);
                q.page.css("clip", "rect(-1000px 2000px 2000px 0px)");
                q.pageLeft = parseFloat(t.css("left"));
                p.css({
                    left: q.pageLeft + "px",
                    zIndex: 1000,
                    clip: "rect(-1000px 2000px 2000px 0px)"
                });
                q.shadowHeight = 2 * Math.ceil(q.pageDiagonal);
                q.shadowTop = -(q.shadowHeight - g) / 2;
                this.internalShadow.css({
                    display: "block",
                    height: q.shadowHeight
                });
                q.foldShadowWidth = this.foldShadow.width();
                this.foldShadow.css({
                    display: "block",
                    height: q.shadowHeight
                });
                this.foldGradientContainer.appendTo(q.backContent);
                q.foldGradientWidth = this.foldGradientElem.width();
                q.foldGradientHeight = 2 * Math.ceil(q.pageDiagonal);
                this.foldGradientElem.css("height", q.foldGradientHeight);
                this.foldGradientContainer.css({
                    position: "absolute",
                    width: q.foldGradientWidth,
                    height: q.foldGradientHeight,
                    top: 0,
                    left: 0,
                    display: "none"
                });
                q.foldGradientVisible = !1;
                return q
            }
        },
        foldPageStyles: function(O) {
            var M = this.pageWidth,
                N = this.pageHeight,
                K = M / 2,
                L = N / 2,
                H = i.BookHtml5.utils.translate,
                I = O.pageLeft,
                F = O.x,
                u = O.y,
                D = O.back,
                J = O.corner || "tl";
            if ("l" == J || "r" == J) {
                var z = O.page.data("grabPoint");
                z || (z = {
                    x: "l" == J ? 0 : M,
                    y: u
                }, page.data("grabPoint", z));
                J = (u >= z.y ? "t" : "b") + (this.pageIsOnTheLeft(O.pageIndex) ? "l" : "r");
                O.corner = J;
                O.page.data("holdedCorner", J);
                var C = F - z.x,
                    B = u - z.y,
                    C = Math.atan2(B, C),
                    z = {
                        x: 0,
                        y: (u >= z.y ? 0 : N) - z.y
                    },
                    z = m(z, 2 * C),
                    F = z.x + F,
                    u = z.y + u
            }
            O.page.data("holdedAt", {
                x: F,
                y: u
            });
            O.page.data("holdedCorner", J);
            var z = this.corners[J],
                J = M - z[0],
                E = z[1],
                C = F - J,
                B = u - E,
                G = Math.sqrt(C * C + B * B);
            G > M && (F = J + M * C / G, u = E + M * B / G);
            var E = N - E,
                C = F - J,
                B = u - E,
                G = Math.sqrt(C * C + B * B),
                A = O.pageDiagonal;
            G > A && (F = J + A * C / G, u = E + A * B / G);
            J = z[0];
            z = z[1];
            z == u && (u = z + 0.001);
            C = F - J;
            B = u - z;
            G = Math.sqrt(C * C + B * B);
            F = G / 2;
            C = Math.atan2(B, C);
            B = Math.tan(C);
            u = C;
            C = 180 * C / Math.PI;
            E = {
                x: J - K,
                y: L - z
            };
            A = m(E, u);
            G = A.x + F + K + 0.5;
            O.pageContent.css("transform", H(-G, 0) + " rotate(" + (-C).toFixed(7) + "deg)");
            O.page.css("transform", H((Math.cos(u) * G).toFixed(5), (Math.sin(u) * G).toFixed(5)) + " rotate(" + C.toFixed(7) + "deg)");
            A = this.calculateOpacity(F, M, this.shadowThreshold, 50);
            if (this.shadows && 0 < A) {
                var d = O.shadowTop;
                this.internalShadow.css({
                    transform: H(G + I, d) + " rotate(" + C + "deg)",
                    transformOrigin: K - G + "px " + (L + (O.shadowHeight - N) / 2) + "px"
                });
                G -= O.foldShadowWidth;
                this.foldShadow.css({
                    transform: H(G + I, d) + " rotate(" + C + "deg)",
                    transformOrigin: K - G + "px " + (L + (O.shadowHeight - N) / 2) + "px"
                });
                this.shadowContainer.css({
                    opacity: A,
                    display: "block"
                })
            } else {
                this.shadowContainer.css("display", "none")
            }
            D.show();
            E.x = -E.x;
            A = m(E, -u);
            G = A.x - F + K - 1;
            N = {
                x: A.x - F,
                y: A.y + F * B
            };
            I = {
                x: A.x - F,
                y: A.y - F / (0 == B ? 0.0001 : B)
            };
            N = m(N, -u);
            I = m(I, -u);
            I = -(I.x + K);
            N = -(N.y - L);
            O.backContent.css("transform", H(-G, 0) + " rotate(" + C + "deg)");
            O.back.css("transform", H((J + I + Math.cos(u) * G).toFixed(5), (z - N + Math.sin(u) * G).toFixed(5)) + " rotate(" + C + "deg)");
            A = this.calculateOpacity(2 * F, 2 * M, this.foldGradientThreshold, 50);
            this.foldGradient && 0 < A ? (this.foldGradientContainer.css({
                opacity: A,
                display: "block",
                transform: H(M - J - O.foldGradientWidth / 2, z - O.foldGradientHeight / 2) + " rotate(" + -C + "deg)"
            }), this.foldGradientElem.css("transform", H(-F + O.foldGradientWidth / 2, 0)), O.foldGradientVisible || (this.foldGradientContainer.css("display", "block"), O.foldGradientVisible = !0)) : O.foldGradientVisible && (this.foldGradientContainer.css("display", "none"), O.foldGradientVisible = !1)
        },
        holdHardpage: function(l, q, g, n, p) {
            this._currentFlip || (this._currentFlip = this.flipHardPageStart(l, q, g, n, p));
            this._currentFlip.page == l && (this._currentFlip.x = q, this._currentFlip.y = g, this._currentFlip.page.data("holdedAt", {
                x: q,
                y: g
            }), this._currentFlip.corner = n, this.flipHardPageStyles(this._currentFlip))
        },
        flipHardPageStart: function(l, s, g, q, r) {
            this.clipBoundaries && this.clipper.children(".BookHtml5-inner-clipper").css("overflow", "visible");
            var n = {};
            "number" === typeof l && (l = this.pages[l]);
            n.book = this;
            n.page = l;
            n.pageIndex = l.data("pageIndex");
            void 0 === r && (r = this.backPage(n.pageIndex));
            if (r && r.length) {
                n.back = r;
                var p = this.pageWidth;
                q || (q = "tl");
                l.data("holdedAt", {
                    x: s,
                    y: g
                });
                l.data("holdedCorner", q);
                l.css("zIndex", 1000);
                r.css("zIndex", 1000);
                this.opts.use3d && Modernizr.csstransforms3d && (l.css(Modernizr.prefixed("perspectiveOrigin"), "0 50%"), r.css(Modernizr.prefixed("perspectiveOrigin"), "0 50%"));
                s = this.pageIsOnTheLeft(n.pageIndex) ? p : 0;
                l.css("transformOrigin", s + "px 50%");
                r.css("transformOrigin", p - s + "px 50%");
                this.shadows && this.hardPageShadow.css("display", "block");
                return n
            }
        },
        flipHardPageStyles: function(u) {
            var s = u.page,
                t = u.back;
            x = u.x;
            y = u.y;
            var q = this.pageIsOnTheRight(u.pageIndex),
                r = this.pageWidth,
                l = this.pageHeight;
            (u = u.corner) || (u = "tl");
            s.data("holdedAt", {
                x: x,
                y: y
            });
            s.data("holdedCorner", u);
            u = q ? r - x : x;
            var n = q ? 0 : r,
                d;
            0 > u && (u = 0);
            u = (d = u < r) ? s : t;
            (d ? t : s).css("display", "none");
            s = q != d;
            t = x - n;
            t > r && (t = r);
            t < -r && (t = -r);
            n = -Math.sqrt(1600 * (1 - t * t / ((r + 15) * (r + 15))));
            q = Math.abs(t / r);
            n = 1 == q ? 0 : Math.atan2(n, t);
            if (this.opts.use3d && Modernizr.csstransforms3d) {
                var p;
                p = s ? -this._calculateAngleFromX(-t, r) : this._calculateAngleFromX(t, r);
                this.animating && (d = this._animationData, this.curledPage || d.curled ? (d.curled = !0, d.angle || (n = {
                    from: p,
                    to: 0
                }, d.angle = n, p = t + d.dx, n.to = s ? -this._calculateAngleFromX(-p, r) : this._calculateAngleFromX(p, r), Math.abs(t) == r && (n.from = 0), Math.abs(p) == r && (n.to = 0), n.delta = n.to - n.from), p = d.angle.from - d.angle.delta * (d.from.x - x) / d.dx) : (Math.abs(x - d.from.x), Math.abs(d.dx / 2)));
                u.css({
                    transform: "perspective(" + this.opts.perspective + this.perspectiveUnit + ") rotate3d(0, 1, 0, " + p + "deg)",
                    display: "block"
                })
            } else {
                u.css({
                    transform: "skewY(" + n + "rad) scaleX(" + q + ")",
                    display: "block"
                })
            }!i.BookHtml5.support.transform && i.BookHtml5.support.filters && (q = "M11=" + q + ", M12=0, M21=" + Math.tan(n) * q + ", M22=1", u.css("filter", "progid:DXImageTransform.Microsoft.Matrix(" + q + ", sizingMethod='auto expand')"), u.css({
                marginTop: l - u.height(),
                marginLeft: s ? r - u.width() : 0
            }));
            this.shadows && this.hardPageShadow.css({
                left: s ? 0 : r,
                opacity: 0.5 * Math.abs(t / r)
            })
        },
        _calculateAngleFromX: function(l, r, g) {
            var p = 2 * r / 3;
            if (l > p) {
                var q = this._calculateAngleFromX(p, r, g);
                return q + (l - p) / (r - p) * (0 - q)
            }
            g = g || this.opts.perspective;
            p = 180 / Math.PI;
            q = r * r;
            g *= g;
            var n = l * l;
            l = Math.acos((r * g * l - Math.sqrt(q * q * g * n + q * q * n * n - q * g * n * n)) / (q * g + q * n));
            return l = -l * p
        },
        foldPageBasic: function(l, q, g, p, n) {
            this._currentFlip || (this._currentFlip = this.foldPageBasicStart(l, q, g, p, n));
            this._currentFlip && this._currentFlip.page == l && (this._currentFlip.x = q, this._currentFlip.y = g, this._currentFlip.page.data("holdedAt", {
                x: q,
                y: g
            }), this._currentFlip.corner = p, this.foldPageBasicStyles(this._currentFlip))
        },
        foldPageBasicStart: function(l, s, g, r, q) {
            var n = {};
            "number" === typeof l && (l = this.pages[l]);
            n.book = this;
            n.page = l;
            n.pageIndex = l.data("pageIndex");
            void 0 === q && (q = this.backPage(n.pageIndex));
            if (q && q.length) {
                n.back = q;
                var p = this.pageHeight;
                r || (r = "tl");
                l.data("holdedAt", {
                    x: s,
                    y: g
                });
                l.data("holdedCorner", r);
                q.css("zIndex", 1000);
                l.data("foldPageBasic", !0);
                n.foldGradientWidth = this.foldGradientElem.width();
                n.foldShadowWidth = this.foldShadow.width();
                this.internalShadow.css("display", "none");
                this.foldShadow.css({
                    display: "none",
                    height: p,
                    transform: "",
                    top: 0
                }).toggleClass("BookHtml5-shadow-fold-flipped", l.onRight);
                this.shadowContainer.css("display", "block");
                s = q.children().first();
                this.foldGradientContainer.appendTo(s).css({
                    width: n.foldGradientWidth,
                    height: p,
                    top: 0,
                    transform: "",
                    zIndex: 1000000000
                });
                this.foldGradientElem.css({
                    left: 0,
                    height: p
                }).toggleClass("BookHtml5-fold-gradient-flipped", l.onRight);
                return n
            }
        },
        foldPageBasicStyles: function(v) {
            var t = v.page,
                u = v.back;
            x = v.x;
            y = v.y;
            var s = this.pageWidth,
                r = v.corner;
            r || (r = "tl");
            t.data("holdedAt", {
                x: x,
                y: y
            });
            t.data("holdedCorner", r);
            var n = (r = this.pageIsOnTheLeft(v.pageIndex)) ? x : s - x;
            0 > n && (n = 0);
            n > 2 * s && (n = 2 * s);
            var p = n / 2,
                l, q;
            r ? (l = "rect(-1000px 1000px 1000px " + p + "px)", q = "rect(-1000px 1000px 1000px " + (s - p) + "px)", n -= s) : (l = "rect(-1000px " + (s - p) + "px 1000px -1000px)", q = "rect(-1000px " + p + "px 1000px -1000px)", n = s - n + s);
            t.css("clip", l);
            u.css({
                clip: q,
                left: n,
                display: "block"
            });
            t = this.calculateOpacity(2 * p, 2 * s, this.shadowThreshold, 50);
            this.shadows && 0 < t ? (u = r ? p - v.foldShadowWidth : s - p + s, this.shadowContainer.css("opacity", t), this.foldShadow.css({
                left: u,
                display: "block"
            })) : this.foldShadow.css("display", "none");
            t = this.calculateOpacity(2 * p, 2 * s, this.foldGradientThreshold, 50);
            this.foldGradient && 0 < t ? (u = r ? s - p : p - v.foldGradientWidth, this.foldGradientContainer.css({
                opacity: t,
                left: u,
                display: "block"
            })) : this.foldGradientContainer.css("display", "none")
        },
        stopAnimation: function(d) {
            arguments.length || (d = !0);
            i(this).stop(!0, d);
            this.animating = !1
        },
        flip: function(l, s, g, q) {
        	if(this.disableFlipAnimation) return;
            q || (q = i.isPlainObject(l) ? l : {});
            q.from || (q.from = []);
            q.to || (q.to = []);
            var r = this;
            if (!r.animating) {
                r.animating = !0;
                g || (g = q.page || r.holdedPage);
                var n = g.data("holded_info");
                g.data("holdedAt");
                var p = q.corner || g.data("holdedCorner"),
                    d = i.easing[q.easing] || q.easing || function(t) {
                        return 1 == t ? 1 : -Math.pow(2, -10 * t) + 1
                    };
                l = {
                    page: g,
                    back: q.back || r.holdedPageBack || r.backPage(g.pageIndex),
                    initialX: void 0 != q.from[0] ? q.from[0] : n[0],
                    initialY: void 0 != q.from[1] ? q.from[1] : n[1],
                    finalX: void 0 != q.to[0] ? q.to[0] : l,
                    finalY: void 0 != q.to[1] ? q.to[1] : s,
                    corner: p || n[2],
                    duration: q.duration,
                    complete: function() {
                        r.animating = !1;
                        i.isFunction(q.complete) && q.complete()
                    },
                    easing: d,
                    arc: q.arc,
                    dragging: q.dragging,
                    start: i.now(),
                    finished: !1
                };
                l.deltaX = l.finalX - l.initialX;
                l.deltaY = l.finalY - l.initialY;
                this._animationData = {
                    from: {
                        x: l.initialX,
                        y: l.initialY
                    },
                    to: {
                        x: l.finalX,
                        y: l.finalY
                    },
                    dx: l.deltaX
                };
                void 0 == l.duration && (l.duration = this.turnPageDuration * Math.abs(l.deltaX) / (2 * this.pageWidth));
                l.duration < this.opts.turnPageDurationMin && (l.duration = this.opts.turnPageDurationMin);
                !g.isHardPage && 0.4 < l.duration / this.turnPageDuration && this.playFlipSound();
                this.currentFlip = l
            }
        },
        rafCallback: function() {
            window.raf(this.callRAFCallback);
            if (this.currentFlip && !this.currentFlip.finished) {
                var d = this.currentFlip,
                    g = (i.now() - d.start) / d.duration;
                1 <= g && (g = 1);
                d.x = d.initialX + d.deltaX * d.easing(g, d.duration * g, 0, 1, d.duration);
                d.y = d.initialY + d.deltaY * d.easing(g, d.duration * g, 0, 1, d.duration);
                d.arc && (d.y -= (0.5 - Math.abs(0.5 - d.easing(g, d.duration * g, 0, 1))) * this.pageHeight / 10);
                d.dragging && (d.x = d.initialX + 0.2 * d.deltaX, d.y = d.initialY + 0.2 * d.deltaY, d.initialX = d.x, d.initialY = d.y, d.deltaX = d.finalX - d.initialX, d.deltaY = d.finalY - d.initialY, 1 > d.deltaX && 1 > d.deltaY && 1 == g);
                this.holdPage(d.page, d.x, d.y, d.corner, d.back);
                1 == g && (d.finished = !0, d.complete && d.complete())
            }
        },
        releasePages: function() {
            for (var d = 0, g = this.pages.length; d < g; d++) {
                this.pages[d].data("holded") && this.releasePage(d)
            }
        },
        releasePage: function(l, s, g, r) {
            "number" === typeof l && (l = this.pages[l]);
            var q = this,
                n = l.data("holdedAt"),
                p = l.data("holdedCorner");
            if (s && n) {
                this.flip({
                    from: [n.x, n.y],
                    to: this.corners[p],
                    page: l,
                    easing: "linear",
                    duration: 10000,
                    duration: r,
                    complete: function() {
                        q.releasePage(l)
                    }
                })
            } else {
                if (s = l.data("pageIndex"), void 0 === g && (g = this.holdedPageBack || this.backPage(s)), this.holdedPageBack = this.holdedPage = null, l.data({
                        holded_info: null,
                        holdedAt: null,
                        holdedCorner: null,
                        grabPoint: !1,
                        foldPageBasic: null,
                        holded: !1
                    }), this.clipBoundaries && !this.zoomed && (this.clipper.css("overflow", "hidden"), this.clipper.children(".BookHtml5-inner-clipper").css("overflow", "hidden")), this.shadowClipper.css("display", "none"), this.internalShadow.parent().hide(), this.foldGradientContainer.hide(), this.hardPageShadow.hide(), this.resetPage(l), g && g.length && (this.resetPage(g), g.hide()), this.foldShadow.removeClass("BookHtml5-shadow-fold-flipped").css({
                        transform: "",
                        left: ""
                    }), this.foldGradientElem.removeClass("BookHtml5-fold-gradient-flipped").css("transform", ""), this.foldGradientContainer.css("transform", "").appendTo(this.pagesContainer), this.positionBookShadow(), this.opts.onReleasePage) {
                    this.opts.onReleasePage(this, s, l, g)
                }
            }
        },
        resetPage: function(g) {
            this._currentFlip = void 0;
            g.removeClass("BookHtml5-page-holded");
            this.resetCSS || (this.resetCSS = {
                transform: "",
                transformOrigin: "",
                clip: "auto",
                marginLeft: 0,
                marginTop: 0,
                filter: ""
            });
            var l = this.pageWidth,
                d = this.pageHeight;
            g.css(this.resetCSS).css({
                zIndex: g.data("zIndex"),
                width: l,
                height: d,
                left: this.pageIsOnTheLeft(g.data("pageIndex")) ? 0 : l
            });
            i.browser.msie && 9 > i.browser.version && g.attr("style", g.attr("style").replace(/clip\: [^;]+;/i, ""));
            g = i(".BookHtml5-page-content", g);
            g.css(this.resetCSS);
            b(g, l, d)
        },
        gotoPage: function(H, F) {
            var G, E;
            if (!this.animating && (this._cantStopAnimation = !0, "string" === typeof H && "#" == H.charAt(0) && (H = this.selectorToPage(H)), 0 > H && (H = 0), H > this.pages.length - 1 && (H = this.pages.length - 1), !this.isOnPage(H))) {
                var D = H < this.currentPage;
                G = this.rtl ? H > this.currentPage : D;
                var A = this,
                    B = G ? A.leftPage() : A.rightPage();
                if (B) {
                    this.uncurl(!0);
                    var w, C;
                    G ? (w = this.leftPage(H), C = this.rightPage(H)) : (w = this.rightPage(H), C = this.leftPage(H));
                    var u = C && C.is(":visible");
                    if (D) {
                        for (D = B.pageIndex - 1; 0 <= D; D--) {
                            this.pages[D].css("display", "none")
                        }
                    } else {
                        for (D = B.pageIndex + 1, E = this.pages.length; D < E; D++) {
                            this.pages[D].css("display", "none")
                        }
                    }
                    w && w.css("display", "block");
                    u && C.css("display", "block");
                    var z = H;
                    w = B.data("holdedAt");
                    u = B.data("holdedAt");
                    D = B.data("holdedCorner") || (G ? "tl" : "tr");
                    G ? (u = u || {
                        x: 0,
                        y: 0
                    }, G = 2 * A.pageWidth, E = "bl" != D ? 0 : this.pageHeight) : (u = u || {
                        x: this.pageWidth,
                        y: 0
                    }, G = -A.pageWidth, E = "br" != D ? 0 : this.pageHeight);
                    var v = "linear";
                    if (this.centeredWhenClosed && (B.isHardPage || C.isHardPage)) {
                        var t = this.pages.length - 1,
                            s = !this.rtl,
                            v = "easeOutCubic";
                        this.currentPage == (s ? 0 : t) && (G += this.pageWidth / 2, s ? H == t && this.pageIsOnTheLeft(H) : 0 == H) && (G += this.pageWidth / 2);
                        this.currentPage == (s ? t : 0) && (G -= this.pageWidth / 2, H == (s ? 0 : t) && (G -= this.pageWidth / 2))
                    }
                    A.flip({
                        from: [u.x, u.y],
                        to: [G, E],
                        easing: v,
                        arc: !w,
                        page: B,
                        back: C,
                        corner: D,
                        complete: function() {
                            A._cantStopAnimation = !1;
                            A.releasePage(B, !1);
                            A.showPage(z, F)
                        }
                    });
                    return H
                }
            }
        },
        back: function() {
            return this.gotoPage(this.currentPage - 2)
        },
        advance: function() {
            return this.gotoPage(this.currentPage + 2)
        },
        leftPage: function(d) {
            void 0 === d && (d = this.currentPage);
            return this.pages[this.leftPageIndex(d)] || null
        },
        leftPageIndex: function(d) {
            this.pageIsOnTheLeft(d) || (d += this.rtl ? 1 : -1);
            if (0 > d || d > this.pages.length - 1) {
                d = null
            }
            return d
        },
        rightPage: function(d) {
            void 0 === d && (d = this.currentPage);
            return this.pages[this.rightPageIndex(d)] || null
        },
        rightPageIndex: function(d) {
            this.pageIsOnTheRight(d) || (d += this.rtl ? -1 : 1);
            if (0 > d || d > this.pages.length - 1) {
                d = null
            }
            return d
        },
        pageIsOnTheRight: function(d) {
            return this.rtl ? !!(d % 2) : !(d % 2)
        },
        pageIsOnTheLeft: function(d) {
            return this.rtl ? !(d % 2) : !!(d % 2)
        },
        otherPage: function(d) {
            d = this.pageIsOnTheLeft(d) ? d + (this.rtl ? -1 : 1) : d + (this.rtl ? 1 : -1);
            if (0 > d || d > this.pages.length - 1) {
                d = null
            }
            return d
        },
        isOnPage: function(d) {
            return "number" === typeof d && (d === this.currentPage || d === this.otherPage(this.currentPage))
        },
        backPage: function(d) {
            return this.pages[d] ? this.pages[d + (d % 2 ? -1 : 1)] : null
        },
        pageBelow: function(d) {
            d = parseInt(d, 10);
            if (d != d) {
                return null
            }
            d += this.pageIsOnTheLeft(d) != this.rtl ? -2 : 2;
            if (0 > d || d > this.pages.length - 1) {
                d = null
            }
            return d
        },
        pageType: function(d) {
            d = "number" === typeof d ? this.pages[d] : d;
            return d.isHardPage ? "hard" : d.find(".BookHtml5-page-content.BookHtml5-basic-page").length ? "basic" : "soft"
        },
        calculateOpacity: function(l, p, g, n) {
            if (l <= g || l >= p - g) {
                return 0
            }
            if (l >= n && l <= p - n) {
                return 1
            }
            l > n && (l = p - l);
            return (l - g) / (n - g)
        },
        startSlideShow: function() {
            this.slideShowRunning = !0;
            this.advanceAfterTimeout(this.slideShowDelay);
            i(this.opts.controls.slideShow).addClass("BookHtml5-disabled")
        },
        advanceAfterTimeout: function(d) {
            var g = this;
            this.slideShowTimer = setTimeout(function() {
                g.animating || g.holdedPage ? g.advanceAfterTimeout(100) : (g.advance(), g.isOnPage(g.pages.length - 1) ? g.stopSlideShow() : g.advanceAfterTimeout(g.slideShowDelay + g.turnPageDuration))
            }, d)
        },
        stopSlideShow: function() {
            clearTimeout(this.slideShowTimer);
            this.slideShowTimer = void 0;
            this.slideShowRunning = !1;
            i(this.opts.controls.slideShow).removeClass("BookHtml5-disabled")
        },
        toggleSlideShow: function() {
            this.slideShowRunning ? this.stopSlideShow() : this.startSlideShow()
        },
        findSections: function(g) {
            g && (this.sectionDefinition = g);
            var r = this.sectionDefinition;
            g = [];
            var d;
            "string" === typeof r && (d = r, r = [], i(d, this.elem).each(function(t, s) {
                r.push(["#" + s.id, i(s).html()])
            }));
            if (i.isArray(r)) {
                for (var p = 0, q = r.length; p < q; p++) {
                    d = r[p];
                    if ("string" === typeof d) {
                        try {
                            d = [d, i(d, this.elem).html()]
                        } catch (l) {
                            continue
                        }
                    }
                    try {
                        d[2] = this.selectorToPage(d[0])
                    } catch (n) {
                        continue
                    }
                    void 0 !== d[2] && g.push({
                        id: d[0],
                        title: d[1],
                        page: d[2]
                    })
                }
                g = g.sort(function(t, s) {
                    return t.page - s.page
                })
            }
            return this.sections = g
        },
        pageToSection: function(l) {
            for (var q = this.sections, g, p = 0, n = q.length; p < n && !(q[p].page > l); p++) {
                g = q[p]
            }
            return g
        },
        searchText: function(g) {
            for (var d = 0; d < $(".BookHtml5-page").length; d++) {
                content = $(".BookHtml5-page").eq(d).html().toLowerCase();
                if ((content.indexOf(g.toLowerCase()) + 1) && this.currentPage != d) {
                    this.gotoPage(d);
                    return 0
                }
            }
        },
        currentSection: function() {
            return this.pageToSection(this.currentPage)
        },
        fillToc: function(l, t) {
            var d = i(l || this.opts.toc),
                r, s, n = "";
            if (0 !== d.length) {
                r = this.sections;
                "undefined" === typeof t && (t = this.opts.tocTemplate);
                t || (n = d.is("UL, OL") ? "<li>" : "<div>", t = '<a href="${link}">${section}</a>');
                for (var p = 0, q = r.length; p < q; p++) {
                    s = r[p], s = t.replace(/\$\{link\}/g, "#" + this.id + "/" + s.id.substr(1)).replace(/\$\{section\}/g, s.title).replace(/\$\{page\}/g, s.page), i(s).appendTo(d).wrap(n)
                }
            }
        },
        locationHashToPage: function(g, l) {
            void 0 === g && (g = window.location.hash);
            if (g == "#" + this.id + "/") {
                return 0
            }
            g = g.slice(1).split("/");
            if (g[0] === this.id) {
                if (1 === g.length) {
                    return 0
                }
                var d = parseInt(g[1]);
                if (!isNaN(d)) {
                    return Math.max(d - 1, 0)
                }
                d = this.selectorToPage("#" + g[1]);
                if (void 0 === d) {
                    return 0
                }
                isNaN(g[2]) || (d += Math.max(parseInt(g[2]) - 1, 0));
                return +d
            }
        },
        pageToLocationHash: function(g) {
            var l = "",
                d = g + 1;
            if (g = this.pageToSection(g)) {
                l += "/" + g.id.replace("#", ""), d -= g.page
            }
            1 < d && (l += "/" + d);
            return "#" + this.id + (l || "/")
        },
        selectorToPage: function(d) {
            d = i(d, this.elem).closest(".BookHtml5-page");
            if (d.length) {
                return +d.data("pageIndex")
            }
        },
        getLocationHash: function() {
            return window.location.hash.slice(1)
        },
        locationEndsInHash: function(d) {
            void 0 === d && (d = window.location.href);
            return d.lastIndexOf("#") == d.length - 1
        },
        zoom: function(G) {
            for (var E = 0, F = this.pages.length; E < F; E++) {
                if (this.pages[E].data("holdedAt")) {
                    return
                }
            }
            G < this.zoomMin && (G = this.zoomMin);
            G > this.zoomMax && (G = this.zoomMax);
            if (G !== this.zoomLevel) {
                this.zoomLevel = G;
                var C = this,
                    E = this.zoomWindow,
                    F = this.zoomContent,
                    D = i(this.zoomBoundingBox),
                    z = 2 * C.pageWidth * G * this.currentScale,
                    A = C.pageHeight * G * this.currentScale,
                    B = D.width(),
                    u = D.height(),
                    w = Math.min(z, B),
                    v = Math.min(A, u),
                    H = (2 * this.pageWidth * this.currentScale - w) / 2,
                    t = (this.pageHeight * this.currentScale - v) / 2;
                E.css({
                    width: w,
                    height: v,
                    left: H,
                    top: t
                });
                this.zoomContentOverflowed = z > B || A > u;
                var d = E.offset(),
                    D = D[0] !== window ? D.offset() : {
                        left: D.scrollLeft(),
                        top: D.scrollTop()
                    };
                d.right = d.left + w;
                d.bottom = d.top + v;
                D.right = D.left + B;
                D.bottom = D.top + u;
                d.left < D.left && E.css("left", H + (D.left - d.left));
                d.right > D.right && E.css("left", H + (D.right - d.right));
                d.top < D.top && E.css("top", t + (D.top - d.top));
                d.bottom > D.bottom && E.css("top", t + (D.bottom - d.bottom));
                d = E.offset();
                B = F.offset();
                d.right = d.left + w;
                d.bottom = d.top + v;
                B.right = B.left + z;
                B.bottom = B.top + A;
                B.left > d.left && F.css("marginLeft", 0);
                B.right < d.right && F.css("marginLeft", (parseFloat(F.css("marginLeft")) || 0) + (d.right - B.right));
                B.top > d.top && F.css("marginTop", 0);
                B.bottom < d.bottom && F.css("marginTop", (parseFloat(F.css("marginTop")) || 0) + (d.bottom - B.bottom));
                w === z && F.css("marginLeft", 0);
                v === A && F.css("marginTop", 0);
                this.opts.zoomUsingTransform ? F.css({
                    transform: "scale(" + G * this.currentScale + ")",
                    transformOrigin: "0 0"
                }) : F.css("zoom", G * this.currentScale);
                1 === this.zoomLevel ? (E.unbind("mousemove.BookHtml5").css({
                    overflow: "visible",
                    left: 0,
                    top: 0
                }), this.clipBoundaries && this.clipper.css("overflow", "hidden"), F.css({
                    marginLeft: 0,
                    marginTop: 0
                })) : (this.clipBoundaries && this.clipper.css("overflow", "visible"), E.css("overflow", "hidden"), this.zoomed || E.bind("mousemove.BookHtml5", function(g) {
                    C.zoomedAt(g);
                }));
                this.zoomed = 1 !== G;
                this.positionBookShadow();
                if (this.onZoom) {
                    this.onZoom(this)
                }
                this.toggleControl("zoomIn", this.zoomLevel == this.zoomMax);
                this.toggleControl("zoomOut", this.zoomLevel == this.zoomMin)
            }
        },
        zoomIn: function(d) {
            this.zoom(this.zoomLevel + (d || this.zoomStep))
        },
        zoomOut: function(d) {
            this.zoom(this.zoomLevel - (d || this.zoomStep))
        },
        zoomedAt: function(n) {
            if(this.disableZoomDrag) return;
            if (this.zoomContentOverflowed) {
                var s = this.zoomLevel,
                    l = this.zoomWindow.offset(),
                    r = n.pageX - l.left;
                n = n.pageY - l.top;
                var q = this.zoomWindow.width(),
                    l = 2 * this.pageWidth * s * this.currentScale - q,
                    r = r / q,
                    q = this.zoomWindow.height(),
                    p = this.pageHeight * s * this.currentScale - q;
                adjust = this.opts.zoomUsingTransform ? 1 : s;
                this.zoomContent.css({
                    marginLeft: -l * r / adjust,
                    marginTop: n / q * -p / adjust
                })
            }
        },
        detectBestZoomMethod: function(g, l, d) {
            void 0 === g && (g = i.BookHtml5.support.transform);
            void 0 === d && (d = i.browser.ie8mode);
            g = i.browser.chrome || i.browser.webkit || i.browser.safari || i.browser.opera || d || !g;
            this.opts.zoomUsingTransform = !g;
            return g ? "zoom" : "transform"
        },
        setupFullscreen: function() {
            if (c) {
                var g = this,
                    l, d = "fullscreenerror mozfullscreenerror webkitfullscreenerror MSFullscreenError ";
                l = "fullscreenchange mozfullscreenchange webkitfullscreenchange MSFullscreenChange ".replace(/ /g, ".BookHtml5 ");
                d = d.replace(/ /g, ".BookHtml5 ");
                this._fullscreenChangeHandler = function() {
                    var n = e.fullscreenElement || e.mozFullScreenElement || e.webkitFullscreenElement || e.msFullscreenElement;
                    i(g.opts.fullscreenElement).toggleClass("fullscreen", n);
                    g.elem.toggleClass("fullscreen", n);
                    g.toggleControl("fullscreen", n)
                };
                i(document).on(l, this._fullscreenChangeHandler);
                this._fullscreenErrorHandler = function() {
                    g.opts.onFullscreenError && g.opts.onFullscreenError.apply(this, arguments)
                };
                i(document).on(d, this._fullscreenErrorHandler)
            } else {
                i(this.opts.controls.fullscreen).hide()
            }
        },
        enterFullscreen: function(d) {
            d = i(d || this.opts.fullscreenElement)[0];
            c.call(d || j)
        },
        exitFullscreen: function() {
            a.call(e)
        },
        toggleFullscreen: function() {
            e.fullscreenElement || e.mozFullScreenElement || e.webkitFullscreenElement || e.msFullscreenElement ? this.exitFullscreen() : this.enterFullscreen()
        },
        positionBookShadow: function() {
            var l = this.pages.length,
                p = !(!this.opts.bookShadow || this.zoomed || !l || 3 > l && this.holdedPage);
            this.bookShadow.toggle(p);
            if (p) {
                var p = this.pageWidth,
                    g = this.holdedPageBack,
                    n = !!g && g.onRight && g.pageIndex == (this.rtl ? l - 1 : 0);
                liftingLastPageOnRight = !!g && g.onLeft && g.pageIndex == (this.rtl ? 0 : l - 1);
                noPageOnLeft = !this.leftPage() || n;
                noPageOnRight = !this.rightPage() || liftingLastPageOnRight;
                onePageVisible = noPageOnLeft != noPageOnRight;
                noPageOnLeft && noPageOnRight ? this.bookShadow.hide() : (l = this.opts.zoomUsingTransform ? this.currentScale : 1, this.bookShadow.css({
                    left: (noPageOnLeft ? p : 0) + this.pagesContainer.position().left / l,
                    width: onePageVisible ? p : 2 * p
                }))
            }
        },
        playFlipSound: function() {
            if (this.flipSound) {
                var d = this.opts.onPlayFlipSound;
                i.isFunction(d) && !1 === d(this) || (this.audio || (this.audio = this.createAudioPlayer()), this.audio && this.audio.play && this.audio.play())
            }
        },
        toggleFlipSound: function(d) {
            arguments.length || (d = !this.flipSound);
            this.flipSound = d;
            this.toggleControl("flipSound", !d)
        },
        createAudioPlayer: function(g, p) {
            g || (g = this.opts.flipSoundPath);
            p || (p = this.opts.flipSoundFile);
            for (var d = [], l = 0, n = p.length; l < n; l++) {
                d.push('<source src="' + g + p[l] + '">')
            }
            return i("<audio preload>" + d.join("") + "</audio>")[0]
        },
        _untouch: function(d) {
            return d.originalEvent.touches && d.originalEvent.touches[0] || d
        },
        touchSupport: function() {
            var d = this;
            d.elem.bind("touchstart.BookHtml5", function(l) {
                var g = l.originalEvent.touches;
                1 < g.length || (d._touchStarted = {
                    x: g[0].pageX,
                    y: g[0].pageY,
                    timestamp: l.originalEvent.timeStamp,
                    inHandle: i(l.target).hasClass("BookHtml5-handle")
                }, d._touchStarted.inHandle && d.pageEdgeDragStart(d._untouch(l)))
            });
            i(document).on("touchmove.BookHtml5", function(l) {
                if (d._touchStarted) {
                    var g = l.originalEvent.touches;
                    d._touchEnded = {
                        x: g[0].pageX,
                        y: g[0].pageY,
                        timestamp: l.originalEvent.timeStamp
                    };
                    if (d._touchStarted.inHandle) {
                        return d.pageEdgeDrag(d._untouch(l))
                    }
                    20 < Math.abs(d._touchEnded.x - d._touchStarted.x) && l.preventDefault();
                    l.preventDefault()
                }
            });
            i(document).on("touchend.BookHtml5 touchcancel.BookHtml5", function(p) {
                if (d._touchStarted) {
                    !d._touchEnded && i(p.target).hasClass("BookHtml5-handle") && (p = i(p.target).data("corner"), "r" === p && d.advance(), "l" === p && d.back());
                    var g = d._touchStarted,
                        l = d._touchEnded || d._touchStarted;
                    d._touchStarted = null;
                    d._touchEnded = null;
                    if (g.inHandle) {
                        return d.pageEdgeDragStop({
                            pageX: l.x
                        }), !1
                    }
                    p = l.x - g.x;
                    var n = l.y - g.y,
                        g = l.timestamp - g.timestamp;
                    if (!(20 > Math.abs(p) || 200 < g) && Math.abs(p) > Math.abs(n)) {
                        return 0 > p ? d.advance() : d.back(), !1
                    }
                }
            })
        },
        pageEdgeDragStart: function(g) {
            if (this.animating && !this.curledPage || this.zoomed || !i(g.target).hasClass("BookHtml5-handle")) {
                return !1
            }
            var n = this,
                d = n.elem.offset();
            n.elem.addClass("BookHtml5-unselectable");
            n.mouseDownAtLeft = (g.pageX - d.left) / this.currentScale < n.pageWidth;
            n.pageGrabbed = n.mouseDownAtLeft ? n.leftPage() : n.rightPage();
            if (!n.pageGrabbed) {
                return !1
            }
            this.uncurl(!0);
            n.pageGrabbedOffset = n.pageGrabbed.offset();
            this.opts.zoomUsingTransform && (n.pageGrabbedOffset.left /= this.currentScale, n.pageGrabbedOffset.top /= this.currentScale);
            d = g.pageX / this.currentScale - this.pageGrabbedOffset.left;
            g = g.pageY / this.currentScale - this.pageGrabbedOffset.top;
            this.stopAnimation(!1);
            var l = n.mouseDownAtLeft ? "l" : "r";
            this.holdPage(this.pageGrabbed, d, g);
            this.flip(d, g, this.pageGrabbed, {
                corner: l
            });
            i(document).bind("mousemove.BookHtml5", function(p) {
                return n.pageEdgeDrag(p)
            }).bind("mouseup.BookHtml5", function(p) {
                return n.pageEdgeDragStop(p)
            });
            return !1
        },
        pageEdgeDrag: function(g) {
            var l = g.pageX / this.currentScale - this.pageGrabbedOffset.left;
            g = g.pageY / this.currentScale - this.pageGrabbedOffset.top;
            var d = this.mouseDownAtLeft ? "l" : "r";
            this.stopAnimation(!1);
            this.flip(l, g, this.pageGrabbed, {
                corner: d,
                dragging: !0
            });
            return !1
        },
        pageEdgeDragStop: function(d) {
            var g = this.elem.offset();
            d = (d.pageX - g.left) / this.currentScale < this.pageWidth;
            this.elem.removeClass("BookHtml5-unselectable");
            this._cantStopAnimation || this.stopAnimation(!1);
            this.mouseDownAtLeft && !d ? this.rtl ? this.advance() : this.back() : !this.mouseDownAtLeft && d ? this.rtl ? this.back() : this.advance() : this.releasePage(this.pageGrabbed, !0);
            i(document).unbind("mousemove.BookHtml5 mouseup.BookHtml5")
        },
        curl: function(l, q) {
            if (!this.curledPage && !this.holdedPage) {
                void 0 == l && (l = this.currentPage);
                if ("number" == typeof l || "string" == typeof l) {
                    l = this.pages[+l]
                }
                if (l && !l.isCurled) {
                    l.isCurled = !0;
                    this.curledPage = l;
                    var g = this.pageIsOnTheLeft(l.pageIndex),
                        p = g ? 0 : this.pageWidth,
                        n = q ? this.pageHeight - 1 : 2;
                    this.flip({
                        from: [p, n],
                        to: [p + this.opts.curlSize * (g ? 1 : -1), n + this.opts.curlSize * (q ? -1 : 1)],
                        corner: (q ? "b" : "t") + (g ? "l" : "r"),
                        page: l,
                        duration: 400
                    })
                }
            }
        },
        uncurl: function(d, g) {
            if (this.curledPage) {
                !0 == d && (g = !0, d = void 0);
                void 0 == d && (d = this.curledPage || this.currentPage);
                if ("number" == typeof d || "string" == typeof d) {
                    d = this.pages[+d]
                }
                d.isCurled && (this.stopAnimation(!1), this.releasePage(d, !g, void 0, 400), d.isCurled = !1, this.curledPage = null)
            }
        },
        initThumbnails: function() {
            var g = this.opts;
            this.thumbnails = [];
            var p = this.thumbnailsContainer = i("<div class='BookHtml5-thumbnails'>").append("<div class='BookHtml5-wrapper'><div class='BookHtml5-back BookHtml5-button' /><div class='BookHtml5-clipper'><ul></ul></div><div class='BookHtml5-next BookHtml5-button' /></div>").css("display", "none").css("transform", "translateZ(0)").appendTo(i(g.thumbnailsParent));
            this.thumbnailsList = p.find("ul");
            this.thumbnailsClipper = p.find(".BookHtml5-clipper");
            g.thumbnailsContainerWidth && p.width(g.thumbnailsContainerWidth);
            g.thumbnailsContainerHeight && p.height(g.thumbnailsContainerHeight);
            var d = g.thumbnailsPosition;
            if ("left" == d || "right" == d) {
                g.thumbnailsVertical = !0, "right" == d && p.css({
                    position: "absolute",
                    left: "auto",
                    right: "0px"
                })
            }
            if ("top" == d || "bottom" == d) {
                g.thumbnailsVertical = !1, "bottom" == d && p.css({
                    position: "absolute",
                    top: "auto",
                    bottom: "0px"
                })
            }
            p.addClass(g.thumbnailsVertical ? "BookHtml5-vertical" : "BookHtml5-horizontal");
            var l = this,
                n = g.thumbnailsVertical ? "height" : "width";
            this.thumbnailsBackButton = p.find(".BookHtml5-back.BookHtml5-button").click(function() {
                l._moveThumbnailsList(l.thumbnailsClipper[n]())
            });
            this.thumbnailsNextButton = p.find(".BookHtml5-next.BookHtml5-button").click(function() {
                l._moveThumbnailsList(-l.thumbnailsClipper[n]())
            });
            this.thumbnailsClipper.css("transform", "translateZ(0)")
        },
        destroyThumbnails: function() {
            this.thumbnailsContainer && this.thumbnailsContainer.remove();
            this.thumbnails = this.thumbnailsContainer = null
        },
        createThumbnails: function() {
            this.thumbnails || this.initThumbnails();
            var l, p = this.thumbnailConfig();
            this.thumbnails = [];
            for (var g = 0, n = this.pages.length; g < n; g++) {
                l = this.createThumbnail(g, p), this.thumbnailsList.append(l), this.thumbnails.push(l)
            }
            if (this.rtl) {
                for (l = this.thumbnailsList.children(), l.eq(0).addClass("BookHtml5-right").removeClass("BookHtml5-left"), g = 1, n = l.length; g < n; g += 2) {
                    l.eq(g).insertAfter(l.eq(g + 1))
                }
            }
            g = this.thumbnailsContainer;
            g.width() || g.width(2 * p.width)
        },
        thumbnailConfig: function() {
            var g = {},
                p = i('<div class="BookHtml5-thumbnail" style="display:none;position:absolute;line-height:0px;font-size:0px;">').prependTo(this.thumbnailsContainer),
                d = this.opts.thumbnailHeight || (2 >= p.height() ? 0 : p.height()),
                l = this.opts.thumbnailWidth || p.width(),
                n = l / this.pageWidth || d / this.pageHeight || this.opts.thumbnailScale;
            p.remove();
            g.width = l || this.pageWidth * n;
            g.height = d || this.pageHeight * n;
            g.cloneCSS = {
                display: "block",
                left: 0,
                top: 0,
                position: "relative",
                transformOrigin: "0 0"
            };
            i.BookHtml5.support.transform ? g.cloneCSS.transform = "scale(" + n + ")" : g.cloneCSS.zoom = n;
            return g
        },
        createThumbnail: function(g, p) {
            var d = this.pages[g];
            if (d) {
                p || (p = this.thumbnailConfig());
                var l = i('<li class="BookHtml5-thumbnail"><div class="BookHtml5-overlay">').addClass(this.pageIsOnTheLeft(g) ? "BookHtml5-left" : "BookHtml5-right").css({
                    width: p.width,
                    height: p.height
                });
                this.opts.thumbnailsSprite ? l.css("background", "url(" + this.opts.thumbnailsSprite + ") no-repeat 0px -" + g * p.height + "px") : (d = d.clone(), d.hasClass("BookHtml5-page-holded") && (this.resetPage(d), d.find(".BookHtml5-fold-gradient-container").remove()), d.css(p.cloneCSS), l.prepend(d));
                var n = this;
                l.click(function() {
                    n.gotoPage(g)
                });
                return l
            }
        },
        updateThumbnail: function(l, p) {
            var g = this.thumbnails[l];
            if (g) {
                var n = this.createThumbnail(l, p);
                n && (this.opts.thumbnailsSprite || g.children(":not(.BookHtml5-overlay)").replaceWith(n.children(":not(.BookHtml5-overlay)")), g.width(n.width()), g.height(n.height()))
            }
        },
        updateThumbnails: function() {
            if (this.thumbnails) {
                for (var g = this.thumbnailConfig(), l = 0, d = this.pages.length; l < d; l++) {
                    this.updateThumbnail(l, g)
                }
                l = this.thumbnailsContainer;
                l.width() || l.width(2 * g.width)
            } else {
                this.createThumbnails()
            }
        },
        _moveThumbnailsList: function(g, l) {
            var d = this.thumbnailsList.position()[this.opts.thumbnailsVertical ? "top" : "left"];
            this._setThumbnailsListPosition(d + g, l)
        },
        _setThumbnailsListPosition: function(n, s) {
            var l = this.opts.thumbnailsVertical,
                r = l ? "height" : "width",
                q = {},
                p = this.thumbnailsClipper[r](),
                r = -this.thumbnailsList[r]() + p;
            n < r && (n = r);
            0 < n && (n = 0);
            this.thumbnailsBackButton.toggleClass("BookHtml5-disabled", 0 == n || 0 < r);
            this.thumbnailsNextButton.toggleClass("BookHtml5-disabled", n == r || 0 < r);
            q[l ? "top" : "left"] = n;
            this.thumbnailsList.animate(q, void 0 != s ? s : this.opts.thumbnailsAnimOptions)
        },
        showThumbnail: function(n, t) {
            if (this.thumbnails && this.thumbnailsContainer.is(":visible")) {
                void 0 == n && (n = this.currentPage);
                0 < n && this.pageIsOnTheRight(n) && n--;
                var l = this.opts.thumbnailsVertical,
                    s = l ? "top" : "left",
                    r = l ? "height" : "width",
                    q = this.thumbnails[n],
                    p = this.thumbnailsClipper,
                    l = p[r]() / 2 - q[r]() / (l ? 2 : 1),
                    s = q.offset()[s] - p.offset()[s];
                this._moveThumbnailsList(l - s, t)
            }
        },
        showThumbnails: function(d) {
            this.thumbnails && this.thumbnails.length || this.createThumbnails();
            this.thumbnailsContainer.fadeIn(void 0 != d ? d : this.opts.thumbnailsAnimOptions);
            this.showThumbnail(void 0, 0)
        },
        hideThumbnails: function(d) {
            this.thumbnailsContainer.fadeOut(void 0 != d ? d : this.opts.thumbnailsAnimOptions)
        },
        toggleThumbnails: function(d) {
            i(this.thumbnailsContainer).is(":visible") ? this.hideThumbnails(d) : this.showThumbnails(d)
        },
        controllify: function(d) {
            var g = this;
            i(d.zoomIn).on("click.BookHtml5", function() {
                g.zoomIn();
                return !1
            });
            i(d.zoomOut).on("click.BookHtml5", function() {
                g.zoomOut();
                return !1
            });
            i(d.next).on("click.BookHtml5", function() {
                g.advance();
                return !1
            });
            i(d.back).on("click.BookHtml5", function() {
                g.back();
                return !1
            });
            i(d.first).on("click.BookHtml5", function() {
                g.gotoPage(0);
                return !1
            });
            i(d.last).on("click.BookHtml5", function() {
                g.gotoPage(g.pages.length - 1);
                return !1
            });
            i(d.slideShow).on("click.BookHtml5", function() {
                g.toggleSlideShow();
                return !1
            });
            i(d.flipSound).on("click.BookHtml5", function() {
                g.toggleFlipSound();
                return !1
            });
            i(d.thumbnails).on("click.BookHtml5", function() {
                g.toggleThumbnails();
                return !1
            });
            i(d.fullscreen).on("click.BookHtml5", function() {
                g.toggleFullscreen();
                return !1
            })
        },
        toggleControl: function(d, g) {
            (d = this.opts.controls[d]) && i(d).toggleClass("BookHtml5-disabled", g)
        }
    };
    i.BookHtml5.defaults = {
        width: 500,
        height: 300,
        startPage: 0,
        hardcovers: !1,
        hardPages: !1,
        centeredWhenClosed: !1,
        transparentPages: !1,
        doublePages: ".double",
        responsive: !1,
        scaleToFit: "",
        onResize: null,
        fullscreenElement: document.documentElement,
        onFullscreenError: null,
        use3d: !0,
        perspective: 2000,
        useTranslate3d: "mobile",
        bookShadow: !0,
        gutterShadow: !0,
        shadowThreshold: 20,
        shadows: !0,
        foldGradient: !0,
        foldGradientThreshold: 20,
        pageNumbers: !0,
        firstPageNumber: 1,
        numberedPages: !1,
        deepLinking: !0,
        updateBrowserURL: !0,
        curl: !0,
        curlSize: 40,
        slideShow: !1,
        slideShowDelay: 1000,
        pauseOnHover: !0,
        touchEnabled: !0,
        mouseWheel: !1,
        handleWidth: !1,
        handleClickDuration: 300,
        turnPageDuration: 1000,
        turnPageDurationMin: 300,
        forceBasicPage: !1,
        sections: ".BookHtml5-section",
        zoomLevel: 1,
        zoomMax: 2,
        zoomMin: 1,
        zoomBoundingBox: window,
        zoomStep: 0.05,
        onZoom: null,
        flipSound: !0,
        flipSoundFile: ["page-flip.mp3", "page-flip.ogg"],
        flipSoundPath: "/static/book/sound/",
        onPlayFlipSound: null,
        keyboardNavigation: {
            back: 37,
            advance: 39
        },
        clipBoundaries: !0,
        controls: {},
        onShowPage: null,
        onHoldPage: null,
        onReleasePage: null,
        thumbnails: !1,
        thumbnailsParent: "body",
        thumbnailScale: 0.2,
        thumbnailWidth: null,
        thumbnailHeight: null,
        thumbnailsPosition: null,
        thumbnailsVertical: !0,
        thumbnailsContainerWidth: null,
        thumbnailsContainerHeight: null,
        thumbnailsSprite: null,
        thumbnailsAnimOptions: {}
    };
    "undefined" != typeof QUnit && (i.BookHtml5.BookHtml5 = f);
    window.raf = function() {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(d) {
            window.setTimeout(d, 1000 / 60)
        }
    }();
    i.browser.ie8mode = i.browser.msie && 8 == document.documentMode;
    var h = i.browser.msie && 9 > i.browser.version ? 1 : 0,
        o = {
            thin: 2 - h,
            medium: 4 - h,
            thick: 6 - h
        },
        e = window.document,
        j = e.documentElement,
        c = j.requestFullscreen || j.mozRequestFullScreen || j.webkitRequestFullscreen || j.msRequestFullscreen,
        a = e.exitFullscreen || e.mozCancelFullScreen || e.webkitExitFullscreen || e.msExitFullscreen;
    i.BookHtml5.utils = {
        translate: function(g, l, d) {
            return i.BookHtml5.useTranslate3d ? "translate3d(" + g + "px, " + l + "px, " + (d || 0) + "px) " : "translate(" + g + "px, " + l + "px) "
        },
        isMobile: function() {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
        }
    }
})(jQuery);
(function(e) {
    function b(j) {
        var i = j || window.event,
            f = [].slice.call(arguments, 1),
            k = 0,
            h = 0,
            d = 0;
        j = e.event.fix(i);
        j.type = "mousewheel";
        i.wheelDelta && (k = i.wheelDelta / 120);
        i.detail && (k = -i.detail / 3);
        d = k;
        void 0 !== i.axis && i.axis === i.HORIZONTAL_AXIS && (d = 0, h = -1 * k);
        void 0 !== i.wheelDeltaY && (d = i.wheelDeltaY / 120);
        void 0 !== i.wheelDeltaX && (h = -1 * i.wheelDeltaX / 120);
        f.unshift(j, k, h, d);
        return (e.event.dispatch || e.event.handle).apply(this, f)
    }
    var c = ["DOMMouseScroll", "mousewheel"];
    if (e.event.fixHooks) {
        for (var a = c.length; a;) {
            e.event.fixHooks[c[--a]] = e.event.mouseHooks
        }
    }
    e.event.special.mousewheel = {
        setup: function() {
            if (this.addEventListener) {
                for (var f = c.length; f;) {
                    this.addEventListener(c[--f], b, !1)
                }
            } else {
                this.onmousewheel = b
            }
        },
        teardown: function() {
            if (this.removeEventListener) {
                for (var f = c.length; f;) {
                    this.removeEventListener(c[--f], b, !1)
                }
            } else {
                this.onmousewheel = null
            }
        }
    };
    e.fn.extend({
        mousewheel: function(f) {
            return f ? this.bind("mousewheel", f) : this.trigger("mousewheel")
        },
        unmousewheel: function(f) {
            return this.unbind("mousewheel", f)
        }
    })
})(jQuery);
(function(e) {
    function b(d) {
        if (d in c.style) {
            return e.BookHtml5.support[d] = d
        }
        for (var h = a.length, f, i = d.charAt(0).toUpperCase() + d.substr(1); h--;) {
            if (f = a[h] + i, f in c.style) {
                return e.BookHtml5.support[d] = f
            }
        }
    }
    if (e.cssHooks) {
        var c = document.createElement("div"),
            a = ["O", "ms", "Webkit", "Moz"];
        b("transform");
        b("transformOrigin");
        b("boxSizing");
        b("zoom");
        e.BookHtml5.support.boxSizing && 8 > document.documentMode && (e.BookHtml5.support.boxSizing = !1);
        c = null;
        e.each(["transform", "transformOrigin"], function(d, f) {
            e.BookHtml5.support[f] && e.BookHtml5.support[f] !== f && !e.cssHooks[f] && (e.cssHooks[f] = {
                get: function(g, i, h) {
                    return e.css(g, e.BookHtml5.support[f])
                },
                set: function(g, h) {
                    g.style[e.BookHtml5.support[f]] = h
                }
            })
        });
        e.BookHtml5.applyAlphaImageLoader = function(f) {
            var j, i, m, h, d = "",
                k = e("<div style='display:none'></div>").appendTo("body");
            i = 0;
            for (m = f.length; i < m; i++) {
                if (h = f[i], k.addClass(h), j = k.css("background-image").match(/^url\("(.*)"\)$/)) {
                    d += "." + h + "{background:none; filter : progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + j[1] + "', sizingMethod='scale'); } ", k.removeClass(h)
                }
            }
            e("body").append("<style>" + d + "</style>")
        }
    } else {
        alert("jQuery 1.4.3+ is needed for this plugin to work")
    }
})(jQuery);