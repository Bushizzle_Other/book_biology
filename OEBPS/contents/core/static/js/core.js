// Scrollbar

(function($){
	$(window).load(function(){
		
		$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
		$.mCustomScrollbar.defaults.axis="y"; //enable 2 axis scrollbars by default

		$(".lesson-content").mCustomScrollbar({theme:"dark"});
		
		$(".all-themes-switch a").click(function(e){
			e.preventDefault();
			var $this=$(this),
				rel=$this.attr("rel"),
				el=$(".lesson-content");
			switch(rel){
				case "toggle-content":
					el.toggleClass("expanded-content");
					break;
			}
		});
		
	});
})(jQuery);

// fancybox
$(document).ready(function() {
	$('.fancybox').fancybox({
		'showCloseButton' : true,
      beforeShow: function() {
            $(".fancybox-image").one("click", function() {
                  $.fancybox.close();
            });
        }

	});
});

